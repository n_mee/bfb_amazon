// html5
(function(a,b){function h(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function i(){var a=l.elements;return typeof a=="string"?a.split(" "):a}function j(a){var b={},c=a.createElement,f=a.createDocumentFragment,g=f();a.createElement=function(a){l.shivMethods||c(a);var f;return b[a]?f=b[a].cloneNode():e.test(a)?f=(b[a]=c(a)).cloneNode():f=c(a),f.canHaveChildren&&!d.test(a)?g.appendChild(f):f},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+i().join().replace(/\w+/g,function(a){return b[a]=c(a),g.createElement(a),'c("'+a+'")'})+");return n}")(l,g)}function k(a){var b;return a.documentShived?a:(l.shivCSS&&!f&&(b=!!h(a,"article,aside,details,figcaption,figure,footer,header,hgroup,nav,section{display:block}audio{display:none}canvas,video{display:inline-block;*display:inline;*zoom:1}[hidden]{display:none}audio[controls]{display:inline-block;*display:inline;*zoom:1}mark{background:#FF0;color:#000}")),g||(b=!j(a)),b&&(a.documentShived=b),a)}function p(a){var b,c=a.getElementsByTagName("*"),d=c.length,e=RegExp("^(?:"+i().join("|")+")$","i"),f=[];while(d--)b=c[d],e.test(b.nodeName)&&f.push(b.applyElement(q(b)));return f}function q(a){var b,c=a.attributes,d=c.length,e=a.ownerDocument.createElement(n+":"+a.nodeName);while(d--)b=c[d],b.specified&&e.setAttribute(b.nodeName,b.nodeValue);return e.style.cssText=a.style.cssText,e}function r(a){var b,c=a.split("{"),d=c.length,e=RegExp("(^|[\\s,>+~])("+i().join("|")+")(?=[[\\s,>+~#.:]|$)","gi"),f="$1"+n+"\\:$2";while(d--)b=c[d]=c[d].split("}"),b[b.length-1]=b[b.length-1].replace(e,f),c[d]=b.join("}");return c.join("{")}function s(a){var b=a.length;while(b--)a[b].removeNode()}function t(a){var b,c,d=a.namespaces,e=a.parentWindow;return!o||a.printShived?a:(typeof d[n]=="undefined"&&d.add(n),e.attachEvent("onbeforeprint",function(){var d,e,f,g=a.styleSheets,i=[],j=g.length,k=Array(j);while(j--)k[j]=g[j];while(f=k.pop())if(!f.disabled&&m.test(f.media)){for(d=f.imports,j=0,e=d.length;j<e;j++)k.push(d[j]);try{i.push(f.cssText)}catch(l){}}i=r(i.reverse().join("")),c=p(a),b=h(a,i)}),e.attachEvent("onafterprint",function(){s(c),b.removeNode(!0)}),a.printShived=!0,a)}var c=a.html5||{},d=/^<|^(?:button|form|map|select|textarea|object|iframe)$/i,e=/^<|^(?:a|b|button|code|div|fieldset|form|h1|h2|h3|h4|h5|h6|i|iframe|img|input|label|li|link|ol|option|p|param|q|script|select|span|strong|style|table|tbody|td|textarea|tfoot|th|thead|tr|ul)$/i,f,g;(function(){var c=b.createElement("a");c.innerHTML="<xyz></xyz>",f="hidden"in c,f&&typeof injectElementWithStyles=="function"&&injectElementWithStyles("#modernizr{}",function(b){b.hidden=!0,f=(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle).display=="none"}),g=c.childNodes.length==1||function(){try{b.createElement("a")}catch(a){return!0}var c=b.createDocumentFragment();return typeof c.cloneNode=="undefined"||typeof c.createDocumentFragment=="undefined"||typeof c.createElement=="undefined"}()})();var l={elements:c.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:c.shivCSS!==!1,shivMethods:c.shivMethods!==!1,type:"default",shivDocument:k};a.html5=l,k(b);var m=/^$|\b(?:all|print)\b/,n="html5shiv",o=!g&&function(){var c=b.documentElement;return typeof b.namespaces!="undefined"&&typeof b.parentWindow!="undefined"&&typeof c.applyElement!="undefined"&&typeof c.removeNode!="undefined"&&typeof a.attachEvent!="undefined"}();l.type+=" print",l.shivPrint=t,t(b)})(this,document)
// browser selector
function css_browser_selector(u){var ua = u.toLowerCase(),is=function(t){return ua.indexOf(t)>-1;},g='gecko',w='webkit',s='safari',o='opera',h=document.getElementsByTagName('html')[0],b=[(!(/opera|webtv/i.test(ua))&&/msie\s(\d)/.test(ua))?('ie ie'+RegExp.$1):is('firefox/2')?g+' ff2':is('firefox/3.5')?g+' ff3 ff3_5':is('firefox/3')?g+' ff3':is('gecko/')?g:is('opera')?o+(/version\/(\d+)/.test(ua)?' '+o+RegExp.$1:(/opera(\s|\/)(\d+)/.test(ua)?' '+o+RegExp.$2:'')):is('konqueror')?'konqueror':is('chrome')?w+' chrome':is('iron')?w+' iron':is('applewebkit/')?w+' '+s+(/version\/(\d+)/.test(ua)?' '+s+RegExp.$1:''):is('mozilla/')?g:'',is('j2me')?'mobile':is('iphone')?'iphone':is('ipod')?'ipod':is('mac')?'mac':is('darwin')?'mac':is('webtv')?'webtv':is('win')?'win':is('freebsd')?'freebsd':(is('x11')||is('linux'))?'linux':'','js']; c = b.join(' '); h.className += ' '+c; return c;}; css_browser_selector(navigator.userAgent);
// Form Elements
var checkboxHeight = "25";
var radioHeight = "25";
var selectWidth = "190";
var pos=0;
/* No need to change anything after this */
document.write('<style type="text/css">input.styled { display: none; } select.styled { position: relative; width: ' + selectWidth + 'px; opacity: 0; filter: alpha(opacity=0); z-index: 5; }</style>');
var Custom = {
	init: function() {
		var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
		for(a = 0; a < inputs.length; a++) {
			if((inputs[a].type == "checkbox" || inputs[a].type == "radio") && inputs[a].className == "styled") {
				span[a] = document.createElement("span");
				span[a].className = inputs[a].type;

				if(inputs[a].checked == true) {
					if(inputs[a].type == "checkbox") {
						position = "0 -" + (checkboxHeight*2) + "px";
						span[a].style.backgroundPosition = position;
					} else {
						position = "0 -" + (radioHeight*2) + "px";
						span[a].style.backgroundPosition = position;
					}
				}
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				inputs[a].onchange = Custom.clear;
				span[a].onmousedown = Custom.pushed;
				span[a].onmouseup = Custom.check;
				document.onmouseup = Custom.clear;
			}
		}
		inputs = document.getElementsByTagName("select");
		for(a = 0; a < inputs.length; a++) {
			if(inputs[a].className == "styled") {
				option = inputs[a].getElementsByTagName("option");
				active = option[0].childNodes[0].nodeValue;
				textnode = document.createTextNode(active);
				for(b = 0; b < option.length; b++) {
					if(option[b].selected == true) {
						textnode = document.createTextNode(option[b].childNodes[0].nodeValue);
					}
				}
				span[a] = document.createElement("span");
				span[a].className = "select";
				span[a].id = "select" + inputs[a].name;
				span[a].appendChild(textnode);
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				inputs[a].onchange = Custom.choose;
			}
		}
	},
	pushed: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight*3 + "px";
		} else if(element.checked == true && element.type == "radio") {
			this.style.backgroundPosition = "0 -" + radioHeight*3 + "px";
		} else if(element.checked != true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
		} else {
			this.style.backgroundPosition = "0 -" + radioHeight + "px";
		}
	},
	check: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 0";
			element.checked = false;
		} else {
			if(element.type == "checkbox") {
				this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			} else {
				this.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
				group = this.nextSibling.name;
				inputs = document.getElementsByTagName("input");
				for(a = 0; a < inputs.length; a++) {
					if(inputs[a].name == group && inputs[a] != this.nextSibling) {
						inputs[a].previousSibling.style.backgroundPosition = "0 0";
					}
				}
			}
			element.checked = true;
		}
	},
	clear: function() {
		inputs = document.getElementsByTagName("input");
		for(var b = 0; b < inputs.length; b++) {
			if(inputs[b].type == "checkbox" && inputs[b].checked == true && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			} else if(inputs[b].type == "checkbox" && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			} else if(inputs[b].type == "radio" && inputs[b].checked == true && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
			} else if(inputs[b].type == "radio" && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			}
		}
	},
	choose: function() {
		option = this.getElementsByTagName("option");
		for(d = 0; d < option.length; d++) {
			if(option[d].selected == true) {
				document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue;
			}
		}
	}
}


var loading_ajax_posts = false;
function ajax_load_more_posts() {
	//if ( console !== undefined ) console.log('ajax loading');
	var $ld = jQuery('#load-more-posts');
	var next = $ld.attr('href');
	$ld.hide();
	$cn = jQuery('#posts-container');
	loading_ajax_posts = true;	
	var load_seed = Math.round(Math.random()*10000);
	newurl = next + (~next.indexOf('?')?'&':'?') + 'load_ajax_posts=' + load_seed;
	jQuery.ajax({ 
	  	type: "GET", 
    	url: newurl, 
    	//dataType: "html", 
    	cache : true, //! jQuery('html').is('ie') 
   	crossDomain: false, 
    	success: function(resp) {
			$ld.remove();
/*			
			if(jQuery('body').is('.desktop-view'))
				jQuery("li.fckn-pinit a").removeClass().find('*').remove();
			jQuery('a[href*="//pinterest.com/pin/create/button/"]').each(function(){
		    	this.href = this.href.replace('//pinterest.com/pin/create/button/','//www.pinterest.com/pin/create/button/');
	    	});
*/
	    	
	    	$cn.append(jQuery(resp));
        	try{
	        	_gaq.push(['_trackPageview', next.replace(/http:\/\/(www)?\.bforbel\.com/i, '')]);
	        	var title=((document.getElementsByTagName('tkillitle').length==0)?'':document.getElementsByTagName('title')[0].innerHTML);
				var __e = new WoopraEvent('pv', {url:next.replace(/http:\/\/(www)?\.bforbel\.com/i, ''),title:title}, woopraTracker.cv, 'visit');
				__e.fire();
			}catch(e){
				if(window.console)console.log('Tracking exception: ' + e.description);
			}
		
			// BFBWEB-34 re-render pinit buttons.
			window.parsePins();

/*			
			jQuery.ajax({
				url     : 'http://assets.pinterest.com/js/pinit.js',
				dataType: 'script',
				cache   : true
 				}).done(function(){
 					return;
					jQuery('a[href*="//www.pinterest.com/pin/create/button/"]').each(function(){
					this.href = this.href.replace('//www.pinterest.com/pin/create/button/','//pinterest.com/pin/create/button/');
					});
			});
*/				
			
//update share buttons newly loaded.			
//			FB.XFBML.parse();
			stButtons.locateElements();
			$cn.find('div.fb-comments').each(function(){
				//FB.XFBML.parse(this[0]);	//removed this as it slows the site down too much			
			});
			//twttr.widgets.load();
	    	loading_ajax_posts = false;
	    	jQuery('img').removeAttr('height');
			jQuery("li.fckn-pinit a").addClass('pin-fix').css("width",'80px !important');
		    jQuery('.st_tumblr_hcount .stArrow').each(function(){
			    $t = jQuery(this);
			    $t.css('margin-left','-2px');
		    });
	    }, 
	    error: function (xhr, status, error) {
	    	console && console.log(status);
    		if ( status != 404 ) $ld.remove();
    		loading_ajax_posts = false;
	    	//if ( console !== undefined ) console.log(xhr);
	    	//if ( console !== undefined ) console.log(status);
	    	//if ( console !== undefined ) console.log(error);
	        //$ld.show();
	    	//loading_ajax_posts = false;  
	    }    
    });  
}

function _setCookie(c_name,value,exdays){
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value + ";path=/";
}

function _getCookie(c_name) {
  var i,x,y,ARRcookies=document.cookie.split(";");
  for (i=0;i<ARRcookies.length;i++) {
	  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
	  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
	  x=x.replace(/^\s+|\s+$/g,"");
	  if (x==c_name) return unescape(y);
  }
}

function force_desktop() {
	if ( ! is_mobile_user ) return false;
	jQuery('.compact').removeClass('compact').removeClass('mobile');
	jQuery('meta[name="viewport"]').attr('content','user-scalable=yes,width=1120');
	jQuery('body').addClass('desktop-view');
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var is_mobile_user = false;
var isiPad = navigator.userAgent.match(/iPad/i) != null;


//To Close the Banner
function appBannerClose()
{
	var x = document.getElementById("app-banner");
	x.style.display='none';
}
	
	
jQuery(function($){
 
 	if($('article.single-post .share-list-bottom .fckn-pinit span').text()){
 		$('article.single-post .share-list-bottom .fckn-pinit').next().css('margin-left','30px');
 		$('article.single-post .share-list .fckn-pinit').next().css('margin-left','30px');
 	}
  
  $('img').removeAttr('height');
  var $body = $('body');
  $('a.switch-to-mobile').click(function(){
	  _setCookie('forced_dt_view_n','no',7);
	  return true;
  });
  a = navigator.userAgent||navigator.vendor||window.opera;
  if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
  	is_mobile_user = true;
  if(getParameterByName('fview') == 'm') is_mobile_user = true;

  //if ( isiPad && _getCookie('forced_dt_view_n') != 'no' ) {
  	//force_desktop();
  	//is_mobile_user = false;
	//jQuery('meta[name="viewport"]').attr('content','user-scalable=yes,width=980');  	
  //}
  var is_ff = !(window.mozInnerScreenX == null);
  if ( is_mobile_user ) $body.addClass('compact').addClass('mobile');  	
  
  $('#switch-to-desktop').click(function(){
  	//force_desktop();
  	_setCookie('forced_dt_view_n','yes');
  	return true;
  });
  $fcc= $('div.facebook-comment-container');
  $('a.toggle-next').live('click', function(){
  	$(this).next().toggle();
  	$(this).parent().toggleClass('comments-open');
  	if ( $(this).html() == 'Comments' ) {
  		$(this).html('View Comments');
	} else {
		$(this).html('Comments');
  	}
  	return false;
  });
  var container = document.body;
  var canFixed = false;

	if(/(iPhone|iPod|iPad)/i.test(a)) {
		if(/OS [2-4]_\d(_\d)? like Mac OS X/i.test(navigator.userAgent)) {
			// iOS 2-4
		} else if(/CPU like Mac OS X/i.test(navigator.userAgent)) {
			// iOS 1
		} else {
			canFixed = true;
		}
	} else if(/chrome/i.test(a)) {
		canFixed = true;
	} else if(/(android|galaxy)/i.test(a)) {
		canFixed = false;
	} else {
	  if (document.createElement && container && container.appendChild && container.removeChild) {
	    var el = document.createElement('div');
	    if (el.getBoundingClientRect) {
		    el.innerHTML = 'x';
		    el.style.cssText = 'position:fixed;top:100px;';
		    container.appendChild(el);
		    var originalHeight = container.style.height,
		        originalScrollTop = container.scrollTop;
		    container.style.height = '3000px';
		    container.scrollTop = 500;
		    var elementTop = el.getBoundingClientRect().top;
		    container.style.height = originalHeight;
		    var isSupported = (elementTop === 100);
		    container.removeChild(el);
		    container.scrollTop = originalScrollTop;
	        canFixed = isSupported;
	    }
	  }
	}

	//!!!! Temp
	//canFixed = true;

	Custom.init;
	var $wnd = $(window);
	var $anc = $('#load-more-posts');
	if ( $anc.length ) $anc.live('click',function(){ ajax_load_more_posts(); return false; });	
	var $gcn = $('#content');
	var $aside = $('#sidebar');
	var $mvs = $aside.find('section.widget-container').eq(-2).css('margin-top','0');
	if ( ! $mvs.length ) $mvs = $aside.find('section.widget-container').last().css('margin-top','0');
	$mvs.before('<div id="sb-anchor"></div>');
	$aside.append('<div id="sb-end-anchor"></div>');
	var $sba = $('#sb-anchor');
	var $sbea = $('#sb-end-anchor');
	var	$hdr = $('#header');
	var $sidebar = $('#sidebar');
	var	$logo = $hdr.find('a.logo');
	var	$fixed = $hdr.find('.fixed-block');
	var $content = $('#content');
	var $cleft = $content.find('.left-page');
	var dx = $body.is('.admin-bar')?28:0; 
	var isSingle = $('body').is('.single');
	var _f_responsive = function(){
		ws = $wnd.scrollTop() + dx;
		wh = $wnd.height();
		ww = $wnd.width();
		isDesktop = $body.is('.desktop-view');
		if (isDesktop && ww < 981){
			$body.css('min-width','981px');
			ww = 981;
		}
		if ( is_ff && ww > 840 && ww < 861 ) $body.css('width','840px');
		if ( is_ff && (ww < 841 || ww > 860)) $body.css('width','100%');
		// Logo Adjust
		if ( ww < 860 ) {
			$logo.css('width', '155px' );
			$body.addClass('compact');
		} else {
			$body.removeClass('compact');
			$('#mobile-nav').show();
			$logo.css('width', ($hdr.width() - $fixed.width() - 15) + 'px' );
		}
		
		// Content Area
		if ( ww < 860 ) {
			$content.addClass('compact');
			$cleft.css('max-width', '100%');
		} else {
			$content.removeClass('compact');
			if ( ! isSingle )
				if ( ww < 980 ) {
					$cleft.addClass('compact');
				} else {
					$cleft.removeClass('compact');
				}
			cwd = ($content.width() * 0.95 - $sidebar.width());
			if (cwd > 767) cwd = 767;
			$cleft.css('max-width',cwd + 'px');
		}
		//if (! isSingle && $fcc.length) $('div.facebook-comment-container').width($fcc.parent().width()+84);
		if ( isSingle && ww > 641 ) $('div.fb-com-container').show();
		// Ajax
		$anc = $('#load-more-posts');
		if ( $anc.length && $gcn.is('.infinite-scroll') && ! loading_ajax_posts && ( ws + wh * 4 > $anc.offset().top ) ) ajax_load_more_posts();
		// Move objects;
		if(isDesktop){
			$('.sticky-area').each(function(){
				$t = $(this);
				$a = $t.closest('article');
				tp = $a.offset().top;
				aid = $a.attr('data-post-id');
				if (tp > ws || $a.outerHeight() + tp < ws || $t.closest('.compact').length ) {
					$t.css({
						top: '0',
						position: 'relative',
						right: '0'
					});
				} else {
					tt = ws - tp;
					if ( canFixed ) {
						pos = $a.height() - $t.outerHeight() - tt;
						if ( pos > 0 ) pos = 0;
						_ttt = ww - ($a.offset().left + $a.width());
						if (460 + _ttt + 220 > $wnd.width()) _ttt = $wnd.width() - 680;
						$t.css({
							top: (pos + dx) + 'px',
							position: 'fixed',
							right: _ttt + 'px'
						});
					} else {
						/*tt2 = $a.height() - $t.outerHeight();
						if ( tt2 < tt ) tt = tt2;
						$t.css({
							top: tt + 'px',
							position: 'relative',
							left: '0'
						});*/
					}
				}
			});
			// Move sidebar
			if(!isSingle && isDesktop && $mvs.length){
				if(canFixed){
					$sba.addClass('can-be-fixed');
					isfixed = $mvs.css('position') == 'fixed';
					_otop   = $mvs.offset().top;
					_sba_top= $sba.offset().top;
					_tot_h  = $mvs.outerHeight(true) + $mvs.next('section').outerHeight(true);
					if((isfixed && _sba_top >= _otop) || ws + wh < _sba_top + _tot_h){
						$mvs.add($mvs.next('section')).css({
							position: 'relative',
							top : '0',
							left: '0'
						});
					}else if ( isfixed || ws > _otop ){
						dddd = wh - _tot_h;
						if (dddd > 7) dddd = 7;
						$mvs.css({
							position: 'fixed',
							top : (dx + dddd) + 'px',
							left: $aside.offset().left + 'px'
						});
						$mvs.next('section').css({
							position: 'fixed',
							top : (dx + dddd + $mvs.outerHeight(true))+ 'px',
							left: $aside.offset().left + 'px'
						});
					}
				}else{
					$sba.addClass('not-fixed');
					ofs = ws - $mvs.offset().top + parseInt($mvs.css('margin-top'));
					if ( $sbea.offset().top > wh + ws ) ofs -= $sbea.offset().top - wh - ws
					if ( ofs < 0 ) ofs = 0;
					$mvs.css('margin-top', ofs + 'px');
				}
			}
		}
	};
	if(!$body.is('.desktop-view')){
		$(window).scroll(function(){
			var __top = 0;
			var sel_1 = $('header#header');
			var sel_2 = $('.compact #mobile-nav');
			if($(this).scrollTop() < pos || $(this).scrollTop() == 0){
				sel_1.removeAttr('style');
				sel_2.css({'top':'90px'});
			} else {
				sel_1.css({'top':'-95px'});
				sel_2.css({'top':'-1000px'});
			}
			pos = $(this).scrollTop();
		});
		$('article.single-post p img').css('width','100%');
		var button = '<iframe style="margin-top:50px; margin-bottom: -50px;" id="FB_main_fraim" src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fbforbelmedia&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=80" height="240" width="320" frameborder="0" scrolling="no"></iframe>';
		$('article.single-post ul.share-list-bottom').before(button);
	}else{
		// script for add like button after image
		var button = '<iframe id="FB_main_fraim" src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fbforbelmedia&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=80" width="320" frameborder="0" scrolling="no"></iframe>'; 
		$('article.single-post p').find('img').first().after(button);
	}
	$wnd.scroll(_f_responsive);
	$wnd.resize(_f_responsive);
	if (_getCookie('forced_dt_view_n') == 'yes' && is_mobile_user ) force_desktop();
	_f_responsive();
	jQuery('.st_tumblr_hcount .stArrow').each(function(){
		$t = jQuery(this);
		$t.css('margin-left','-2px');
	});
	
	
	$('article.single-post ul.share-list li').css("visibility","visible"); 
	$('article.single-post ul.share-list-bottom li').css("visibility","visible");
	if (screen.width == 0 || screen.width > 529)
	{
		$('#app-banner-view-button').css("visibility","visible");
	}
	if ($(window).width()> 529)
	{
                $('#app-banner-view-button').css("visibility","visible");
        }
	if ($(window).width() < 400)
        {
                $('div#app-banner').css("visibility","hidden");
        }
	$(window).resize(function() {
		if ($(window).width() < 400)
	        {
        	        $('div#app-banner').css("visibility","hidden");
			return;
      		}
                if ($(window).width() < 530)
                {
                        $('div#app-banner').css("visibility","visible");
	                $('#app-banner-view-button').css("visibility","hidden");
                        return;
                }
		$('div#app-banner').css("visibility","visible");
                $('#app-banner-view-button').css("visibility","visible");
	});


});
