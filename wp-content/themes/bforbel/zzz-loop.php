<?php
/**
 * @package WordPress
 * @subpackage B for Bell
 */
$had_ads = 0;
while ( have_posts() ) : the_post(); 
    $src = get_post_image_src();
    if ( ! $src ) continue;
    $lseed = check_if_ajax_loading() and $lseed = "seed-$lseed";
?>
<article class="small-post cf <?php echo $lseed; ?>" data-post-id="<?php the_ID(); ?>">
    <div class="sticky-area">
      <header><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></header>
      <footer class="cf">
        <table>
          <tr>
            <td class="border-none border-bottom">
				<div class="fb-share-button fb_iframe_widget" data-href="<?php echo esc_attr(get_permalink()); ?>" data-type="button" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;href=<?php echo urlencode(get_permalink()); ?>&amp;locale=en_US&amp;sdk=joey&amp;type=button"><span style="vertical-align: bottom; width: 56px; height: 20px;"><iframe name="fffffffff" width="1000px" height="1000px" frameborder="0" allowtransparency="true" scrolling="no" title="fb:share_button Facebook Social Plugin" src="http://www.facebook.com/plugins/share_button.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D28%23cb%3Df3d11b3198%26domain%3D<?php echo  get_site_url(); ?>%26origin%3D<?php echo get_site_url(); ?>%252Ffce769b68%26relation%3Dparent.parent&amp;href=<?php echo urlencode(get_permalink()); ?>&amp;locale=en_US&amp;sdk=joey&amp;type=button" style="border: none; visibility: visible; width: 56px; height: 20px;" class=""></iframe></span></div>

			</td>
            <td class="fckn-pinit border-bottom">
           		<div class='share_box' style="width:40px; height:22px;margin-left:30px;">
						<a href="http://www.pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink()); ?>&media=<?php echo urlencode(get_post_image_src()); ?>&description=<?php echo urlencode(html_entity_decode(get_the_title(), ENT_NOQUOTES, 'UTF-8')); ?>" data-pin-do="buttonPin" data-pin-config="beside">
				        <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
				    	</a>                

          		</div>
            </td>
          </tr>
          <tr>
	          <td class="border-none"><div class='share_box' style="width:60px; height:22px; background:url('http://w.sharethis.com/images/twitter_counter.png');"><span class='st_twitter_hcount' st_via="bforbel" st_title='<?php the_title(); ?>' st_url='<?php echo esc_attr(get_permalink()); ?>' displayText='share'></span></div></td>
              <!-- td class="last-cell"><div class='share_box' style='height:20px;'><span class='st_tumblr_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText=' '></span></div></td -->
              <td class="last-cell" style="text-align: center;">
                  <div style="width:64px;height:18px; background: url('/wp-content/themes/b-for-bell/img/stumbla_button_img.png'); display:inline-block;">
                      <iframe scrolling="no" frameborder="0" allowtransparency="true" src="http://badge.stumbleupon.com/badge/embed/2/?url=<?php echo urlencode(the_permalink()); ?>" width="65" height="18" id="iframe-stmblpn-widget-1" style="overflow: hidden; margin: 0px; padding: 0px; border: 0px;"></iframe>
                  </div>
              </td>
          </tr>
        </table>
      </footer>
      <div class="short-content"><?php echo short_content(); ?></div>
      <a href="<?php the_permalink(); ?>" class="btn-more">more</a>
    </div>
    <?php render_img_content(get_permalink(), esc_attr($GLOBALS['post']->post_title), get_post_image_src()); ?>    
    <?php if (bfb_is_mobile_view()): ?>
	    <ul class="mob-share cf">
			<li><div class='share_box'>
				<div class="fb-share-button fb_iframe_widget" data-href="<?php echo esc_attr(get_permalink()); ?>" data-type="button" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;href=<?php echo urlencode(get_permalink()); ?>&amp;locale=en_US&amp;sdk=joey&amp;type=button"><span style="vertical-align: bottom; width: 56px; height: 20px;"><iframe name="fffffffff" width="1000px" height="1000px" frameborder="0" allowtransparency="true" scrolling="no" title="fb:share_button Facebook Social Plugin" src="http://www.facebook.com/plugins/share_button.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D28%23cb%3Df3d11b3198%26domain%3D<?php echo get_site_url(); ?>%26origin%3D<?php echo get_site_url(); ?>%252Ffce769b68%26relation%3Dparent.parent&amp;href=<?php echo urlencode(get_permalink()); ?>&amp;locale=en_US&amp;sdk=joey&amp;type=button" style="border: none; visibility: visible; width: 56px; height: 20px;" class=""></iframe></span></div>

			</div></li>
			
	      <li class="fckn-pinit"><a target="_blank" href="//pinterest.com/pin/create/button/?<?php echo 'url=', urlencode(get_permalink()),'&media=',urlencode(get_post_image_src()), '&description=',urlencode(html_entity_decode(get_the_title(), ENT_NOQUOTES, 'UTF-8')); ?>" data-pin-do="buttonPin" ><img src="http://assets.pinterest.com/images/PinExt.png" /></a></li>
          <li class="stumbla"><iframe scrolling="no" frameborder="0" allowtransparency="true" src="http://badge.stumbleupon.com/badge/embed/2/?url=<?php echo urlencode(the_permalink()); ?>" width="65" height="18" id="iframe-stmblpn-widget-1" style="overflow: hidden; margin: 0px; padding: 0px; border: 0px;"></iframe></li>
          <li><div class='share_box'><span class='st_twitter_hcount' st_via="bforbel" st_title='<?php the_title(); ?>' st_url='<?php echo esc_attr(get_permalink()); ?>' displayText='share'></span></div></li>
	    </ul>
	    <div class="facebook-comment-container" <?php if ( bfb_is_desktop_view() ) echo 'style="display: none;"'; ?>>
	        <a href="#toggle-fb-comments" data-fb-block="<?php the_ID(); ?>" class="fb-comments-toggle toggle-next">View Comments</a>
	        <div class="fb-com-container" style="display: none;">
	            <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="<?php echo bfb_is_mobile_view() ? 556 : 450; ?>" data-num-posts="3" <?php if (bfb_is_mobile_view()) echo 'data-mobile="false"'; ?>></div>
	        </div>
	    </div>
	<?php endif; ?>
</article>
<?php
if ( 3 > $had_ads++ and bfb_is_mobile_view() )
   if (bfb_browser_need_wap()) :
      adsense_wap();
   else:
   ?>
   <aside id="mobile-ads" class="compact-only">
     <?php dynamic_sidebar('Header Bar'); ?>
   </aside>
   <?php
endif;
    endwhile; 
    $next = next_posts($GLOBALS['wp_query']->max_num_pages,false); 
    if ( $next ) : ?><a id="load-more-posts" href="<?php  
echo remove_query_arg('load_ajax_posts',preg_replace("/\#0?38;/ims","",$next)); ?>">Show even more posts</a> <?php endif; ?>