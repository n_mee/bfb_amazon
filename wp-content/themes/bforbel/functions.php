<?php
/*
 * @package WordPress
 * @subpackage B for Bel
 */
 
if ( !empty($_GET['m']) and $_GET['m'] == '1' ) :
	setcookie('forced_dt_view_n','no',time() + 60*60*24*30,'/');
    $_COOKIE['forced_dt_view_n'] = 'no';
    wp_safe_redirect(remove_query_arg('m'),301);
    exit;
endif;

if ( preg_match("#/category/([^/]+)/?#i", urldecode($_SERVER['REQUEST_URI']), $mtch) and strpos($mtch[1], " ") !== false) :
    //if (is_user_logged_in()) die("/category/" + preg_replace("#-+#i","-",preg_replace("/[^a-z0-9]/i","-",$mtch[1])));
    status_header(301);
    wp_redirect(get_bloginfo('url') . "/category/" . preg_replace("#-+#i","-",preg_replace("/[^a-z0-9]/i","-",$mtch[1])));
    exit;
endif;

if ( stripos($_SERVER['REQUEST_URI'], "/search") === 0 ) :
    if ( isset($_GET['updated-max']) and $time = str_replace('T', ' ', substr($_GET['updated-max'], 0, 19)) and $ttime = strtotime($time)) :
		if ( $time == '2012-05-15 22:51:00' ) :
			$url = '/2012/05/tips-tricks-how-to-grow-out-your-hair.html'; // As per clients request
		else :
        	$url = date('/Y/m', $ttime);
		endif; 
    else:
        $url = '';
    endif;
    status_header(301);
    wp_redirect(get_bloginfo('url') . $url);
    exit;
endif;

/**
 * Check for mobile or desktop 
 */
function bfb_is_mobile_agent() {
	if(isset($_GET['fview'])) return true;
    if(!isset($GLOBALS['bfb_mobile_agent'])):
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        $GLOBALS['bfb_mobile_agent'] = preg_match('/(android|bb\d+|meego).+mobile|nexus|galaxy|android|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
    endif;
    return $GLOBALS['bfb_mobile_agent'];
}

function bfb_is_mobile_view() {
	if(isset($_GET['fview']) and 'm' == $_GET['fview']) return true;
	if(isset($_GET['fview']) and 'd' == $_GET['fview']) return false;
    return bfb_is_mobile_agent() && $_COOKIE['forced_dt_view_n'] != 'yes';
}

function bfb_is_desktop_view() {
	if(isset($_GET['fview']) and 'd' == $_GET['fview']) return true;
	if(isset($_GET['fview']) and 'm' == $_GET['fview']) return false;
    return !bfb_is_mobile_view();
}

function bfb_browser_need_wap() {
   if (!bfb_is_mobile_agent()) return false;
   if (stripos($_SERVER['HTTP_USER_AGENT'],'BlackBerry') !== false) return true;
   if (stripos($_SERVER['HTTP_USER_AGENT'],'BB10;') !== false) return true;
   if (stripos($_SERVER['HTTP_USER_AGENT'],'Windows Phone') !== false) return true;
   if (stripos($_SERVER['HTTP_USER_AGENT'],'IEMobile/') !== false) return true;
   return false;
}

  
define ( 'HV_THEME_URL', get_bloginfo('template_url') );
define ( 'SITE_URL', get_bloginfo('url') );
add_editor_style();					// Allows editor-style.css to configure editor visual style.
add_theme_support('post-thumbnails');

register_sidebar(array(
	'name' => 'Main Sidebar',
	'before_widget' => '<section id="%1$s" class="cf widget-container widget-box %2$s">',
	'after_widget' => '</section>',
	'before_title' => '<header><h3 class="widget-title">',
	'after_title' => '</h3></header>',
) );

register_sidebar(array(
	'name' => 'Single Post Sidebar',
	'description' => 'The secondary widget area',
    'before_widget' => '<section id="%1$s" class="cf widget-container widget-box %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<header><h3 class="widget-title">',
    'after_title' => '</h3></header>',
) );

register_sidebar(array(
    'name' => 'Header Bar',
    'description' => 'Area above menu',
    'before_widget' => '<section id="%1$s" class="cf widget-container widget-box %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<header><h3 class="widget-title">',
    'after_title' => '</h3></header>',
));

register_nav_menus( array(
	'main' => 'Main Navigation Menu',
	'secondary' => 'Secondary navigation Menu'
) );

function short_content($content = false,$sz = 100,$more = '...') {
    if ( false === $content ) $content = get_the_content();
	$content = strip_shortcodes($content);
    $content = preg_replace("#<a[^>]*>\s*<img[^>]*>\s*</a>?#ims", "",$content);
    $content = preg_replace("#\s*<img[^>]*>\s*#ims", " ",$content);
    $content = preg_replace("#<a[^>]*>\s*</a>#ims", " ",$content);
    $content = preg_replace("#<!--(.*?)-->#ims", "",$content);
    $content = preg_replace("#<script.*?</script>#ims", "", $content);
    $content = preg_replace("#<(/?)div#ims", '<\1span',$content);
    $rlen = strlen($content);
    $len = strlen(strip_tags($content)) or $len = 1;
	if ($len<=$sz) return $content;
    $nsz = $sz;
    do {
        $p = strpos($content, " ",$nsz);
        if (!$p) :
            $s = $content;
            break;
        endif;
    	$s = substr($content, 0, $p);
        $s = preg_replace("#<[^>]*$#i", "", $s);
        $nsz++;
    } while( strlen(strip_tags($s)) < $sz );
    $s = preg_replace('#[\r\n]+#', "\r\n",$s);
    $s.= $more; 
    $tags = array();
    preg_match_all("#</?([^ >]*)[^>]*?/?>#ims",$s,$match);
    $tags = $match[1];
    $lasttag = False;
    $restags = array();
    foreach( $tags as $tag ) :
        if (in_array($tag,array('br','hr'))) continue; 
        if ($lasttag == $tag):
            array_pop($restags); 
            if (count($restags)):
                $lasttag = $restags[count($restags)-1]; 
            else :
                $lasttag = False;
            endif;
            continue;
        endif;
        $restags[] = $lasttag = $tag;
    endforeach;
    foreach ( array_reverse($restags) as $tag ) $s .= "</$tag>";
    return apply_filters("the_content", preg_replace("#(\s*<br[^>]*>\s*)+#ims","\r\n", $s));
}
//error_reporting(E_ALL);
//add_action( 'admin_init', 'fixposts_images' );
function fixposts_images() {
    if ( is_admin() and isset( $_GET['fixposts'] ) ) :
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        foreach( get_posts(array('numberposts'=>9999)) as $pst ) {
            $upd = array( "ID" => $pst->ID);
            if (preg_match_all('/<img[^>]*?src="([^ \"]*)"[^>]*>/ims', $pst->post_content, $match)) {
                foreach( $match[1] as $kk => $urlmatched ) :
                    if ( stripos($urlmatched, get_bloginfo('url')) !== false ) continue;
                    $tmp = download_url( $urlmatched );     
                    // Set variables for storage
                    // fix file filename for query strings
                    preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $urlmatched, $matches );
                    $file_array['name'] = basename($matches[0]);
                    $file_array['tmp_name'] = $tmp;
            
                    // If error storing temporarily, unlink
                    if ( is_wp_error( $tmp ) ) {
                        @unlink($file_array['tmp_name']);
                        $file_array['tmp_name'] = '';
                    }
            
                    // do the validation and storage stuff
                    $id = media_handle_sideload( $file_array, $pst->ID, '' );
                    // If error storing permanently, unlink
                    if ( is_wp_error($id) ) {
                        @unlink($file_array['tmp_name']);
                    } else {
                        if ( ! get_post_meta($pst->ID, '_thumbnail_id', true) )
                            update_post_meta($pst->ID, '_thumbnail_id', $id);
                        if ( $src = wp_get_attachment_image_src($id, 'full') and ! empty($src[0]) )
                            $pst->post_content = str_ireplace($urlmatched, $src[0], $pst->post_content);
                    }
                endforeach;
            }
            $upd['post_content'] = preg_replace( "#<div[^>]*?>(.*?)</div>#ims", '${1}', $pst->post_content);
            wp_update_post($upd);
        }
    endif;
}

function check_if_ajax_loading() {
    if ( empty($_GET['load_ajax_posts']) ) return 0; 
    return (float) $_GET['load_ajax_posts'];
}

/**
function change_ID_order($q) {
    if ( $q->is_main_query ) {
        $q->set('orderby', 'ID');
        $q->set('order', 'DESC');
        if ( check_if_ajax_loading() ) {
           $q->set('paged', '1');
           add_filter('posts_where', 'ajax_max_id_filter'); 
        }
    }
}
function ajax_max_id_filter( $where, $query ) {
   global $wpdb;
   return "($where) AND {$wpdb->posts}.ID < " .check_if_ajax_loading();
}
add_action( 'pre_get_posts', 'change_ID_order' ); 
**/

function is_google() {
    $bots = array(
        'Googlebot', 'Baiduspider', 'ia_archiver',
        'R6_FeedFetcher', 'NetcraftSurveyAgent', 'Sogou web spider',
        'bingbot', 'Yahoo! Slurp', 'facebookexternalhit', 'PrintfulBot',
        'msnbot', 'Twitterbot', 'UnwindFetchor',
        'urlresolver', 'Butterfly', 'TweetmemeBot' );

    foreach($bots as $b){
        if( stripos( $_SERVER['HTTP_USER_AGENT'], $b ) !== false ) return true;
    }
    return false;
}
/*
if ( ! is_user_logged_in() and $GLOBALS['pagenow'] != 'wp-login.php' ) {
    if ( is_google() ) {
        header('HTTP/1.1 403 Forbidden');
        exit;
    }
    if ( isset($_GET['loadtest']) and $_GET['loadtest'] == 1 and stripos($_SERVER['HTTP_REFERER'], "http://www.bforbel.com") === 0 ) {
        $req = substr($_SERVER['HTTP_REFERER'], strlen("http://www.bforbel.com"));
        wp_safe_redirect(add_query_arg("loadtest",2,site_url($req)));
        exit;
    } elseif( isset($_GET['loadtest']) and $_GET['loadtest'] == 2 and stripos($_SERVER['HTTP_REFERER'], "http://www.bforbel.com") === 0 ) {
        timer_start();
    } else {
        header('HTTP/1.1 403 Forbidden');
        exit;
    }
}
*/

add_filter( 'the_content', 'strip_google_divs', 1 );
function strip_google_divs( $content ) {
    return preg_replace( "#<div[^>]*?>(.*?)</div>#ims", '${1}', $content);
}

function get_post_image_src( $id = null, $size = 'full' ) {
    $id or $id = $GLOBALS['post']->ID;
    if ( $tid = get_post_thumbnail_id($id) and $src = wp_get_attachment_image_src($tid, $size) and ! empty($src[0]) ) return $src[0];
    $p = get_post($id);
    if ( $p->post_type == 'post' and preg_match('/<img[^>]*?src="([^ \"]*)"[^>]*>/ims', $p->post_content, $match)) {
       return $match[1];
    }
    return '';
} 

include dirname(__FILE__) . '/widgets.php';

// Fix Slugs

if ( is_admin() and isset($_GET['fixposts'])) :
    global $wpdb;
	set_time_limit(0);
	switch( $_GET['fixposts'] ) :
		
		case "links":
		    foreach( $wpdb->get_results("SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = 'blogger_permalink' LIMIT 9999") as $pst ) :
		        $upd = array( "ID" => $pst->post_id);
		        $slug = explode("/", $pst->meta_value);
		        $slug = explode(".html", array_pop($slug));
		        $upd['post_name'] = $slug[0];
		        wp_update_post($upd);
		    endforeach;
			echo "links updated <br />\r\n";
			break;
		
		case "data":
			//error_reporting(E_ALL);
			ini_set('memory_limit','1024M');
			$cnt = 2615;
		    $ppp = $wpdb->get_results("SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '_blogger_self' LIMIT $cnt, 9999");
		    $tot = count($ppp) + $cnt;
		    foreach( $ppp as $pst ):
		        ++$cnt;
		        $data = array();
		        $xml = @simplexml_load_file($pst->meta_value);
				if ( $xml and ! empty($xml->content)):
		            $c = (string) $xml->content;
		            preg_match_all( "#<form.*?</form>#ims", $c, $forms);
		            $fc = 0;
		            $fc = 0;
		            foreach( $forms[0] as $chunk ) :
		                $sc = 'form-' .(++$fc);
		                $data[$sc] = $chunk;
		                $c = str_ireplace($chunk, "[blogger-imported $sc]", $c);
		            endforeach;
		            preg_match_all( "#<script.*?</script>#ims", $c, $script);
		            $fc = 0;
		            foreach( $script[0] as $chunk ) :
		                $sc = 'script-' .(++$fc);
		                $data[$sc] = $chunk;
		                $c = str_ireplace($chunk, "[blogger-imported $sc]", $c);
		            endforeach;
		            if ( ! empty($data) ) update_post_meta($pst->post_id, 'imported_content', $data);
		            $upd = array( "ID" => $pst->post_id, "post_content" => $c );
		            wp_update_post($upd);
		            echo "$cnt/$tot Post Updated<br />\r\n";
		        endif;
		    endforeach;
			break;
	endswitch;
endif;

add_shortcode( "blogger-imported", "insert_blogger_import" );
function  insert_blogger_import( $data ) {
    $d = (array) get_post_meta(get_the_ID(), 'imported_content', 1);
    if ( empty($d[$data[0]]) ) return '';
    return $d[$data[0]];
}

function featuredtoRSS($content) {
	global $post;
	if ( has_post_thumbnail( $post->ID ) ){
		$content = get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'style' => 'float:left; margin:0 15px 15px 0;' ) ) . '' . $content;
	}
	return $content;
}
add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');

function adsense_wap()
{
   include 'adsense_wap.php';
}


//json api controllers.
function add_bfb_controller($controllers) {
  $controllers[] = 'bfb';
  return $controllers;
}
add_filter('json_api_controllers', 'add_bfb_controller');

function set_bfb_controller_path() {
  return get_template_directory() . "/json_api_controllers/bfb.php";
}
add_filter('json_api_bfb_controller_path', 'set_bfb_controller_path');


//render img content. used in loop
function render_img_content($pLink, $pTitle, $pImgSrc)
{
    $file_parts = pathinfo($pImgSrc);
    
    //if is gif
    if (strcasecmp('gif', $file_parts['extension']) == 0) {
        $staticImgSrc = $file_parts['dirname'] . '/' . $file_parts['filename'] . '.png';
  //      var_dump($file_parts);
  //      var_dump($staticImgSrc);
        echo "  <figure>
                        <div id='bfb_gif' class='bfb_gif' onclick='doGif(this);'>
                            <img id='gif' width='556' alt='" . $pTitle . "' src='" . $pImgSrc . "' style='display:none'/>
                            <img id='static' width='556' alt='" . $pTitle . "' src='" . $staticImgSrc . "' />
                            <span id='span'></span>
                        </div>
                    </figure>";
    }
    //else
    else{
        echo "  <figure>
                        <a href='" . $pLink . "'>
                            <img width='556' alt='" . $pTitle . "' src='" . $pImgSrc . "' />
                        </a>
                    </figure>";
    }
}

function bfb_device_from_useragent()
{
	//rudimentary
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$devices = array (
		'ipad' => 'ipad',
                'ipod' => 'ipod',
		'iphone' => 'iphone',
		'android' => 'android',
	);
	$device = '';
	foreach ($devices as $device_str => $device_tag)
	{
		if (stripos($user_agent,$device_str) !== FALSE)
		{
			$device = $device_tag;
			break;
		}
	}
	return $device;
}


