<?php

/**
 * Environment specific functions and variables.
 *
 * @package WordPress
 * @subpackage B For Bel
 */

/**
 * Static class for environment specific functions and variables.
 *
 * @package WordPress
 * @subpackage B For Bel
 * @author Nick_Mee
 */


    

class BFB_Config {

    
    /**
	 * Display META tag to prevent indexing in stage
	 *
	 * @access public
    */
    public static function stage_noindex_meta()
    {
        if (defined('BFB_ENV') && BFB_ENV == 'STAGE')
            echo '<meta name="robots" content="noindex" />';
    }



	/**
	 * Uses the POST HTTP method.
	 *
	 * Used for sending data that is expected to be in the body.
	 *
	 * @access public
	 * @since 2.7.0
	 *
	 * @param string       $url  URI resource.
	 * @param string|array $args Optional. Override the defaults.
	 * @return array|object Array containing 'headers', 'body', 'response', 'cookies', 'filename'. A WP_Error instance upon error
	 */


}

