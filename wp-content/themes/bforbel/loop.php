<?php
/**
 * @package WordPress
 * @subpackage B for Bell
 */


//vote plugin start
if (function_exists('bfb_enqueue_vote_script')) { 
    bfb_enqueue_vote_script(); 
}
if (!function_exists('bfb_vote_click_parameter')) { 
    //prevent error if vote plugin deactivated
    function bfb_vote_click_parameter($post_id,$vote_status) {return '';} 
}
if (!function_exists('bfb_vote_container_status')) { 
    //prevent error if vote plugin deactivated
    function bfb_vote_container_status($post_id) {return '';} 
}

$vote_href = "#popup2";
$vote_lightbox = " lightbox";
if(is_user_logged_in())
{
    $vote_href = "#";
    $vote_lightbox = "";
}
//vote plugin end

//bookmark plugin
if (function_exists('bfb_enqueue_bookmark_script')) { 
    bfb_enqueue_bookmark_script(); 
}
if (!function_exists('bfb_bookmark_click_parameter')) { 
    //prevent error if vote plugin deactivated
    function bfb_bookmark_click_parameter($post_id) {return '';} 
}
if (!function_exists('bfb_bookmark_link_status')) { 
    //prevent error if vote plugin deactivated
    function bfb_bookmark_link_status($post_id) {return '';} 
}
$bookmark_href = "#popup2";
$bookmark_class = " lightbox";
if(is_user_logged_in())
{
    $bookmark_href = "#";
    $bookmark_class = "";
}
//bookmark plugin ends


?>

<body class="home">
	<div class="wrap-holder">
		<!-- main container of all the page elements -->
		<div id="wrapper">
			<!-- header of the page -->
			<header id="header">
				<div class="holder">
					<!-- page logo -->
					<strong class="logo"><a href="#">B for Bel</a></strong>
					<div class="section">
						<div class="popup-holder1">
							<a class="open" href="#">menu</a>
							<div class="popup">
								<div class="categories">
									<ul>
										<li><a href="#">Humor &amp; Fun</a></li>
										<li><a href="#">Beauty &amp; Fashion</a></li>
										<li><a href="#">Ideas &amp; Inspiration</a></li>
									</ul>
									<ul>
										<li><a href="#">Products &amp; Gifts</a></li>
										<li><a href="#">Food</a></li>
										<li><a href="#">Games</a></li>
										<li><a href="#">Tips &amp; Tricks</a></li>
									</ul>
								</div>
								<ul class="nav">
									<li><a href="#">About</a></li>
									<li><a href="#">Privacy &amp; Terms</a></li>
									<li><a href="#">Copyright &amp; Trademark</a></li>
								</ul>
							</div>
						</div>
						<!-- search form -->
						<form action="#" class="search-form">
							<fieldset>
								<input type="submit" value="search" >
								<input type="text" >
							</fieldset>
						</form>
					</div>
					<div class="section r">
						<ul class="social-list">
							<li><a href="#">facebook</a></li>
							<li><a href="#" class="twitter">twitter</a></li>
							<li><a href="#" class="pinterest">pinterest</a></li>
						</ul>
						<a href="#" class="upload-btn">upload</a>
						<div class="login-block"><a href="#popup2" class="lightbox">Log In</a> / <a href="#popup3" class="lightbox">Sign Up</a></div>
					</div>
				</div>
			</header>
                        <div class="container">
				<div class="inner">
					<h1>BforBel is an Internet Party.</h1>
					<p>(Bring Food, Cats and Moves Like Jagger.)</p>
					<a href="#popup3" class="btn lightbox">Join Now</a>
				</div>
			</div>
                        <div class="w1">
				<div class="gallery-block">
					<h2><span>FEATURED</span></h2>
					<div class="gallery-holder">
						<div class="mask">
							<div class="slideset">
								<div class="slide">
									<img src="wp-content/themes/bforbel/images/img1.jpg" alt="image description" width="362" height="255" >
									<img src="wp-content/themes/bforbel/images/img2.jpg" alt="image description" width="343" height="255" >
									<img src="wp-content/themes/bforbel/images/img3.jpg" alt="image description" width="317" height="255" >
								</div>
							</div>
						</div>
					</div>
					<div class="textholder">
						<span>What if your favorite Disney Princesses had Instagram?</span>
						<a href="#" class="btn">View Post</a>
					</div>
					<div class="pagination">
						<ul>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li class="active"><a href="#">5</a></li>
						</ul>
					</div>
				</div>
				<!-- contain main informative part of the site -->
				<div id="main">
					<div class="ajax-holder">
						<!-- contain the main content of the page -->
						<div id="content" class="left-side">
        
 
<?php
while ( have_posts() ) : 
    the_post(); 
    $src = get_post_image_src();
    if ( ! $src ) continue;
    ?>
    <div class="block">
            <div class="btn-holder">
                    <a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a>                    
                    <a id="bfb-bookmark-link-<?php echo $post->ID?>" href="<?php echo $bookmark_href; ?>" class="bfb-bookmark-link<?php echo $bookmark_class; ?><?php echo bfb_bookmark_link_status($post->ID); ?>" <?php echo bfb_bookmark_click_parameter($post->ID)?> >
                        <div class="visual">
                            <img src="<?php echo $src; ?>" alt="<?php the_title(); ?>" width="451" >
                            <div class="mask"> &nbsp;
                            </div>                                                                   
                        </div>
                    </a>
                                    
                    <div id="bfb-vote-container-<?php echo $post->ID?>" class="vote<?php echo bfb_vote_container_status($post->ID); ?>">
                            <a href="<?php echo $vote_href; ?>" class="up<?php echo $vote_lightbox; ?>" <?php echo bfb_vote_click_parameter($post->ID, 'up')?> >up</a>
                            <a href="<?php echo $vote_href; ?>" class="down<?php echo $vote_lightbox; ?>" <?php echo bfb_vote_click_parameter($post->ID, 'down')?> >down</a>
                    </div>

            </div>
            <ul class="social-list1">
                    <li><a href="#">facebook</a></li>
                    <li><a href="#" class="facebook">facebook</a></li>
                    <li><a href="#" class="twitter">twitter</a></li>
                    <li><a href="#" class="pinterest">pinterest</a></li>
                    <li><a href="#" class="mail">mail</a></li>
            </ul>
    </div>
    <?php

endwhile;
?>


						</div>
						<!-- contain sidebar of the page -->
						<aside id="sidebar" class="right-side">
							<div class="block">
								<div class="fixed-holder">
									<div class="block1 fixed-box">
										<span>Advertisement</span>
										<img src="wp-content/themes/bforbel/images/img4.jpg" alt="image description" width="267" height="224" >
									</div>
								</div>
								<h2>HOT TOPICS</h2>
								<ul class="items-list">
									<li>
										<a href="#">
											<img src="wp-content/themes/bforbel/images/img5.jpg" alt="image description" width="265" height="104" >
											<span>Are You Mocking Me, Sir?</span>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="wp-content/themes/bforbel/images/img6.jpg" alt="image description" width="270" height="122" >
											<span>6 Things That Every Girl Hates</span>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="wp-content/themes/bforbel/images/img7.jpg" alt="image description" width="268" height="121" >
											<span>What Are You Looking At?</span>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="wp-content/themes/bforbel/images/img8.jpg" alt="image description" width="268" height="120" >
											<span>The Power Of Make Up</span>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="wp-content/themes/bforbel/images/img9.jpg" alt="image description" width="268" height="114" >
											<span>14 Things Every Coffee Lover Should Know</span>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="wp-content/themes/bforbel/images/img10.jpg" alt="image description" width="268" height="94" >
											<span>14 Craziest College Traditions</span>
										</a>
									</li>
								</ul>
							</div>
						</aside>
						<img src="wp-content/themes/bforbel/images/loading1.gif" class="loader" alt="image description" width="45" height="45" >
						<a href="wp-content/themes/bforbel/images/content-index1-1.html" class="load-more">load more</a>
					</div>
				</div>
			</div>
                        			<!-- footer of the page -->
			<footer id="footer">
				<div class="footer-holder">
					<div class="promobox"><a href="#"><img src="wp-content/themes/bforbel/images/banner.jpg" alt="image description" width="702" height="87" ></a></div>
					<div class="holder">
						<!-- navigation of the page -->
						<nav class="add-nav">
							<ul>
								<li><a href="#">About</a></li>
								<li><a href="#">Privacy &amp; Terms</a></li>
								<li><a href="#">Advertise</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
						</nav>
						<p class="copyright"><a href="#">&copy; Bforbel Media 2014</a></p>
					</div>
				</div>
			</footer>
			<a href="#" class="btn-top">top</a>
		</div>
	</div>
	<div class="popup-holder">
		<div id="popup1" class="lightbox">
			<h2>Would you like to...</h2>
			<div class="upload-block">
				<div class="block">
					<a href="#">
						<span>Upload an image?</span>
						<img src="wp-content/themes/bforbel/images/img12.jpg" alt="image description" width="153" height="115" >
						<div class="mask">&nbsp;</div>
					</a>
				</div>
				<span class="sep">or</span>
				<div class="block">
					<a href="#">
						<span>Make a Benjamin Meme?</span>
						<img src="wp-content/themes/bforbel/images/img13.jpg" alt="image description" width="138" height="158" >
						<div class="mask">&nbsp;</div>
					</a>
				</div>
			</div>
		</div>
		<div id="popup2" class="lightbox">
			<h2>Log In</h2>
			<div class="holder">
				<p>Connect with a social network</p>
				<a href="#" class="btn-facebook">Login with Facebook</a>
				<span class="or">Or <a href="#popup3" class="lightbox">Sign Up</a> for an account</span>
				<p>Log in with your email address:</p>
				<form action="#" class="login-form">
					<fieldset>
						<div class="row">
							<label for="email">Email</label>
							<input id="email" type="email" >
						</div>
						<div class="row">
							<label for="password">Password</label>
							<input id="password" type="password" >
							<a href="#popup4" class="lightbox">Forgot your password?</a>
						</div>
						<input type="submit" value="Log In">
					</fieldset>
				</form>
			</div>
		</div>
		<div id="popup3" class="lightbox">
			<h2>Sign Up- Become a Belly Button</h2>
			<div class="holder">
				<p>BforBel is an internet party. <br>We love cats, food, Disney Princesses and important, political issues like giving dogs eyebrows.</p>
				<a href="#" class="btn-facebook">Login with Facebook</a>
				<p>
					<span>Sign up with your <a class="lightbox" href="#popup5">Email Address</a></span>
					<span>Have an account? <a href="#popup2" class="lightbox">Login</a></span>
				</p>
			</div>
		</div>
		<div id="popup4" class="lightbox">
			<h2>Reset Password</h2>
			<div class="holder">
				<form action="#" class="survey">
					<fieldset>
						<div class="row">
							<label for="email1">Enter your email below</label>
							<input id="email1" type="email" >
						</div>
						<input type="submit" value="Submit">
					</fieldset>
				</form>
				<p>Thank you! You will receive an email with further instructions.</p>
			</div>
		</div>
		<div id="popup5" class="lightbox">
			<h2>Become a Belly Button</h2>
			<div class="holder1">
				<form action="#" class="login-form">
					<fieldset>
						<div class="row">
							<label for="name">Full Name</label>
							<input id="name" type="text" >
						</div>
						<div class="row">
							<label for="email2">Email Address</label>
							<input id="email2" type="email" >
						</div>
						<div class="row">
							<label for="password1">Password</label>
							<input id="password1" type="password" >
						</div>
						<div class="visual1"><img src="wp-content/themes/bforbel/images/img16.jpg" alt="image description" width="345" height="99" ></div>
						<div class="row long">
							<label for="words">Enter The Words Above</label>
							<input id="words" type="text" >
						</div>
						<input type="submit" value="Sign Up">
					</fieldset>
				</form>
			</div>
		</div>
	</div>
    <?php wp_footer(); ?>
</body>