<?php
class HV_Widget_Recent_Posts extends WP_Widget {

    function __construct() {
        $widget_ops = array('classname' => 'widget-posts' );
        parent::__construct('related-posts', __('Related Posts'), $widget_ops);
    }
    
    function widget($args, $instance) {
        if ( empty( $GLOBALS['post']->ID ) ) return;
        $transName = 'related-widget-' . $GLOBALS['post']->ID;
        //$data = get_transient($transName);
        if ( ! empty($data) ) {
            echo $data;
            return;
        }
        
        ob_start();
        if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) ) $number = 10;
        $ids = array();
        
        foreach( get_the_terms( $GLOBALS['post']->ID, 'category' ) as $cat ) $ids[] = $cat->term_id;
        $ids = get_objects_in_term($ids, array('category'));
        if ( false !== ($_t = array_search($GLOBALS['post']->ID, $ids) ) ) unset($ids[$_t]);
	    if(($_num = (int) get_field('related_posts')) > 0) $number = $_num;
        $r = get_posts(array(
            'posts_per_page'=> $number, 
            'no_found_rows' => true, 
            'post_status'   => 'publish', 
            'ignore_sticky_posts'=> true,
            'post__in' => $ids,
        ));
        extract($args);
        extract($instance);
        echo $before_widget; 
        if ( $title ) echo $before_title . $title . $after_title; 
            echo '<ul>'; 
            foreach( $r as $pst ) :
                $prm = get_permalink($pst->ID);
                echo '<li><figure><a href="',$prm,'"><img src="',get_post_image_src($pst->ID,'thumbnail'),'" alt="',esc_attr($pst->post_title),'" /></a><figcaption><a href="',$prm,'">',$pst->post_title,'</a></figcaption></figure></li>';
            endforeach;
            echo '</ul>';
        echo $after_widget;
        $data = ob_get_flush();
        set_transient($transName, $data, time() + 600 );
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = (int) $new_instance['number'];
        return $instance;
    }


    function form( $instance ) {
        $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        ?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
    
            <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
        <?php
    }
    
    function _registerSelf() {
        register_widget(__CLASS__);
    }
}

add_action( 'widgets_init', array('HV_Widget_Recent_Posts', '_registerSelf') );

class HV_Widget_Follow extends WP_Widget {

    function __construct() {
        $widget_ops = array('classname' => 'widget-share' );
        parent::__construct('follow-buttons', __('Follow'), $widget_ops);
    }
    
    function widget($args, $instance) {
        $transName = 'follow-widget';
        $data = get_transient($transName);
        if ( ! empty($data) ) {
            echo $data;
            return;
        }
        ob_start();
        extract($args);
        echo $before_widget;
        ?>
        <ul class="cf">
          <li><a href="http://pinterest.com/bforbel/"><img src="<?php echo HV_THEME_URL; ?>/img/pinterest.png" alt=" "></a></li>
          <li>
            <a href="https://twitter.com/bforbel" class="twitter-follow-button" data-show-count="false">Follow @bforbel</a>
          </li>
          <li><a target="_blank" href="http://tumblr.com/follow/cartoonclosets" onclick="javascript:_gaq.push(['_trackevent','outbound-article','http://tumblr.com']);"><img src="<?php echo HV_THEME_URL; ?>/img/tumblr.gif" alt=" "></a></li>
        </ul>
        <?php        
         
//        if ( $title ) echo $before_title . $title . $after_title; 
        echo $after_widget;
        $data = ob_get_flush();
        set_transient($transName, $data, time() + 600 );
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        return $instance;
    }


    function form( $instance ) {}
    
    function _registerSelf() {
        register_widget(__CLASS__);
    }
}

add_action( 'widgets_init', array('HV_Widget_Follow', '_registerSelf') );

