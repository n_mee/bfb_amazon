<?php
/**
 * @package WordPress
 * @subpackage B for Bell
 */
the_post();
get_header(); ?>
<section id="content" class="cf">
    <div class="left-page">
        <article class="small-post cf" data-post-id="<?php the_ID(); ?>">
            <h1>Not Found</h1>
            <br />
            <?php get_search_form(); ?>
        </article>
        <script type="text/javascript">
            document.getElementById('s') && document.getElementById('s').focus();
        </script>
    </div>
    <?php get_sidebar(); ?>
</section>
<?php get_footer(); ?>