<?php
/**
 * @package WordPress
 * @subpackage B for Bell
 */
session_start();

//define BFB_ENV - DEV STAGE LIVE
require_once('bfb-config.php');
require_once('class-bfb-config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
        <!-- Dont allow indexing if stage environment -->
        <?php BFB_Config::stage_noindex_meta(); ?>
        
	<!-- set the encoding of your site -->
	<meta charset="utf-8">
	<title>B4B</title>
	
        
        <?php
            //todo - sort out wp_enqueue for different environments, load times- diff versions of jquery
            wp_enqueue_style('all_css','/wp-content/themes/bforbel/css/markup/all.css',array(),null);
            wp_enqueue_script('jquery');
            wp_enqueue_script('min_jquery','/wp-content/themes/bforbel/js/markup/jquery-1.8.3.min.js',array(),null);
            wp_enqueue_script('main_js','/wp-content/themes/bforbel/js/markup/jquery.main.js',array(),null);
	?>
        
	<!-- include HTML5 IE enabling script and stylesheet for IE -->
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie.css" >
		<script type="text/javascript" src="js/ie.js"></script>
	<![endif]-->
        
        <?php
            wp_head();
	?>
        
</head




			
			