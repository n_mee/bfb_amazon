<?php
/**
 * Template Name:Submit Form
 */
wp_deregister_script('jquery');
wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
wp_register_script('wp-jquery-ui-emf', 'http://www.emailmeform.com/builder/js/jquery-ui-1.7.2.custom.min.js');
wp_enqueue_script('emf-formbuilder', 'http://www.emailmeform.com/builder/js/dynamic.php?t=post&t2=0&use_CDN=true', array('jquery-ui-dialog'));
wp_enqueue_style('emf-builder', 'http://www.emailmeform.com/builder/styles/dynamic.php?t=post'); 
wp_enqueue_style('emf-theme', 'http://www.emailmeform.com/builder/theme_css/1lYDbSbA5C4mwUW58', array('wp-jquery-ui-emf'));
the_post();
get_header(); ?>
<section id="content" class="cf">
    <div class="left-page">
        <article class="submit-post small-post cf" data-post-id="<?php the_ID(); ?>">
<script type="text/javascript" src="http://www.emailmeform.com/builder/js/dynamic.php?t=post&t2=0&use_CDN=true"></script>
<div id="emf-container-outer">
  <div id="emf-container" style="width:500px">
    <div id="emf-logo">
      <a>EmailMeForm</a>
    </div>
    <div class="emf-error-message" style='display:none'></div>
    <form id="emf-form" target="_self" class="leftLabel" enctype="multipart/form-data" method="post" action="http://www.emailmeform.com/builder/form/1lYDbSbA5C4mwUW58">
      <div id="emf-form-instruction" class="emf-head-widget">
        <div id="emf-form-description">
          I have something to share!
        </div>
      </div>
      <ul>
        <li id="emf-li-0" class="emf-li-field emf-field-file data_container" style="text-align:left">
          <label class="emf-label-desc" for="element_0">Image Upload</label>
          <div class="emf-div-field">
            <input id="element_0" name="element_0" onchange="$(this).blur()" class="validate[optional,funcCall[check_file_error[null,2M]]]" type="file" />
          </div>
          <div class="emf-clear"></div>
        </li><li id="emf-li-1" class="emf-li-field emf-field-text data_container" style="text-align:left">
          <label class="emf-label-desc" for="element_1">Name</label>
          <div class="emf-div-field">
            <input id="element_1" name="element_1" value="" size="30" type="text" class="validate[optional]" />
          </div>
          <div class="emf-clear"></div>
        </li><li id="emf-li-2" class="emf-li-field emf-field-email data_container" style="text-align:left">
          <label class="emf-label-desc" for="element_2">Email (will not be shown)</label>
          <div class="emf-div-field">
            <input id="element_2" name="element_2" class="validate[optional,custom[email]]" value="" size="30" type="text" />
          </div>
          <div class="emf-clear"></div>
        </li><li id="emf-li-3" class="emf-li-field emf-field-radio data_container" style="text-align:left">
          <label class="emf-label-desc" for="element_3">Do you know who created this image?</label>
          <div class="emf-div-field">
            <div class="one_column">
              <input id="element_3_0" name="element_3" value="Yes" class="validate[optional]" type="radio" /><label class="padleft-w5" for="element_3_0">Yes</label>
            </div>
            <div class="one_column">
              <input id="element_3_1" name="element_3" value="No" class="validate[optional]" type="radio" /><label class="padleft-w5" for="element_3_1">No</label>
            </div>
            <div class="emf-clear"></div>
          </div>
          <div class="emf-clear"></div>
        </li><li id="emf-li-4" class="emf-li-field emf-field-text data_container" style="text-align:left">
          <label class="emf-label-desc" for="element_4">If yes, who's the creator?</label>
          <div class="emf-div-field">
            <input id="element_4" name="element_4" value="" size="30" type="text" class="validate[optional]" />
          </div>
          <div class="emf-clear"></div>
        </li><li id="emf-li-5" class="emf-li-field emf-field-url data_container" style="text-align:left">
          <label class="emf-label-desc" for="element_5">Website to give you credit (optional)</label>
          <div class="emf-div-field">
            <input id="element_5" name="element_5" class="validate[optional,funcCall[url_ex,optional]]" value="" size="30" type="text" />
          </div>
          <div class="emf-clear"></div>
        </li><li id="emf-li-captcha">
          <div id="emf-captcha-top-settings">
            Image Verification
          </div>
          <div id="emf-captcha-bottom-settings">
            <div id="emf-captcha-image-settings">
              <img id="captcha_image" src="http://www.emailmeform.com/builder/captcha/index/1078e820b88075875d4c12e0056b05ad" alt="captcha" onload="on_captcha_image_load();" />
            </div>
            <div id="emf-captcha-input-settings">
              <div>
                Please enter the text from the image:
              </div><input type="text" id="captcha_code" name="captcha_code" maxlength="10" size="10" class="validate[required,funcCall[valid_captcha]]" /> [<a id="captcha_code_refresh" href=
              "javascript:void(0);" onclick=
              "document.getElementById('captcha_image').src = 'http://www.emailmeform.com/builder/captcha/index/'+Math.random()+'?PHPSESSID='+g_emf_session_id;return false;">Refresh Image</a>]
              [<a id="captcha_code_about" href="javascript:void(0);" onclick=
              "window.open('http://www.emailmeform.com/captcha-instruction.html','_blank','width=400, height=500, left=' + (screen.width-450) + ', top=100');return false;">What's This?</a>]
            </div>
            <div class="emf-clear"></div>
          </div>
          <div class="emf-clear"></div>
        </li><li id="emf-li-post-button" class="left">
          <input value="Submit" type="submit" onmouseover="return true;" />
        </li>
      </ul><input name="element_counts" value="6" type="hidden" /> <input name="embed" value="forms" type="hidden" />
      <div style="margin-top:18px;text-align:center">
        <font face="Franklin Gothic Medium" size="2" color="#000000">Powered by</font><span style="position: relative; padding-left: 3px; bottom: -5px;"><img src=
        "http://www.emailmeform.com/builder/images/footer-logo.png" /></span><font face="Franklin Gothic Medium" size="2" color="#000000">EMF</font> <a style="text-decoration:none;" href=
        "http://www.emailmeform.com/" target="_blank"><font face="Franklin Gothic Medium" size="2" color="#000000">Online Poll</font></a><br />
        <a style="line-height:20px;font-size:70%;text-decoration:none;" href="http://www.emailmeform.com/report-abuse.html?http://www.emailmeform.com/builder/form/1lYDbSbA5C4mwUW58" target=
        "_blank"><font face="Franklin Gothic Medium" size="2" color="#000000">Report Abuse</font></a>
      </div>
    </form>
  </div>
</div><script type="text/javascript">
//<![CDATA[

//var emf_session_timeout_warner=new SessionTimeoutWarner(7200);


EMF_jQuery(window).load(function(){
        post_message_for_frame_height("1lYDbSbA5C4mwUW58");
});

EMF_jQuery(function(){
        EMF_jQuery("#emf-form").validationEngine({
                validationEventTriggers:"blur",
                scroll:true
        });


        EMF_jQuery("#emf-form ul li").mousedown(highlight_field_on_mousedown);
        EMF_jQuery("#emf-form ul li input, #emf-form ul li textarea, #emf-form ul li select").focus(highlight_field_on_focus);

                var form_obj=EMF_jQuery("#emf-container form");
        if(form_obj.length>0 && form_obj.attr('action').indexOf('#')==-1 && window.location.hash){
                form_obj.attr('action', form_obj.attr('action')+window.location.hash);
        }

        init_rules();
        
        //emf_session_timeout_warner.set_timers();
        
        force_session_for_submit_form();

        detect_unsupported_browser();
        


});



var emf_widgets={text : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,number : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,textarea : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,checkbox : 
                        function(index){
                                var arr=new Array();
                                $("input[name='element_"+index+"[]']:checked").each(function(){
                                        arr[arr.length]=this.value;                             
                                });
                                var result=arr.join(", ");
                                return result;
                        }
                ,radio : 
                        function(index){
                                var result="";
                                $("input[name=element_"+index+"]:checked").each(function(){
                                        result=this.value;                              
                                });
                                return result;
                        }
                ,select : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,email : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,phone : 
                        function(index){
                                var arr=new Array();
                                $("input[id^=element_"+index+"_]").each(function(){
                                        arr[arr.length]=this.value;
                                });
                                
                                var result="";
                                if(arr.length>0){
                                        result=arr.join("-");
                                }else{
                                        result=$("#element_"+index).val();
                                }
                                return result;
                        }
                ,datetime : 
                        function(index){
                                var result="";
                                
                                var date_part="";
                                if($("#element_"+index+"_year").length==1){
                                        date_part=$("#element_"+index+"_year-mm").val()+"/"+$("#element_"+index+"_year-dd").val()+"/"+$("#element_"+index+"_year").val();
                                }

                                var time_part="";
                                if($("#element_"+index+"_hour").length==1){
                                        time_part=$("#element_"+index+"_hour").val()+":"+$("#element_"+index+"_minute").val()+" "+$("#element_"+index+"_ampm").val();
                                }
                                
                                if(date_part && time_part){
                                        result=date_part+" "+time_part;
                                }else{
                                        result=date_part ? date_part : time_part;
                                }
                                
                                return result;
                        }
                ,url : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,file : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,select_multiple : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,price : 
                        function(index){
                                var result="";
                                var arr=new Array();
                                $("input[id^=element_"+index+"_]").each(function(){
                                        arr[arr.length]=this.value;
                                });
                                result=arr.join(".");
                                return result;
                        }
                ,hidden : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,Image : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,section_break : 
                        function(index){
                                return "";
                        }
                ,page_break : 
                        function(index){
                                return "";
                        }
                ,deprecated : 
                        function(index){
                                return $("#element_"+index).val();
                        }
                ,address : 
                        function(index){
                                var result="";
                                var element_arr=$("input,select").filter("[name='element_"+index+"[]']").toArray();
                                result=element_arr[0].value+" "+element_arr[1].value+"\n"
                                        +element_arr[2].value+","+element_arr[3].value+" "+element_arr[4].value+"\n"
                                        +element_arr[5].value;
                                return result;
                        }
                ,name : 
                        function(index){
                                var arr=new Array();
                                $("input[id^=element_"+index+"_]").each(function(){
                                        arr[arr.length]=this.value;
                                });
                                var result=arr.join(" ");
                                return result;
                        }
                };

var emf_condition_id_to_js_map={5 : 
                        function(field_value, value){
                                return field_value==value;
                        }
                ,6 : 
                        function(field_value, value){
                                return field_value!=value;
                        }
                ,1 : 
                        function(field_value, value){
                                return field_value.indexOf(value)>-1;
                        }
                ,2 : 
                        function(field_value, value){
                                return field_value.indexOf(value)==-1;
                        }
                ,3 : 
                        function(field_value, value){
                                return field_value.indexOf(value)==0;
                        }
                        ,4 : 
                        function(field_value, value){
                                return field_value.indexOf(value)==field_value.length-value.length;
                        }
                ,7 : 
                function(field_value, value){
                return parseFloat(field_value)==parseFloat(value);
        }
        ,8 : 
                        function(field_value, value){
                                return parseFloat(field_value)>parseFloat(value);
                        }
                ,9 : 
                        function(field_value, value){
                                return parseFloat(field_value) < parseFloat(value);
                        }
                ,10 : 
                        function(field_value, value){
                                var date_for_field_value=Date.parse(field_value);
                                var date_for_value=Date.parse(value);
                                if(date_for_field_value && date_for_value){
                                        return date_for_field_value == date_for_value;
                                }
                                return false;
                        }
                ,11 : 
                        function(field_value, value){
                                var date_for_field_value=Date.parse(field_value);
                                var date_for_value=Date.parse(value);
                                if(date_for_field_value && date_for_value){
                                        return date_for_field_value < date_for_value;
                                }
                                return false;
                        }
                ,12 : 
                        function(field_value, value){
                                var date_for_field_value=Date.parse(field_value);
                                var date_for_value=Date.parse(value);
                                if(date_for_field_value && date_for_value){
                                        return date_for_field_value > date_for_value;
                                }
                                return false;
                        }
                };
var emf_group_to_field_rules_map=[];
var emf_group_to_page_rules_for_confirmation_map=[];

var emf_cart=null;
var emf_page_info={current_page_index: 0, page_element_index_min: 0, page_element_index_max: 5};
var emf_index_to_value_map=null;
var emf_form_visit_id="1lYDbSbA5C4mwUW58";
//]]>
</script>            <?php the_content(); ?>
        </article>
    </div>
    <?php get_sidebar(); ?>
</section>
<?php get_footer(); ?> 