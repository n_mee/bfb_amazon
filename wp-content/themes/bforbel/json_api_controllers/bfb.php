<?php

class JSON_API_Bfb_Controller {

	private function bfb_get_JSON_post_array($post_id)
	{
		$post = get_post($post_id);
		$img_src = $this->get_original_image_from_content($post->post_content);
		$img_height =  $this->get_first_attached_image_height($post_id);
		if ($img_height == '') $img_height = $this->get_image_height_from_content($post->post_content);
		$static_img_src = $this->get_static_image($img_src);
		
		$bfb_JSON_post_array = array(
			'id' => $post_id,
			'post_link' => get_permalink( $post_id ),
			'image_height' => $img_height,
			//'post_content' => $p->post_content,
			'post_title' => $post->post_title,
			'image' => $img_src,          //original
			'image_ai' => $img_src,  //for android index view
			'image_ii' => $img_src,  //for ios index view
			'image_is' => $img_src,  //for ios single view   
			'image_thumbnail' => $this->get_thumbnail_img($img_src),
			'y_cut_off' => get_post_meta($post_id, BFBPNFC_META_KEY, true),
			//'featured_image' => $this->get_featured_image_src($id),			
		);
		if (strlen($static_img_src) > 0) $bfb_JSON_post_array['image_static_gif'] = $static_img_src;
		return $bfb_JSON_post_array;
	}
        private function get_thumbnail_img($p_org_img)
        {
                $path_parts = pathinfo($p_org_img);
                return $path_parts['dirname'] . '/' . $path_parts['filename'] . '-150x150.' . $path_parts['extension'];
        }
	
	private function get_featured_image_src($pid, $size = 'full')
	{
		if ( $tid = get_post_thumbnail_id($pid) and $src = wp_get_attachment_image_src($tid, $size) and ! empty($src[0]) ) 
			return $src[0];
		return '';
    }
	
	private function get_static_image($image_src_path)
	{
		$file_parts = pathinfo($image_src_path);
    
		//if is gif
		if (strcasecmp('gif', $file_parts['extension']) == 0) {
			$staticImgSrc = $file_parts['dirname'] . '/' . $file_parts['filename'] . '.png';
			return $staticImgSrc;
		}
		return '';
	}
	
	//returns the height of the first attached image
	private function get_first_attached_image_height($pid,$size='full')
	{
		$post_children_params = array(
			'post_parent' 	=>	$pid,
			'post_type'		=>	'attachment',
			'post_status'	=>	null,
			'post_mime_type'=>	'image',
			'number_posts'	=>	1,
			'order'			=>	'ASC',
		);
		$post_children_array = get_children($post_children_params);
		if ($post_children_array){
			foreach ($post_children_array as $post_child)
			{
				$image_attr = wp_get_attachment_image_src($post_child->ID, $size);
				if ($image_attr[2]) return $image_attr[2];
			}
		}
		
		return '';
    }

    private function get_image_height_from_content($p_content)
    {
        if (preg_match('/<img[^>]*?height="([^ \"]*)"[^>]*>/ims', $p_content, $match)) {
		$image_height = (int)$match[1];
		if ($image_height > 0) return $image_height;
        }
        return '';
    }
    private function get_original_image_from_content($p_content)
    {
        if (preg_match('/<img[^>]*?src="([^ \"]*)"[^>]*>/ims', $p_content, $match)) {
           return $match[1];
        }
        return '';
    }

    private function get_href_from_content($p_content)
    {
        if(preg_match_all('/<a[^>]+href=([\'"])(.+?)\1[^>]*>/i', $p_content, $match)) {
           return $match[2][0];
        }
        return '';
    }

    private function get_caption_from_content($p_content)
    {
        if (preg_match('/<div class=\"caption\">(.*?)<\/div>/s', $p_content, $match)) {
           return $match[1];
        }
        return '';
    }

    private function get_related_posts($p_id)
    {
        $number = 10;
        $ids = array();
        foreach( get_the_terms( $p_id, 'category' ) as $cat ) 
            $ids[] = $cat->term_id;
        
        $ids = get_objects_in_term($ids, array('category'));
        if ( false !== ($_t = array_search($GLOBALS['post']->ID, $ids) ) ) unset($ids[$_t]);
        if(($_num = (int) get_field('related_posts')) > 0) $number = $_num;        
        $r = get_posts(array(
            'posts_per_page'=> $number, 
            'no_found_rows' => true, 
            'post_status'   => 'publish', 
            'ignore_sticky_posts'=> true,
            'post__in' => $ids,
        ));
    
        $return_posts = array();
        foreach ($r as $p) {
                //check against self
                if($p->ID != $p_id)
                {
					$return_posts[] = $this->bfb_get_JSON_post_array($p->ID);
					/*
                        $org_img = $this->get_original_image_from_content($p->post_content);
                        array_push(
							$return_posts,
							$this->get_JSON_post_array($p)
							
							array(
									'id' => $p->ID,
									'post_link' => get_permalink( $p->ID ),
									'image_height' => $this->get_first_attached_image_height($p->ID),
									//'post_content' => $p->post_content,
									'post_title' => $p->post_title,
									'image' => $org_img,          //original
									'image_ai' => $org_img,  //for android index view
									'image_ii' => $org_img,  //for ios index view
									'image_is' => $org_img,  //for ios single view   
									'image_thumbnail' => $this->get_thumbnail_img($org_img),
									'y_cut_off' => get_post_meta($p->ID, BFBPNFC_META_KEY, true),
									//'featured_image' => $this->get_featured_image_src($id)
									'image_static_gif' => $this->get_static_image($org_img),
							)
															
                        );
					*/
                }
        }
        return $return_posts;
    }

  	public function bfb_get_category() {
  		global $json_api;
  		$deviceid = $json_api->query->deviceid;
  		$apptype = $json_api->query->apptype;
  		$appver = $json_api->query->appver;
  		
  		//only the categories shown on index page
  		
  		$raw_categories = array(
			get_category(13),	//humour-and-fun
			get_category(20),	//tips-and-tricks
			get_category(17),	//beauty-and-fashion
			get_category(14),	//ideas-and-inspiration
			get_category(16),	//products-and-design
			get_category(10),	//food
			get_category(11)	//games			   		
  		);
  		
  		$categories = array();
  		foreach ($raw_categories as $rc) {
  			array_push(
  				$categories,
  				array(
  					'id' => $rc->term_id,
  					'name' => $rc->name,
  					'slug' => $rc->slug,
  					'post_count' => $rc->count
  				)			
  			);
		}
  		
  		return array(
//  			"deviceid" => $deviceid,
//  			"apptype" => $apptype,
//  			"appver" => $appver,
      	"catlist" => $categories
		);
  	}

  	public function bfb_get_item_index_and_items() {
  		global $json_api;
  		$deviceid = $json_api->query->deviceid;
  		$apptype = $json_api->query->apptype;
  		$appver = $json_api->query->appver;

        $category = isset($json_api->query->category) ? $json_api->query->category : 0;
        $start = isset($json_api->query->start) ? $json_api->query->start : 0;
        $count = isset($json_api->query->count) ? $json_api->query->count : 200;
        	 		
		$args = array( 
            'numberposts' => $count,
            'offset' => $start,
            'category' => $category, 
            'post_type' => 'post', 
            'post_status' => 'publish' 
        );
		$recent_posts = wp_get_recent_posts( $args );
		
  		$ids = array();
  		foreach ($recent_posts as $post) {
  			array_push($ids, $post['ID']);
		}
	
		$recent_posts = array_slice($recent_posts, 0, 20);
		$return_posts = array();
  		foreach ($recent_posts as $p) {
			$return_posts[] = $this->bfb_get_JSON_post_array($p['ID']);
			/*
  			$org_img = $this->get_original_image_from_content($p['post_content']);
  			array_push(
  				$return_posts,
				//$this->get_JSON_post_array($p)
				
  				array(
  					'id' => $p['ID'],
                    'post_link' => get_permalink( $p['ID'] ),
					'image_height' => $this->get_first_attached_image_height($p['ID']),
					//'post_content' => $p['post_content'],
					'post_title' => $p['post_title'],
                    'image' => $org_img,          //original
                    'image_ai' => $org_img,  //for android index view
                    'image_ii' => $org_img,  //for ios index view
                    'image_is' => $org_img,  //for ios single view
                    'image_thumbnail' => $this->get_thumbnail_img($org_img),
                    'y_cut_off' => get_post_meta($p['ID'], BFBPNFC_META_KEY, true),

					//'featured_image' => $this->get_featured_image_src($p['ID'])
					'image_static_gif' => $this->get_static_image($org_img),
  				)
								
  			);
			*/
		}
		
  		  			
  		return array(
//  			"deviceid" => $deviceid,
//  			"apptype" => $apptype,
//  			"appver" => $appver,
//  			"catetory" => $category,
  			"ids" => $ids,
            "posts" => $return_posts 
		);
  	}
  
  	public function bfb_get_item_by_ids() {
  		global $json_api;
  		$deviceid = $json_api->query->deviceid;
  		$apptype = $json_api->query->apptype;
  		$appver = $json_api->query->appver;
  		$ids_string = $json_api->query->ids;
  		
  		
  		$return_posts=array();
  		if(null != $ids_string)	
  		{
			$ids = explode(",", $ids_string);
  			foreach ($ids as $id) {
				$return_posts[] = $this->bfb_get_JSON_post_array($id);
				/*
  				$p = get_post($id); 
				$org_img = $this->get_original_image_from_content($p->post_content);
				
				
				
				
				array_push(
					$return_posts,
					$this->get_JSON_post_array($p)
					/*
					array(
	  					'id' => $id,
                        'post_link' => get_permalink( $id ),
						'image_height' => $this->get_first_attached_image_height($id),
						//'post_content' => $p->post_content,
						'post_title' => $p->post_title,
                        'image' => $org_img,          //original
                        'image_ai' => $org_img,  //for android index view
                        'image_ii' => $org_img,  //for ios index view
                        'image_is' => $org_img,  //for ios single view
                        'image_thumbnail' => $this->get_thumbnail_img($org_img),
                        'y_cut_off' => get_post_meta($id, BFBPNFC_META_KEY, true),
						//'featured_image' => $this->get_featured_image_src($id),
						'image_static_gif' => $this->get_static_image($org_img),						
	  				)
					
				);
				*/
			}
		}
		 		
  		return array(
//  			"deviceid" => $deviceid,
//  			"apptype" => $apptype,
//  			"appver" => $appver,
      	"posts" => $return_posts
		);
    }

    public function bfb_get_suggested_posts() {
  		global $json_api;
  		$deviceid = $json_api->query->deviceid;
  		$apptype = $json_api->query->apptype;
  		$appver = $json_api->query->appver;
        $id = isset($json_api->query->id) ? $json_api->query->id : 0;
  		
  		$return_posts=array();
  		if(0 < $id)	
  		{
            $return_posts = $this->get_related_posts($id);
		}
		 		
  		return array(
            "posts" => $return_posts,
		);
  	}

   public function bfb_get_featured_posts() {
  		global $json_api;
  		$deviceid = $json_api->query->deviceid;
  		$apptype = $json_api->query->apptype;
  		$appver = $json_api->query->appver;
  		        
        $slider_id = 79025;
        $args = array(
            'force_no_custom_order' => true,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'ml-slider',
                    'field' => 'slug',
                    'terms' => $slider_id     //hard code
                )
            )
        );

        $query = new WP_Query($args);

        $slides = array();

        while ($query->have_posts()) {
            $query->next_post();

            $type = get_post_meta($query->post->ID, 'ml-slider_type', true);
            $type = $type ? $type : 'image'; // backwards compatibility, fall back to 'image'

            if (has_filter("metaslider_get_{$type}_slide")) {
                $return = apply_filters("metaslider_get_{$type}_slide", $query->post->ID, $slider_id);

                if (is_array($return)) {
                    $slides = array_merge($slides, $return);
                } else {
                    $slides[] = $return;
                }
            }
        }//end of while

        $featured = array();

        foreach ($slides as $slide) {
            $url = $this->get_href_from_content($slide);
            array_push(
  				$featured,
  				array(
  					//'slide' => $slide,
  					'image' => $this->get_original_image_from_content($slide),
                    'caption' => $this->get_caption_from_content($slide),
                    'url' => $url,
                    'post_id' => url_to_postid($url)
  				)
  			);
        }
                
  		return array(
            "featured" => $featured
		);
        }

        public function bfb_get_posts_count() {
  		global $json_api;
  		$deviceid = $json_api->query->deviceid;
  		$apptype = $json_api->query->apptype;
  		$appver = $json_api->query->appver;
		 		
  		return array(
                        "published" => wp_count_posts()->publish
		);
  	}

}

?>
