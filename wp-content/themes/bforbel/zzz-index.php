<?php
/**
 * @package WordPress
 * @subpackage B for Bell
 */
if ( check_if_ajax_loading() ) {
   /* ?><!DOCTYPE html><html><body><?php*/
    include "loop.php";
   /* ?></body></html><?php */
    return;
}
if ( ! have_posts() ) {
    include "404.php";
    return;
}
get_header(); ?>

<script type='text/javascript'>
    //handle gif in index
    function doGif(obj) // the li element clicked in the current scope
    {
//        var gif_src = obj.querySelector("#gif").src;
        var gif_style_display = obj.querySelector("#gif").style.display;
//        var static_style_display = obj.querySelector("#static").style.display;
        
        if("none" == obj.querySelector("#gif").style.display)
        {
            obj.querySelector("#gif").style.display = "";
            obj.querySelector("#static").style.display = "none";
            obj.querySelector("#span").style.display = "none";
        }
        else
        {
            obj.querySelector("#gif").style.display = "none";
            obj.querySelector("#static").style.display = ""; 
            obj.querySelector("#span").style.display = "";
        }
    }
</script>

<?php if (!bfb_is_mobile_view()): ?>
<section id="feature">
	<?php echo do_shortcode("[metaslider id=79025]"); ?>
</section>
<?php endif; ?>
<section id="content" class="cf infinite-scroll">
    <div class="left-page <?php if(bfb_is_mobile_view())echo "compact"; ?>" id="posts-container">
        <?php include "loop.php"; ?>
    </div>
	<?php get_sidebar(); ?>
</section>
<?php get_footer(); ?>
