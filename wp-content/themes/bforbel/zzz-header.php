<?php
/**
 * @package WordPress
 * @subpackage B for Bell
 */
session_start();
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta property="fb:admins" content="551921484" />
	<meta property="fb:admins" content="100003145932674" />
        
        <?php echo '<meta name="robots" content="noindex" />';?>

	<?php if ( bfb_is_mobile_view() ): ?>
	   <meta name="viewport" content="width=640, user-scalable=yes" />
	<?php else: ?>
	   <meta name="viewport" content="user-scalable=yes, width=1120" />
	<?php endif; ?>
	<?php if ( is_single() ):
	   echo '<meta property="og:image" content="'.get_post_image_src().'" /> ';
	   echo '<meta property="og:title" content="'.get_the_title().'" /> ';
	   echo '<meta property="og:type" content="article" /> ';
	   echo '<meta property="og:url" content="'.get_permalink($GLOBALS['posts'][0]->ID).'" /> ';
	   //echo '<meta property="og:description" content="'.get_the_title() . " - " . esc_attr(strip_tags($GLOBALS['posts'][0]->post_content)).'" /> ';
	   echo '<meta property="og:description" content="'.get_the_title().'" /> ';
	elseif(is_front_page()):
		echo '<meta property="og:type" content="website" /> ';
    endif; ?>
    <!-- <meta name="apple-mobile-web-app-capable" content="yes" /> -->
	<title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
    <!--[if lt IE 9]>
	    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	    <script type="text/javascript" src="<?php echo HV_THEME_URL, "/js/html5.js" ?>"></script>
    <![endif]-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo HV_THEME_URL, "/style.css?ver=2.13" ?>" />
    <!--[if IE]>
        <link rel="stylesheet" href="<?php echo HV_THEME_URL, "/css/ie.css" ?>" type="text/css" media="screen" />
        <?php if(bfb_is_desktop_view()): ?>
        	<link rel="stylesheet" href="<?php echo HV_THEME_URL, "/css/desktop_tmp.css" ?>" type="text/css" media="screen" />
	    <?php else: ?>
        	<?php /* <meta name="MobileOptimized" content="640" /> */ ?>
        	<link rel="stylesheet" type="text/css" href="<?php echo HV_THEME_URL, "/css/mobile.css?ver=4.0" ?>" />
        	<link rel="stylesheet" type="text/css" href="<?php echo HV_THEME_URL, "/css/desktop.css?ver=2.3" ?>" />
		<?php endif; ?>
    <![endif]-->
    <!--[if !IE]>-->
		<?php if(bfb_is_desktop_view()) : ?>
			<link rel="stylesheet" type="text/css" href="<?php echo HV_THEME_URL, "/css/desktop_tmp.css?ver=1.3" ?>" />
		<?php else: ?>
            <link rel="stylesheet" type="text/css" href="<?php echo HV_THEME_URL, "/css/media-style.css?ver=1.0" ?>" />
		<?php endif; ?>
    <!--<![endif]-->

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php
    	wp_enqueue_script('jquery');
    	wp_head();
	?>
	<?php if(!isset($_GET['no_js'])): ?>
	<script type="text/javascript" src="<?php echo HV_THEME_URL, "/js/main.js?ver=2.59" ?>"></script>
	<?php endif; ?>
    <style type="text/css">
        li.fckn-pinit a.pin-fix, li.fckn-pinit > a {
            background-repeat: no-repeat;
            width: 83px !important;
        }
	    span.st_tumblr_hcount span.stButton > span > span.stButton_gradient {
		    padding: 0;
		    background: none;
		    border: none;
	    }
        span.st_tumblr_hcount span.stButton_gradient span.tumblr {
	        width: 62px;
	        height: 20px;
	        margin: 0;
	        padding: 0;
	        background-image: url("<?php bloginfo('template_url'); ?>/img/share_2.png");
        }

        li span.st_tumblr_hcount span.stArrow {
	        position: relative;
	        top: -2px;
        }
		ul li .stButton {
			margin-right: 1px;
			margin-left: 1px;
		}
	    .sponsor-logo {
		    float: left;
	    }
    </style>
    <script type='text/javascript'>
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-17503869-2']);
      _gaq.push(['_trackPageview']);
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    <!-- Woopra Code Start -->
    <script src='//static.woopra.com/js/woopra.v2.js' type='text/javascript'></script>
    <script type='text/javascript'>
        woopraTracker.track();
    </script>
    <!-- Woopra Code End -->
</head>
<body <?php body_class( bfb_is_mobile_view() ? array('compact', 'mobile' ) : array( 'desktop-view', bfb_is_mobile_agent() ? 'mobile-agent' : '' )); ?>>
<?php
if(isset($_GET['show_agent'])):
	echo "<span style='font-size: 32px;'>",$_SERVER['HTTP_USER_AGENT'],"</span>";
endif; ?>
<?php /* if(bfb_is_mobile_view()): ?>
	<script type="text/javascript">
		jQuery(function($){
			$('html,body').scrollTop(-60);
		});
	</script>
<?php endif; */ ?>
<?php if(bfb_is_mobile_agent() and bfb_is_desktop_view()): ?>
	<a href="<?php echo add_query_arg("m","1"); ?>" class="switch-to-mobile">Switch to Mobile View</a>
<?php endif; ?>
<div id="fb-root" style="position:relative;"></div>
<script>


window.fbAsyncInit = function() {

FB.init({appId: '665674723487017', status: true, cookie: true, xfbml: true});
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <div class="center-box">
        <?php if (bfb_is_mobile_view()): ?><div style="width:100%; height:80px"></div><?php endif; ?>
      <header id="header" class="cf">
        <?php $logo = false;
            if ( is_category() ) $logo = z_taxonomy_image_url(get_queried_object_id());
            if ( is_page() ) $logo = get_post_image_src($GLOBALS['posts'][0]->ID);
            if ( ! $logo ) $logo = HV_THEME_URL . '/img/logos/main_logo.png';
        ?>
        <a class="logo" href="<?php echo SITE_URL; ?>"><img width="<?php echo bfb_is_mobile_view() ? 155 : 332;?>" src="<?php echo $logo ?>" alt="logo"></a>
        <div class="fixed-block">
            <?php if ( bfb_is_desktop_view() ) : ?>
                <aside id="header-bar">
                    <?php
                        if (bfb_browser_need_wap()) :
                           adsense_wap();
                        else:
                           dynamic_sidebar('Header Bar');
                        endif;
                     ?>
                </aside>
            <?php endif; ?>
            <nav class="main-menu cf" id="main-nav-container"<?php if(bfb_is_mobile_view()): ?> style="width:300px;"<?php endif; ?>>
                <?php if(bfb_is_mobile_view()): ?>
                <div class="fb-like" style="max-height:40px;margin-top:17px;margin-left:57px;width:120px; -webkit-transform: scale(1.8);-ms-transform: scale(1.8);-moz-transform: scale(1.8); -o-transform: scale(1.8);" data-send="false" data-layout="button_count" data-width="75" data-show-faces="false" data-href="https://www.facebook.com/bforbelmedia"></div>
                <?php endif; ?>
              <div class="browse-item"><a href="#collapse" id="menu-browse">Browse</a></div>
              <div id="mobile-nav" <?php if (bfb_is_mobile_view()) echo 'style="display: none;"'; ?>>
                  <?php wp_nav_menu(array(
                    'theme_location' => 'main',
                     'container' => '',
                     'menu_id' => '',
                     'menu_class' => ''
                  ));
	                if ( bfb_is_mobile_view() ) : ?>
                  <div class="compact-only">
                      <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="switch-to-desktop" class="mobile-link">Desktop View</a> | <a class="mobile-link" href="<?php bloginfo('url'); ?>/submit">Submit</a>
                  </div>
                  <?php endif; ?>
              </div>
            </nav>
            <script type="text/javascript">
              (function($){
                  li = $('nav.main-menu ul li.shop').first();
                  $('#mobile-nav').after(li.find('a').addClass('btn-shop'));
                  li.remove();
              })(jQuery);
            </script>
            <nav class="sub-menu cf">
              <?php wp_nav_menu(array(
                'theme_location' => 'secondary',
                'container' => '',
              )); ?>
            </nav>
        </div>
      </header>
	<?php if(bfb_is_mobile_view()):?>
    <noscript>
	    <nav class="main-menu compact" style="width: 100%;">
		    <div id="mobile-nav">
			    <?php wp_nav_menu(array(
				    'theme_location'=> 'main',
				    'container'     => '',
				    'menu_id'       => '',
				    'menu_class'    => ''
			    ));
			        if ( bfb_is_mobile_view() ) : ?>
				    <div class="compact-only">
					    <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="switch-to-desktop" class="mobile-link">Desktop View</a> | <a class="mobile-link" href="<?php bloginfo('url'); ?>/submit">Submit</a>
				    </div>
			    <?php endif; ?>
		    </div>
	    </nav>
    </noscript>
	<?php endif; ?>
	
	<?php 
	
		$app_banner_icon =  null;
		switch(bfb_device_from_useragent())
		{
			case 'ipad':
				$app_banner_icon =  HV_THEME_URL . '/img/app-icon-iPhone-300.png';
				$device_text = 'iPad';
				$store_text = 'the App Store';
				$app_link_url = 'https://itunes.apple.com/us/app/bforbel/id853131311?mt=8';
				break;
			case 'ipod':
				$app_banner_icon =  HV_THEME_URL . '/img/app-icon-iPhone-300.png';
				$device_text = 'iPod';
				$store_text = 'the App Store';
				$app_link_url = 'https://itunes.apple.com/us/app/bforbel/id853131311?mt=8';
				break;
			case 'iphone':
				$app_banner_icon =  HV_THEME_URL . '/img/app-icon-iPhone-300.png';
				$device_text = 'iPhone';
				$store_text = 'the App Store';
				$app_link_url = 'https://itunes.apple.com/us/app/bforbel/id853131311?mt=8';
				break;
			case 'android':
				$app_banner_icon =  HV_THEME_URL . '/img/app-icon-android-512.png';
				$device_text = 'Android';
				$store_text = 'the Play Store';
				$app_link_url = 'https://play.google.com/store/apps/details?id=com.bforbel';
				break;
		}
		
		if ($app_banner_icon !==  null):
	?>
	<div id="app-banner">
		<input type="image" id="app-banner-close-btn" src="<?php echo HV_THEME_URL . '/img/popup_close.png' ?>" onclick="appBannerClose()"></input>

		<a id="app-banner-overall-link" href="<?php echo $app_link_url; ?>" target="_blank"></a>
		<a id="app-banner-icon-link"><img id="app-banner-icon" src="<?php echo $app_banner_icon; ?>" /></a>
		<div id="app-banner-device"><h3>BforBel for <?php echo $device_text; ?></h3></div>
		<div id="app-banner-store"><p>Available on <?php echo $store_text; ?></p></div>        
		<a id="app-banner-view-button" href="http://www.bforbel.com">View</a>
	</div>
	
	<?php endif; ?>
	
    <script type="text/javascript">
        jQuery(function($){$('#menu-browse').click(function(){
             $t = $(this);
             if ( $t.is('.active') ) {
                $('#mobile-nav').hide();
             } else {
                $('#mobile-nav').show();
             }
             $t.toggleClass('active');
             return false;
        })});
    </script>
