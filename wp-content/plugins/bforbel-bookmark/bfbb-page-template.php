<?php
	$bfbb_before = "Your bookmarked posts";
	echo "<div class='bfbb-span'>";
	echo '<div class="bfbb-page-before">'.$bfbb_before.'</div>';

	echo "<ul>";
	if ($bookmarked_post_ids) 
	{
		$bookmarked_post_ids = array_reverse($bookmarked_post_ids);
		$posts_per_page = 10;
		$page = intval(get_query_var('paged'));

		$qry = array('post__in' => $bookmarked_post_ids, 'posts_per_page'=> $posts_per_page, 'orderby' => 'post__in', 'paged' => $page);
		query_posts($qry);

		while ( have_posts() ) : the_post();
			echo "<li><a href='".get_permalink()."' title='". get_the_title() ."'>" . get_the_title() . "</a> ";
//			bfbb_remove_favorite_link(get_the_ID());
			echo "</li>";
		endwhile;

		echo '<div class="navigation">';
		if(function_exists('wp_pagenavi')) { wp_pagenavi(); } 
		else { ?>
			<div class="alignleft"><?php next_posts_link( __( '&larr; Previous Entries', 'buddypress' ) ) ?></div>
			<div class="alignright"><?php previous_posts_link( __( 'Next Entries &rarr;', 'buddypress' ) ) ?></div>
		<?php }
		echo '</div>';

		wp_reset_query();
	} 
	else 
	{
		$bfbb_options = bfbb_get_options();
		echo "<li>";
		echo $bfbb_options['bookmarks_empty'];
		echo "</li>";
	}
	echo "</ul>";

//	echo '<p>'.bfbb_clear_bookmarks_link().'</p>';
	echo "</div>";
?>