
/*
		var ajaxData = {
			action: 'bforbel_post_new_fast_c_action',
			is_local_img:isLocalImg,
			category_arr:categoryArr,
			publish_post:bPublish,
			post_title:postTitle.value,
			img_data:imageData,
			img_src:imgSrc.value,
			body_msg:tinyMCE.get(bodyMsgId).getContent()
		};

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		$.post(
			ajaxurl, 
			ajaxData, 
			function(response) {
				//alert('Got this from the server: ' + response);
				//alert("Data: " + data + "\nStatus: " + status);

			}
		);
		
		//alert immediately after ajax call.
		{
			alert(bPublish ? "Post submitted to publish." : "Post submitted as draft");
			//reset
			clearCanvas();
			hasImage = false;
			tinyMCE.get(bodyMsgId).setContent('');
			imgSrc.value = defaultImgSrcValue;							
			postTitle.value = defaultPostTitleValue;		
			context.clearRect(0, 0, canvas.width, canvas.height);		
		}
	*/	
	
/*
	rStatus = 0 ==> not bookmarked;
				 1 ==> bookmarked;
				 -1==> dirty;
*/	
doBookmark_old = function (postId, userId, rStatus) 
{

	//console.log(postId + " | " + userId + " | " + rStatus);
	bfbbMsg = document.getElementById("bfbb_msg");
	if (userId < 1) 
	{
		alert('please login.');
		bfbbMsg.innerHTML="please login!";
		return;
	}

	//logged in. do ajax calls.
	var ajaxData = {
		action: 'bforbel_bookmark_action',
		pid:postId,
		uid:userId,
		c_status:0	//client status 
	};	


	if("bookmarked" == bfbbMsg.innerHTML)
	{
		//send ajax to change status to be not bookmarked.	
		bfbbMsg.innerHTML="not bookmarked";
	}
	else
	{
		//send ajax to change status to be bookmarked.
		ajaxData.c_status=1;	
		bfbbMsg.innerHTML="bookmarked";	
	}
	
	//make ajax call
	jQuery.post(
		ajaxurl, 
		ajaxData, 
		function(response) {
			//alert('Got this from the server: ' + response);
			//alert("Data: " + data + "\nStatus: " + status);

		}
	);
	//alert("ajax call sent to server...");
	
};	
        
doBookmark = function (postId, userId, rStatus) 
{

	//console.log(postId + " | " + userId + " | " + rStatus);
	if (userId < 1) 
	{
		alert('please login.');
		return;
	}
        
        if ($('a#bfb-bookmark-link-' + postId).length === 0)
        {
                console.log('Missing id a#bfb-bookmark-link-' + postId);
                return;
        }
        
        

	//logged in. do ajax calls.
	var ajaxData = {
		action: 'bforbel_bookmark_action',
		pid:postId,
		uid:userId,
		c_status:1	//client status 
	};	

        if ($('a#bfb-bookmark-link-' + postId).hasClass('bookmarked'))
        {
            ajaxData.c_status=0;
        }
        $('a#bfb-bookmark-link-' + postId).toggleClass('bookmarked');
        	
	//make ajax call
	jQuery.post(
		ajaxurl, 
		ajaxData, 
		function(response) {
			//alert('Got this from the server: ' + response);
			//alert("Data: " + data + "\nStatus: " + status);
                        //$('a#bfb-bookmark-link-' + postId).toggleClass('bookmarked');
		}
	);
	//alert("ajax call sent to server...");
	
};