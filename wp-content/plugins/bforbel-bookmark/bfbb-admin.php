<?php

	function bfbb_list_most_bookmarked($limit=10) {
	    global $wpdb;
	    $query = "SELECT post_id, meta_value FROM $wpdb->postmeta";
	    $query .= " LEFT JOIN $wpdb->posts ON post_id=$wpdb->posts.ID";
	    $query .= " WHERE meta_key='". BFBB_META_KEY ."' AND meta_value > 0 ORDER BY ROUND(meta_value) DESC LIMIT 0, $limit";
	    $results = $wpdb->get_results($query);
	    if ($results) {
	        echo "<ul>";
	        foreach ($results as $o):
	            $p = get_post($o->post_id);
	            echo "<li>";
	            echo "<a href='".get_permalink($o->post_id)."' title='". $p->post_title ."'>" . $p->post_title . "</a> ($o->meta_value)";
	            echo "</li>";
	        endforeach;
	        echo "</ul>";
	    }
	}

	echo "Top 10 bookmarked posts: \n";
	
	bfbb_list_most_bookmarked();

?>

