<?php
/**
 * Plugin Name: Bforbel Bookmark
 * Plugin URI: bforbel.com
 * Description: To enable registered user to bookmark posts
 * Version: 1.0
 * Author: Bforbel Media Pte Ltd
 * Author URI: bforbel.com
 * License: Bforbel License
 */


define('BFBB_PATH', plugins_url() . '/bforbel-bookmark');
define('BFBB_META_KEY', "bfb_bookmark");
define('BFBB_OFFSET_META_KEY', "bfb_bookmark_offset");

$ajax_mode = 1;

function bfbb_init() {
    $bfbb_options = array();
    $bfbb_options['add_bookmark'] = "Add to bookmarks";
    $bfbb_options['added'] = "Added to bookmarks!";
    $bfbb_options['remove_bookmark'] = "Remove from bookmarks";
    $bfbb_options['removed'] = "Removed from bookmarks!";
    $bfbb_options['clear'] = "Clear bookmarks";
    $bfbb_options['cleared'] = "<p>Bookmarks cleared!</p>";
    $bfbb_options['bookmarks_empty'] = "Bookmark list is empty.";
    $bfbb_options['rem'] = "remove";
    $bfbb_options['text_only_registered'] = "Only registered users can bookmark!";
    $bfbb_options['statics'] = 1;
    
    add_option('bfbb_options', $bfbb_options);
}
add_action('activate_bforbel-bookmark/bforbel-bookmark.php', 'bfbb_init');


function bfbb_config() { include('bfbb-admin.php'); }

function bfbb_config_page() {
	if ( function_exists('add_submenu_page') )
		add_options_page(__('Bforbel Bookmarks'), __('Bforbel Bookmarks'), 'manage_options', 'bforbel-bookmarks', 'bfbb_config');
}
add_action('admin_menu', 'bfbb_config_page');


function bfbb_die_or_go($str) {
    global $ajax_mode;
    if ($ajax_mode):
        die($str);
    else:
        wp_redirect($_SERVER['HTTP_REFERER']);
    endif;
}

function bfbb_get_options() {
   return get_option('bfbb_options');
}

function bfbb_get_option($opt) {
    $bfbb_options = bfbb_get_options();
    return htmlspecialchars_decode( stripslashes ( $bfbb_options[$opt] ) );
}

function bfbb_get_user_id() {
    global $current_user;
    get_currentuserinfo();
    return $current_user->ID;
}

/* db */
function bfbb_get_user_meta($uid) {
	return get_user_meta($uid, BFBB_META_KEY, true);
}

function bfbb_update_user_meta($uid, $arr) {
	return update_user_meta($uid, BFBB_META_KEY, $arr);
}

function bfbb_check_bookmarked($uid, $pid) {	
	$bookmarked_post_ids = bfbb_get_user_meta($uid);
	if ($bookmarked_post_ids)
	{
		return (in_array($pid, $bookmarked_post_ids));
	}
	return false;
}

//single value of the bookmarked count
function bfbb_get_post_meta($pid) {
	return get_post_meta($pid, BFBB_META_KEY, true);
}

function bfbb_update_post_meta($pid, $v) {
	return update_post_meta($pid, BFBB_META_KEY, $v);
}



/* html rendering */
function bfbb_loading_img() {
	return "<img src='".BFBB_PATH."/img/loading.gif' alt='Loading' title='Loading' class='bfbb-hide bfbb-img' />";
}

function bfbb_before_link_img() {
	return "<img src='".BFBB_PATH."/img/heart.png' alt='Bookmark' title='Bookmark' class='bfbb-img' style='height:50px;'/>";
}

function bfbb_link_html($post_id, $action) {
    $link = "<a class='bfbb-link' href='?bfbbaction=".$action."&amp;postid=". $post_id ."' rel='nofollow'>". bfbb_before_link_img() ."</a>";
    $link = apply_filters( 'bfbb_link_html', $link );
    return $link;
}

function bfb_enqueue_bookmark_script() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('bforbel_bookmark', plugins_url('/js/bfbb.js',__FILE__), array(),null   );
    //to pass ajaxurl for view-facing side scripts. (ajaxurl is globally defined on admin side)
    wp_localize_script( 'bforbel_bookmark', 'ajaxurl', admin_url('admin-ajax.php'));
}

function bfb_bookmark_link_status($post_id) {
    if(is_user_logged_in())
    {
            $user_id = bfbb_get_user_id();
            $r_status = bfbb_check_bookmarked($user_id, $post_id);//if this is too slow when scrolling do in bulk or asynchronously
            if ($r_status == true) {return ' bookmarked';}   
    }
    return '';
}

function bfb_bookmark_click_parameter($post_id) {
    $click_parameter = '';
    if(is_user_logged_in())
    {
            $user_id = bfbb_get_user_id();
            //$r_status = bfbv_check_voted($user_id, $post_id);
            $r_status = 0;//todo - might be better to use the backend, unless its slow
            $click_parameter = "onclick='doBookmark($post_id, $user_id, $r_status);' ";
    }

    return $click_parameter;
}

//render html codes for bookmark links.
function bfbb_link( $return = 0, $action = "", $show_span = 1, $args = array() ) {

	wp_enqueue_script('jquery');
	wp_enqueue_script('bforbel_bookmark', plugins_url('/js/bfbb.js',__FILE__), array(),null );
	//to pass ajaxurl for view-facing side scripts. (ajaxurl is globally defined on admin side)
	wp_localize_script( 'bforbel_bookmark', 'ajaxurl', admin_url('admin-ajax.php'));
	
	global $post;
	$post_id = &$post->ID;
	$user_id = -1;	//-1 for not logged in.
	$r_status = 0;
	$r_msg = "";
	
	if(is_user_logged_in())
	{
		$r_msg = "not bookmarked";
		$user_id = bfbb_get_user_id();
		
		if(bfbb_check_bookmarked($user_id, $post_id))
		{
			$r_status = 1;
			$r_msg = "bookmarked";
		}	        
	}
	
	$str = "";
	if ($show_span)
		$str = "<span class='bfbb-span'>";

	$str .= "<a class='bfbb-link' href='javascript:void(0);' onclick='doBookmark($post_id, $user_id, $r_status);'  rel='nofollow'>". bfbb_before_link_img() ."</a>";
	$str .= "<div id='bfbb_msg'>";
	$str .= $r_msg;
	$str .= "</div>";
		
	if ($show_span)
		$str .= "</span>";
	
	if ($return) { return $str; } else { echo $str; }
}

function bfbb_list_bookmarked_posts( $args = array() ) {
	if(is_user_logged_in())
	{
		$uid = bfbb_get_user_id();   
		$bookmarked_post_ids = bfbb_get_user_meta($uid); 
		include("bfbb-page-template.php");
	}

	else 
	{
		die("login needed");
	}
}

function bfbb_shortcode_func() {
    bfbb_list_bookmarked_posts();
}
add_shortcode('bforbel-bookmarked-posts', 'bfbb_shortcode_func');

function bfbb_content_filter($content) {
	if (is_page()){
		if (strpos($content,'{{bforbel-bookmarked-posts}}')!== false) {
			$content = str_replace('{{bforbel-bookmarked-posts}}', bfbb_list_bookmarked_posts(), $content);
		}
	}
	return $content;
}
add_filter('the_content','bfbb_content_filter');

		
	//for ajax
	add_action( 'wp_ajax_bforbel_bookmark_action', 'bforbel_bookmark_ajax_callback' );

	function bforbel_bookmark_ajax_callback() {
//		global $wpdb; // this is how you get access to the database


		//Request identified as ajax request
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {		
	
/*
			//debug
			$upload_dir = wp_upload_dir();
			$debug_file_path = $upload_dir['basedir'] . '/debug.txt'; 
			$f = fopen($debug_file_path, "w"); 
			fwrite($f, $_POST['pid'] . "\n"); 
			fwrite($f, $_POST['uid'] . "\n"); 
			fwrite($f, $_POST['c_status'] . "\n");
						
			fclose($f);  	
			die();
*/		

			$pid = $_POST['pid'];
			if (is_string($pid)) $pid = intval($pid);
			$uid = $_POST['uid'];
			if (is_string($uid)) $uid = intval($uid);
			$c_status = $_POST['c_status'];
			if (is_string($c_status)) $c_status = intval($c_status);
			

			//update user meta
			//check if $uid same as the current logged in user
						
			if(is_user_logged_in())
			{
				$user_id = bfbb_get_user_id();
				if($user_id != $uid)
				{
					die("invalid user.");
				}
			
				//update db	
						
				$r_status = 0;
				$bookmarked_post_ids = bfbb_get_user_meta($uid);
				if ($bookmarked_post_ids)
					if(in_array($pid, $bookmarked_post_ids))
						$r_status = 1;

							
				if($c_status != $r_status)
				{
					$bookmarked_count = intval(bfbb_get_post_meta($pid));					
					
					if(0 < $c_status)
					{							
						$bookmarked_post_ids[]=$pid;
						++$bookmarked_count;
					}
					else 
					{
						$bookmarked_post_ids = array_diff($bookmarked_post_ids, array($pid));
						$bookmarked_post_ids = array_values($bookmarked_post_ids);
						if(0 < $bookmarked_count)
							--$bookmarked_count;
					}
				
					bfbb_update_user_meta($uid, $bookmarked_post_ids);
					bfbb_update_post_meta($pid, $bookmarked_count);
					
				}
			}
			else {
				die("need login.");
			}
		}
			
		else
		{
			wp_redirect($_SERVER['HTTP_REFERER']);		
		}	
		die();
	}

