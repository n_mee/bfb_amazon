jQuery(document).ready(function ($){

	//alert('Document Ready!');
	var canvas = document.getElementById("my-canvas"),
		context = canvas.getContext("2d"),
		img = document.createElement("img"),
		img2 = document.createElement("img"),
		maxWidth = 450, 
		//maxHeight = 500,		
		w1 = 450,
		h1 = 500,
		
		postTitle = document.getElementById("post_title"),
		bodyMsgId = 'body_msg',
		imgSrc = document.getElementById("img_src"),
		defaultPostTitleValue = "New Post",
		defaultImgSrcValue = "Image has no home, can you help?",
		mouseDown = false,
		hasText = true,
		hasImage = false;
	
		//category ids
		category_ids = ["category_hf13","category_tt20","category_bf17","category_ii14","category_pg15","category_fd10","category_gs11"],
		
		//for remote drag and drop
		isLocalImg= true;
		remoteImgUrl = "";
		
		
		//white text font default to arial
		textFont = "10px Arial";
		textOffsetX = 8;
		textOffsetY = 18;
		
	//check if font is available.
	var detective = new Detector();
	if( detective.detect('Franklin Gothic Book'))
	{
		textFont = "12px Franklin Gothic Book";
	}
		
	clearCanvas = function () {
		if (hasText) {
			context.clearRect(0, 0, canvas.width, canvas.height);
			hasText = false;
		}
	};
		
	//console.log(plugins_url);
	img2.src = plugins_url + '/bforbel-post-new-fast-c/images/strip.png';
		
	// Adding instructions
	context.fillText("Drop an image onto the canvas", 120, 200);
									
	imgSrc.onchange = function()
	{
		console.log('input changed..\n');
		if(hasImage)
		{								
			context.drawImage(img2, 0, h1);			                
		   //draw text
		   context.fillStyle = "white";
		   context.font = textFont;
		   context.fillText(imgSrc.value, textOffsetX, h1+textOffsetY);
		}														
	};
	
	// Image for loading	
	img.addEventListener("load", function () {
			
		w1=img.width;
		h1=img.height;

		//small image with width <=450, need scale up to 450, jira BFBWEB-23
		//if(w1 > maxWidth)
		{
			h1 = h1 * maxWidth / w1;
			w1 = maxWidth;					
		} 

		/* only check the width
		if(h1 > maxHeight)				
		{
			w1 = w1 * maxHeight / h1;
			h1 = maxHeight;
		}
		*/
		
		//adjust canvas size
		var c = $('#my-canvas');
		//c.attr('width', $(container).width() ); //max width
		c.attr('height', h1 + 30); //pink strip
		clearCanvas();
		hasImage = true;
		context.clearRect(0, 0, canvas.width, canvas.height);										
		{            
			// composite now			                              
			context.drawImage(img, 0, 0, w1, h1);                
			context.drawImage(img2, 0, h1);
			
			//draw text
			context.fillStyle = "white";
			context.font = textFont;
			context.fillText(imgSrc.value, textOffsetX, h1+textOffsetY);
		}		
	}, false);

	// To enable drag and drop
	canvas.addEventListener("dragover", function (evt) {
		evt.preventDefault();
	}, false);

	// Handle dropped image file - only Firefox and Google Chrome
	canvas.addEventListener("drop", function (evt) {
		//console.log("sth dropped..");
		
		//local files
		var files = evt.dataTransfer.files;
		if (files.length > 0) {
			var file = files[0];
			if (typeof FileReader !== "undefined" && file.type.indexOf("image") != -1) {
				var reader = new FileReader();
				// Note: addEventListener doesn't work in Google Chrome for this event
				reader.onload = function (evt) {
					img.src = evt.target.result;
					isLocalImg = true;
				};
				reader.readAsDataURL(file);
			}
		}
		
		//images from web through 'text/html' data type
		else{
			var htmlData = evt.dataTransfer.getData('text/html');
			if(htmlData)
			{
				var htmlImgUrl = $(htmlData).filter('img').attr('src');
				if(htmlImgUrl)
				{
					//console.log(htmlImgUrl);
					img.src = htmlImgUrl;
					remoteImgUrl = htmlImgUrl;
					isLocalImg = false;
				}							
			}
		}
		
		
		evt.preventDefault();
	}, false);
	
	//submit to server
	var submit2Server = function(bPublish) {
/*	
		console.log(bPublish);
		if(bPublish)
			alert("do publish");
		else
			alert("save as draft");

		return;
*/		
		//check for valid fields.
		if (null==postTitle.value || ""==postTitle.value)
		{
			alert("Post title cannot be empty.");
			return false;
		}
		if(!hasImage)
		{
			alert("You need add an image to create a post.");
			return false;
		}
        yCutOff = parseInt(yCutOffInput.value);
		if(isNaN(yCutOffInput.value)  || yCutOff <= 0)
		{
			alert("Please enter a valid y cut off value. (1~9999)");
			return false;
		}
        outputFormat = "jpg";
        if (document.getElementById('png-radio').checked) {
                outputFormat = "png";
        }
 //       console.log("outputing: " + outputFormat);

		
		//window.open(canvas.toDataURL("image/png"));
		//console.log(canvas.toDataURL("image/png"));
		
		//need handle both local and remote drag and drop. BFBWEB-26
		var imageData = null;
		if(isLocalImg)
		{
			//pass local img dataUrl, let server render result, same as for remote images.
			imageData = img.src;
		}
		else
		{
			imageData = remoteImgUrl;
		}
		
		//get category data and uncheck checkboxes;
		var categoryArr = [];
		for (var i = 0; i < category_ids.length; i++) {
			var category_input = document.getElementById(category_ids[i]);
			if(category_input.checked)
			{
				categoryArr.push(category_input.value);
				category_input.checked = false;
			}
		}
		
		var ajaxData = {
			action: 'bforbel_post_new_fast_c_action',
			is_local_img:isLocalImg,
			category_arr:categoryArr,
			publish_post:bPublish,
			post_title:postTitle.value,
			img_data:imageData,
			img_src:imgSrc.value,
			y_cut_off:yCutOff,
			output_format:outputFormat,
			body_msg:tinyMCE.get(bodyMsgId).getContent()
		};

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		$.post(
			ajaxurl, 
			ajaxData, 
			function(response) {
				//alert('Got this from the server: ' + response);
				//alert("Data: " + data + "\nStatus: " + status);

			}
		);
		
		//alert immediately after ajax call.
		{
			alert(bPublish ? "Post submitted to publish." : "Post submitted as draft");
			//reset
			clearCanvas();
			hasImage = false;
			tinyMCE.get(bodyMsgId).setContent('');
			imgSrc.value = defaultImgSrcValue;							
			postTitle.value = defaultPostTitleValue;		
			context.clearRect(0, 0, canvas.width, canvas.height);		
		}	
	}

        // Y cut off label
        yCutOffLabel = document.createElement("label");
        yCutOffLabel.innerHTML = "Y Cut: ";
        document.getElementById("main-content").appendChild(yCutOffLabel);

        // Y cut off input
        yCutOffInput = document.createElement("input");
        yCutOffInput.value = "2000";
        document.getElementById("main-content").appendChild(yCutOffInput);

	// Save Post
	var savePost = document.createElement("button");
	savePost.innerHTML = "Save Post";
	savePost.addEventListener("click", function (evt) {
		submit2Server(false);
		evt.preventDefault();
	}, false);
	document.getElementById("main-content").appendChild(savePost);
	
	// Publish Post
	var publishPost = document.createElement("button");
	publishPost.innerHTML = "Publish";
	publishPost.addEventListener("click", function (evt) {
		submit2Server(true);
		evt.preventDefault();
	}, false);
	document.getElementById("main-content").appendChild(publishPost);

	// Reset button
	var resetButton = document.createElement("button");
	resetButton.innerHTML = "Reset";
	resetButton.addEventListener("click", function (evt) {
		clearCanvas();
		hasImage = false;
		tinyMCE.get(bodyMsgId).setContent('');
		imgSrc.value = defaultImgSrcValue;							
		postTitle.value = defaultPostTitleValue;
		yCutOffInput.value = "2000";

		
		context.clearRect(0, 0, canvas.width, canvas.height);	

		//uncheck checkboxes;
		for (var i = 0; i < category_ids.length; i++) {
			var category_input = document.getElementById(category_ids[i]);
			{
				category_input.checked = false;
			}
		}

		
	}, false);
	document.getElementById("main-content").appendChild(resetButton);						
});