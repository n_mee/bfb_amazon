<?php
/**
 * Plugin Name: Bforbel Post New Fast C
 * Plugin URI: bforbel.com
 * Description: To create new post in a faster way
 * Version: 1.0
 * Author: Bforbel Media Pte Ltd
 * Author URI: bforbel.com
 * License: Bforbel License
 */
 
define('BFBPNFC_PATH', plugins_url() . '/bforbel-post-new-fast-c');
define('BFBPNFC_META_KEY', "bfb_pnfc");
 
	 /** Step 2 (from text above). */
	add_action( 'admin_menu', 'bforbel_post_new_fast_c_submenu' );

	/** Step 1. */
	function bforbel_post_new_fast_c_submenu() {
		//add_options_page( 'My Plugin Options', 'My Plugin', 'manage_options', 'my-unique-identifier', 'my_plugin_options' );
		add_submenu_page( 'edit.php', 'Bforbel Add Post Fast C', 'Add Post Fast C', 'edit_posts', 'bforbel-post-new-fast-c-page', 'bforbel_post_new_fast_c_callback');
	}

	/** Step 3. */
	//to generate html content
	function bforbel_post_new_fast_c_callback() {
		if ( !current_user_can( 'edit_posts' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		
		//load  the needed scripts
		wp_enqueue_script('jquery');
		//detect font
		wp_enqueue_script('bforbel_fontdetect_b', plugins_url('/js/fontdetect.js',__FILE__) );
		wp_enqueue_script('bforbel_post_new_fast_c', plugins_url('/js/bforbel-post-new-fast-c.js',__FILE__) );
		//to pass strip image path
		wp_localize_script( 'bforbel_post_new_fast_c', 'plugins_url', plugins_url());

		echo '
			<title>B4Bel</title>	
			<style>
				canvas {
					position: relative;
					border: 1px solid #000;
				}
			</style>
			
			<div id="container">		
				<div role="main">
					<section id="main-content">			
						<p>
							<label for="post_title"><strong>Post Title</strong></label><br/>
							<input type="text" class="widefat" name="_wp_new_post_fast_c_post_title" id="post_title" value="New Post" style="width:500px; height:20px;">
						</p>
						<p>
							<label for="category"><strong>Category</strong></label><br/>
							<input type="checkbox" id="category_hf13" value=13 name="hf13"/>Humour & Fun <br/>
							<input type="checkbox" id="category_tt20" value=20 name="tt20"/>Tips & Tricks <br/>
							<input type="checkbox" id="category_bf17" value=17 name="bf17"/>Beauty & Fashion <br/>
							<input type="checkbox" id="category_ii14" value=14 name="ii14"/>Ideas & Inspiration <br/>
							<input type="checkbox" id="category_pg15" value=15 name="pg15"/>Products & Gift Guides <br/>
							<input type="checkbox" id="category_fd10" value=10 name="fd10"/>Food <br/>
							<input type="checkbox" id="category_gs11" value=11 name="gs11"/>Games <br/>
						</p>

						<p>
							<label for="img_src"><strong>Image Source</strong></label><br/>
							<input type="text" class="widefat" name="_wp_new_post_fast_c_post_title" id="img_src" value="Image has no home, can you help?" style="width:500px; height:20px;">
						</p>
						<p>
							<label for="body_msg"><strong>Body Message</strong></label><br/>';
		
		$content = '';
		$editor_id = 'body_msg';
		$settings = array( 
							'default_post_edit_rows' => 1,
							'media_buttons' => false );

		wp_editor( $content, $editor_id, $settings );
						
			
		echo '
						</p>
						<canvas id="my-canvas" width="450" height="500">I am canvas</canvas>							
						<br/>
                                                <input type="radio" id="png-radio" name="radio-output-format" value="png"> Png<br>
                                                <input type="radio" id="jpg-radio" name="radio-output-format" value="jpg" checked> Jpg<br>
					</section>
				</div>
			</div>';
	}
	
		
	//for ajax
	add_action( 'wp_ajax_bforbel_post_new_fast_c_action', 'bforbel_post_new_fast_c_ajax_callback' );
	
	// Use GD Api
	function bforbel_post_new_fast_c_ajax_callback() {
//		global $wpdb; // this is how you get access to the database

		 //Request identified as ajax request
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {		
/*			
			//debug
                        $upload_dir = wp_upload_dir();
                        $debug_file_path = $upload_dir['basedir'] . '/debug.txt';
                        $f = fopen($debug_file_path, "w");
			fwrite($f, $_POST['is_local_img'] . "\n"); 
			fwrite($f, "category array: "); 
			foreach ($_POST['category_arr'] as $cate) {
				fwrite($f, $cate. " | "); 
			}
			fwrite($f, "\n"); 
			
			fwrite($f, $_POST['publish_post'] . "\n"); 
			fwrite($f, $_POST['post_title'] . "\n"); 
			fwrite($f, $_POST['img_src'] . "\n"); 
			fwrite($f, $_POST['body_msg'] . "\n"); 
			fwrite($f, $_POST['img_data'] . "\n"); 
			fwrite($f, $_POST['y_cut_off'] . "\n");                         
			fwrite($f, $_POST['output_format'] . "\n");                      
			fwrite($f, plugins_url('/images/strip.png',__FILE__) . "\n"); 
						
			fclose($f);
			die();
*/			
	
			$uid = get_current_user_id();
			$postStatus = ('true' == $_POST['publish_post']) ? 'publish' : 'draft';
			$postTitle = $_POST['post_title'];
			$imgData = $_POST['img_data'];
			$imgSrc = $_POST['img_src'];
			$bodyMsg = $_POST['body_msg'];
			$categoryArr = $_POST['category_arr'] ;
                        $yCutOff = $_POST['y_cut_off'];
                        $outputFormat = $_POST['output_format'];
			
			//generate the bforbel image with remote/local img url, img src and pink strip (own font).
			//load src directly to create an image obj.
			$orgImg = imagecreatefromstring(file_get_contents($imgData));
			//list($orgW, $orgH) = getimagesize($imgData);
			$orgW = imagesx($orgImg); 
			$orgH = imagesy($orgImg); 
			
			//450 X 32
			$pinkW = 450;
			$pinkH = 32;
                        $pinkStrip = imagecreatefromstring(file_get_contents(dirname(__FILE__) . '/images/strip.png'));
			
			//fonts
			//$fontpath = plugins_url('/font/franklin-gothic-book-1361543735.ttf',__FILE__);
			//$fontpath = dirname(__FILE__) . '/font/franklin-gothic-book-1361543735.ttf';
			$fontpath = dirname(__FILE__) . '/font/FRABK.ttf';
			
			
			$newW = 450;
			$newH = $orgH;
			$resultImg = NULL;
			//small image with width <=450, need scale up to 450, jira BFBWEB-23
			//if ($orgW > $newW)
			{
				$newH = $newW * $orgH / $orgW;
				$resultImg = imagecreatetruecolor($newW, $newH + $pinkH);	
				//org img
				//BFBWEB-31
				//use imagecopyresampled()
				imagecopyresampled($resultImg, $orgImg, 0, 0, 0, 0, $newW, $newH, $orgW, $orgH);
				//imagecopyresized($resultImg, $orgImg, 0, 0, 0, 0, $newW, $newH, $orgW, $orgH);
			}
			
			/*
			//small image with width <=450, need scale up to 450, jira BFBWEB-23
			else
			{
				$resultImg = imagecreatetruecolor($newW, $newH + $pinkH);	
				//set everything transparent.
				imagecolortransparent($resultImg);
				imagecopy($resultImg, $orgImg, 0, 0, 0, 0, $orgW, $orgH);	
			}
			*/
					
					
			//pink strip
			imagecopy($resultImg, $pinkStrip, 0, $newH, 0, 0, $pinkW, $pinkH);	
			//draw text
			$white = imagecolorallocate($resultImg, 255, 255, 255);
			//TrueType org
			//imagettftext($resultImg, 11.5, 0, 6, $newH+20, $white, $fontpath, $imgSrc);
			//now try FreeType2
			imagefttext($resultImg, 8.5, 0, 6, $newH+20, $white, $fontpath, $imgSrc);
			//draw bold by drawing the non-bold text again, with x-offset
			imagefttext($resultImg, 8.5, 0, 7, $newH+20, $white, $fontpath, $imgSrc);
			
			//free up mem.
			imagedestroy($orgImg);
			imagedestroy($pinkStrip);

			//save image to the right path
			//wp_upload_dir
			$uploadDir = wp_upload_dir();
			$artDir = 'wp-content/uploads' . $uploadDir["subdir"] . "/";
			
			//alpha-numeric only
			$imgFileName = "";
			$imgFilePath = "";
                        
                        if('png' == $outputFormat)      //png
                        {
                                $imgFileName = ereg_replace('[^A-Za-z0-9-]', '', $postTitle) . '-image-' . time() . '.png';
                                $imgFilePath = "../" . $artDir . $imgFileName;
                                imagepng($resultImg, $imgFilePath);
                        }
                        else    //jpg
                        {
                                $imgFileName = ereg_replace('[^A-Za-z0-9-]', '', $postTitle) . '-image-' . time() . '.jpg';
                                $imgFilePath = "../" . $artDir . $imgFileName;
                                imagejpeg($resultImg, $imgFilePath, 90);	
                        }	
			//free up mem.
			imagedestroy($resultImg);

		//do wordpress stuff
			//create new post
			$postArr = array(
				 //'post_status' => 'publish',
				 'post_status' => $postStatus,
				 //'post_status' => 'draft',
				 'post_type' => 'post',
				 'post_title' => $postTitle,
				 'post_content' => $bodyMsg,
				 'post_author' => $uid,
				 'post_category' => $categoryArr
			 );
			$postId = wp_insert_post($postArr);


			//image attachment
			$siteUrl = get_option('siteurl');
			$imgFileInfo = getimagesize($imgFilePath);

			//create an array of attachment data to insert into wp_posts table
			$artData = array();
			$artData = array(
				'post_author' => $uid, 
				'post_date' => current_time('mysql'),
				'post_date_gmt' => current_time('mysql', 1),
				'post_title' => $postTitle, //$imgFileName, 
				'post_status' => 'inherit',
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				'post_name' => sanitize_title_with_dashes(str_replace("_", "-", $imgFileName)),											
				'post_modified' => current_time('mysql'),
				'post_modified_gmt' => current_time('mysql', 1),
				'post_parent' => $postId,
				'post_type' => 'attachment',
			//	'guid' => $siteUrl.'/'.$artDir.$imgFileName,
				'guid' => $siteUrl.'/'.$artDir.$imgFileName,
				'post_mime_type' => $imgFileInfo['mime'],
				'post_excerpt' => '',
				'post_content' => ''
			);
			
			$savePath = $uploadDir['basedir'] . $uploadDir["subdir"] . '/'.$imgFileName;
			
			//insert the database record
			$attachId = wp_insert_attachment( $artData, $savePath, $postId);
			
			//generate metadata and thumbnails
			$attachData = wp_generate_attachment_metadata( $attachId, $savePath);
			
			if ($attachData) {
				wp_update_attachment_metadata($attachId, $attachData);
			}

			//add img tags into post content.
			if ($attachId > 0){
				$wpPost = get_post($postId, 'ARRAY_A');
				$wpImage = wp_get_attachment_image_src( $attachId, 'full' );
				//$wpImage = wp_get_attachment_image_src( $attachId, 'thumbnail' );
				
				//$wpImageTag = '<p><a href="'.$wpImage[0].'"><img src="'.$wpImage[0].'" width="'.$wpImage[1].'" /></a></p>';
				$wpImageTag = '<p><a href="'.$wpImage[0].'"><img alt="'.$postTitle.'" width="'.$wpImage[1].'" src="'.$wpImage[0].'"></a></p>';
				
				//add image under the content
				$wpPost['post_content'] = $wpImageTag . $wpPost['post_content'];
				//add image above the content
				//$wpPost['post_content'] = $wpPost['post_content'] . $wpImageTag;
				
				$postId = wp_update_post( $wpPost );
                                
                                //update post meta for $yCutOff
                                update_post_meta($postId, BFBPNFC_META_KEY, $yCutOff);
                                
			}

			//set a featured image
			//set_post_thumbnail( $postId, $attachId );
			
			//optional make it the featured image of the post it's attached to
			//$rows_affected = $wpdb->insert($wpdb->prefix.'postmeta', array('post_id' => $post_id, 'meta_key' => '_thumbnail_id', 'meta_value' => $attachId));
		}
			
		else
		{
			//redirect for non-ajax calls
			header("Location: post-new.php");
			
		}	
		die();
	}
	
	