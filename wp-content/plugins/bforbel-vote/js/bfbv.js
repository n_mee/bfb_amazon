/*
	rStatus = 0 ==> not voted;
				 1 ==> voted up;
				 -1==> voted down;
	bVoteUp > 0 (e.g 1) means vote up is clicked
				else vote down clicked				 
*/	


doVote = function (postId, userId, rStatus, bVoteUp) 
{

	//console.log(postId + " | " + userId + " | " + rStatus);
	if (userId < 1) 
	{
		alert('please login.');
		return;
	}
        
        if ($('div#bfb-vote-container-' + postId).length === 0) {
            console.log('No id for div#bfb-vote-container-' + postId);
            return;
        }

	//logged in. do ajax calls.
	var ajaxData = {
		action: 'bforbel_vote_action',
		pid:postId,
		uid:userId,
		c_status:0	//client status 
	};	
        
        var clickedUp = $('div#bfb-vote-container-' + postId).hasClass('click-up');//this is a  little dangerous - can we assume it hasnt been changed by main.js
        var clickedDown = $('div#bfb-vote-container-' + postId).hasClass('click-down');

	//vote up clicked - Note - I dont think this is the right way - use r-status instaed and only send ajax if theres a change
	if(clickedUp === false && 0 < bVoteUp)
	{
            ajaxData.c_status=1;
	}
        if(clickedDown === false && 0 > bVoteUp)
	{
            ajaxData.c_status=-1;
	}
		
	//make ajax call
	jQuery.post(
		ajaxurl, 
		ajaxData, 
		function(response) {}
	);
};

