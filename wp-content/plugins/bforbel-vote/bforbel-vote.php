<?php
/**
 * Plugin Name: Bforbel Vote
 * Plugin URI: bforbel.com
 * Description: To enable registered user to vote posts
 * Version: 1.0
 * Author: Bforbel Media Pte Ltd
 * Author URI: bforbel.com
 * License: Bforbel License
 */


define('BFBV_PATH', plugins_url() . '/bforbel-vote');
define('BFBV_META_KEY', "bfb_vote");
define('BFBV_OFFSET_META_KEY', "bfb_vote_offset");

function bfbv_init() {
	$bfbv_options = array();
	$bfbv_options['vote_up'] = "Vote Up!";
	$bfbv_options['vote_down'] = "Vote Down!";
	$bfbv_options['votes_empty'] = "Vote list is empty.";
	$bfbv_options['debug'] = 1;
	
	//use "update_option" while plugin's under development.
	update_option('bfbv_options', $bfbv_options);
}
add_action('activate_bforbel-vote/bforbel-vote.php', 'bfbv_init');


function bfbv_config() { include('bfbv-admin.php'); }

function bfbv_config_page() {
	if ( function_exists('add_submenu_page') )
		add_options_page(__('Bforbel Vote'), __('Bforbel Vote'), 'manage_options', 'bforbel-vote', 'bfbv_config');
}
add_action('admin_menu', 'bfbv_config_page');

function bfbv_get_options() {
   return get_option('bfbv_options');
}

function bfbv_get_option($opt) {
    $bfbv_options = bfbv_get_options();
    return htmlspecialchars_decode( stripslashes ( $bfbv_options[$opt] ) );
}

function bfbv_get_user_id() {
    global $current_user;
    get_currentuserinfo();
    return $current_user->ID;
}

/* db */
function bfbv_get_user_meta($uid) {
	return get_user_meta($uid, BFBV_META_KEY, true);
}

function bfbv_update_user_meta($uid, $arr) {
	return update_user_meta($uid, BFBV_META_KEY, $arr);
}

function bfbv_check_voted($uid, $pid) {	
	$voted_posts = bfbv_get_user_meta($uid);
	if ($voted_posts)
	{
		if(array_key_exists($pid, $voted_posts)) 
		{
			return $voted_posts[$pid];
		}
	}
	return 0;	//not voted
}

//single value of the voted count
function bfbv_get_post_meta($pid) {
	return get_post_meta($pid, BFBV_META_KEY, true);
}

function bfbv_update_post_meta($pid, $v) {
	return update_post_meta($pid, BFBV_META_KEY, $v);
}



/* html rendering */
function bfbv_vote_up_img() {
	return "<img src='".BFBV_PATH."/img/vote_up.png' alt='Vote-Up' title='Vote-Up' class='bfbv-img' />";
}

function bfbv_vote_down_img() {
	return "<img src='".BFBV_PATH."/img/vote_down.png' alt='Vote-Down' title='Vote-Down' class='bfbv-img' />";
}


function bfb_enqueue_vote_script() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('bforbel_vote', plugins_url('/js/bfbv.js',__FILE__), array(),null   );
    //to pass ajaxurl for view-facing side scripts. (ajaxurl is globally defined on admin side)
    wp_localize_script( 'bforbel_vote', 'ajaxurl', admin_url('admin-ajax.php'));
}

//return 'click-up' or 'click-down' or empty string '' depending on current status
//many performance improvements needed - getmeta should only be called once per page - store the array and use it for each post, but consider scrolling
function bfb_vote_container_status($post_id) {
    if(is_user_logged_in())
    {
            $user_id = bfbv_get_user_id();
            $r_status = bfbv_check_voted($user_id, $post_id);//if this is too slow when scrolling do in bulk or asynchronously
            if ($r_status == 1) {return ' click-up';}
            if ($r_status == -1) {return ' click-down';}    
    }
    return '';
}

//return the click parameter to be placed in the vote hyperlink - eg onclick='doVote($post_id, $user_id, $r_status, 1);'
//vote status = 'up' or 'down'
function bfb_vote_click_parameter($post_id, $vote_status) {
    $click_parameter = '';
    if(is_user_logged_in())
    {
            $user_id = bfbv_get_user_id();
            //$r_status = bfbv_check_voted($user_id, $post_id);
            $r_status = 0;//todo - might be better to use the backend, unless its slow
            if ($vote_status == 'up') {$click_parameter = "onclick='doVote($post_id, $user_id, $r_status, 1);' ";}
            if ($vote_status == 'down') {$click_parameter = "onclick='doVote($post_id, $user_id, $r_status, -1);' ";}
    }

    return $click_parameter;
}


//render html codes for vote links.
function bfbv_link( $return = 0, $action = "", $show_span = 1, $args = array() ) {

	wp_enqueue_script('jquery');
	wp_enqueue_script('bforbel_vote', plugins_url('/js/bfbv.js',__FILE__) );
	//to pass ajaxurl for view-facing side scripts. (ajaxurl is globally defined on admin side)
	wp_localize_script( 'bforbel_vote', 'ajaxurl', admin_url('admin-ajax.php'));
	
	global $post;
	$post_id = &$post->ID;
	$user_id = -1;	//-1 for not logged in.
	$r_status = 0;
	$r_msg = "";
	
	if(is_user_logged_in())
	{
		$user_id = bfbv_get_user_id();
		
		$r_status = bfbv_check_voted($user_id, $post_id);
		if(0 < $r_status)
			$r_msg = "voted up";
		else if(0 > $r_status)
			$r_msg = "voted down";
	}
	
	$str = "";
	if ($show_span)
		$str = "<span class='bfbv-span'>";

	$str .= "<a class='bfbv-link' href='javascript:void(0);' onclick='doVote($post_id, $user_id, $r_status, 1);'  rel='nofollow'>". bfbv_vote_up_img() ."</a>";
	$str .= "------";
	$str .= "<a class='bfbv-link' href='javascript:void(0);' onclick='doVote($post_id, $user_id, $r_status, -1);'  rel='nofollow'>". bfbv_vote_down_img() ."</a>";
	$str .= "<div id='bfbv_msg'>";
	$str .= $r_msg;
	$str .= "</div>";
		
	if ($show_span)
		$str .= "</span>";
	
	if ($return) { return $str; } else { echo $str; }
}

function bfbv_list_voted_posts( $args = array() ) {
	if(is_user_logged_in())
	{
		$uid = bfbv_get_user_id();   
		$voted_posts = bfbv_get_user_meta($uid); 
		include("bfbv-page-template.php");
	}

	else 
	{
		die("login needed");
	}
}

function bfbv_shortcode_func() {
    bfbv_list_voted_posts();
}
add_shortcode('bforbel-voted-posts', 'bfbv_shortcode_func');

function bfbv_content_filter($content) {
	if (is_page()){
		if (strpos($content,'{{bforbel-voted-posts}}')!== false) {
			$content = str_replace('{{bforbel-voted-posts}}', bfbv_list_voted_posts(), $content);
		}
	}
	return $content;
}
add_filter('the_content','bfbv_content_filter');

		
	//for ajax
	add_action( 'wp_ajax_bforbel_vote_action', 'bforbel_vote_ajax_callback' );

	function bforbel_vote_ajax_callback() {
//		global $wpdb; // this is how you get access to the database


		//Request identified as ajax request
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {		
	
			$pid = $_POST['pid'];
			if (is_string($pid)) $pid = intval($pid);
			$uid = $_POST['uid'];
			if (is_string($uid)) $uid = intval($uid);
			$c_status = $_POST['c_status'];
			if (is_string($c_status)) $c_status = intval($c_status);
				
/*
			//debug
			$upload_dir = wp_upload_dir();
			$debug_file_path = $upload_dir['basedir'] . '/debug.txt'; 
			$f = fopen($debug_file_path, "w"); 
			fwrite($f, $_POST['pid'] . "\n"); 
			fwrite($f, $_POST['uid'] . "\n"); 
			fwrite($f, $_POST['c_status'] . "\n");
			fwrite($f, $pid . "\n"); 
			fwrite($f, $uid . "\n"); 
			fwrite($f, $c_status . "\n");			
						
			fclose($f);  	
			die();
*/		

			

			//update user meta
			//check if $uid same as the current logged in user
			if(is_user_logged_in())
			{
				$user_id = bfbv_get_user_id();
				if($user_id != $uid)
				{
					die("invalid user.");
				}
			
				//update db	
				$r_status = 0;
				$voted_posts = bfbv_get_user_meta($uid);
				if ($voted_posts)
				{
					if(array_key_exists($pid, $voted_posts)) 
					{
						$r_status = $voted_posts[$pid];
					}
				}				
				
				if($c_status != $r_status)
				{
					$voted_count = intval(bfbv_get_post_meta($pid));
					$voted_posts = array_diff($voted_posts, array($pid=>$r_status));
					
					if(0 != $c_status)
					{							
						$voted_posts[$pid]=$c_status;
					}
					$voted_count += $c_status - $r_status;
					
					bfbv_update_user_meta($uid, $voted_posts);
					bfbv_update_post_meta($pid, $voted_count);
				}
			}
			else {
				die("need login.");
			}
		}
			
		else
		{
			wp_redirect($_SERVER['HTTP_REFERER']);		
		}	
		die();
	}

