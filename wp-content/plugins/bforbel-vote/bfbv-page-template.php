<?php
	$bfbv_before = "Your voted posts";
	echo "<div class='bfbv-span'>";
	echo '<div class="bfbv-page-before">'.$bfbv_before.'</div>';

	echo "<ul>";
	if ($voted_posts && 0<sizeof($voted_posts)) 
	{
		//need extra bool to have numeric keys preserved
		$voted_posts = array_reverse($voted_posts, true);
		$voted_post_ids = array_keys($voted_posts);
		
		$posts_per_page = 10;
		$page = intval(get_query_var('paged'));

		$qry = array('post__in' => $voted_post_ids, 'posts_per_page'=> $posts_per_page, 'orderby' => 'post__in', 'paged' => $page);
		query_posts($qry);

		while ( have_posts() ) : the_post();
			echo "<li><a href='".get_permalink()."' title='". get_the_title() ."'>" . get_the_title() . "</a> ";
//			bfbv_remove_favorite_link(get_the_ID());
			echo ($voted_posts[get_the_ID()] > 0) ? "Up" : "Down";
			echo "</li>";
		endwhile;

		echo '<div class="navigation">';
		if(function_exists('wp_pagenavi')) { wp_pagenavi(); } 
		else { ?>
			<div class="alignleft"><?php next_posts_link( __( '&larr; Previous Entries', 'buddypress' ) ) ?></div>
			<div class="alignright"><?php previous_posts_link( __( 'Next Entries &rarr;', 'buddypress' ) ) ?></div>
		<?php }
		echo '</div>';

		wp_reset_query();
	} 
	else 
	{
		$bfbv_options = bfbv_get_options();
		echo "<li>";
		echo $bfbv_options['votes_empty'];
		echo "</li>";
	}
	echo "</ul>";

//	echo '<p>'.bfbv_clear_votes_link().'</p>';
	echo "</div>";
?>