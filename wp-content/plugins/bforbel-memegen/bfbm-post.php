<?php

        define("MEMEGEN_MAX_IMAGE_HEIGHT",94);
        define("MEMEGEN_MAX_IMAGE_WIDTH",87);

        //add memegen post submenu
	add_action( 'admin_menu', 'bforbel_post_memegen_submenu' );

	function bforbel_post_memegen_submenu() {
		//add_options_page( 'My Plugin Options', 'My Plugin', 'manage_options', 'my-unique-identifier', 'my_plugin_options' );
		add_submenu_page( 'edit.php', 'Bforbel MemeGen', 'Bforbel MemeGen', 'edit_posts', 'bforbel-post-memegen-page', 'bforbel_post_memegen_callback');
	}

        //todo - move these so that theyre only called when needed
        wp_enqueue_style('memegen_backend_css', plugins_url('/css/bforbel-memegen-backend.css',__FILE__), array(),null  );
	wp_enqueue_script('jquery');

	wp_enqueue_script('memegen_backend_js', plugins_url('/js/bforbel-memegen-backend.js',__FILE__), array(),null  );
        
       
	//to generate html content
	function bforbel_post_memegen_callback() {
		if ( !current_user_can( 'edit_posts' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

                if (isset($_POST['upload']))
                {
                    //category has been renamed
                    foreach($_FILES['file']['tmp_name'] as $file_index => $uploaded_file)
                    {
                        $this_file = array();
                        $this_file['name'] = $_FILES['file']['name'][$file_index];
                        $this_file['type'] = $_FILES['file']['type'][$file_index];
                        $this_file['tmp_name'] = $_FILES['file']['tmp_name'][$file_index];
                        $this_file['size'] = $_FILES['file']['size'][$file_index];
                        
                        $upload_result = wp_handle_upload( $this_file, array('test_form' => FALSE) );
                        
                        if (isset($upload_result['type']) && stripos($upload_result['type'],'image') !== false){
                            //insert attachment
                            
                            $attachment_data= array(
                                'guid' => $upload_result['url'],
                                'post_title' => '',
                                'post_content' => '',
                                'post_status' => 'inherit',
                                'post_type' => 'attachment',
                                'post_mime_type' => $upload_result['type'],
                                'post_category' => array($_POST['category_id'],get_category_by_slug('memegen')->term_id),//current category and memegen
                            );
                            $attach_id = wp_insert_attachment ($attachment_data,$upload_result['file']);
                            $attach_data = wp_generate_attachment_metadata( $attach_id, $upload_result['file'] );
                            wp_update_attachment_metadata( $attach_id, $attach_data );
                        }
                    }                  
                }


                if (isset($_POST['rename']))
                {
                    //category has been renamed
                    if (isset($_POST['newname']) && strlen($_POST['newname']) > 0) {
                        $newName = $_POST['newname'];
                        $category_slug = 'funny';
                        $category_id = $_POST['category_id'];
                        wp_update_term($category_id,'category',array('name' => $newName));
                    }
                   
                }
                
                if (isset($_POST['new']))
                {
                    //category has been renamed
                    if (isset($_POST['newcategory']) && strlen($_POST['newcategory']) > 0) {
                        $newName = $_POST['newcategory'];
                        $category_id = get_category_by_slug('memegen')->term_id;
                        wp_create_category($newName,$category_id);
                    }                  
                }
                
                if (isset($_POST['delete']))
                {                    
                        $newName = $_POST['newname'];
                        $category_slug = 'funny';
                        $category_id = $_POST['category_id'];
                        wp_delete_category($category_id);                                     
                }
                
                if (isset($_POST['update-image-category']))
                {
                    $memegen_category_id = get_category_by_slug('memegen')->term_id;
                    $radio_category_id = $_POST['image-category-id'];
                    $post_id = $_POST['image-post-id'];
                    $append = false;
                    $post_categories = array($memegen_category_id,$radio_category_id);
                    wp_set_post_categories( $post_id, $post_categories, $append );
                }
                
                if (isset($_POST['delete-image']))
                {
                    $post_id = $_POST['image-post-id'];
                    $force_delete = true;
                    wp_delete_post( $post_id, $force_delete );
                }
                
             
                $memegen_category_id = get_category_by_slug('memegen')->term_id;
                $category_args = array(
                    'child_of' => $memegen_category_id, 
                    'hide_empty' => 0,
                );
                $memegen_categories = get_categories($category_args);
                /*
                $memegen_tabs = array();
                foreach ($memegen_subcategories as $subcategory)
                {
                    $memegen_tabs[] = array(
                        'name' => $subcategory->name,
                        'slug' => $subcategory->name,
                    );
                }
                 * 
                 */

                //read images from each category into memegen_images array
                $memegen_images = array();

                foreach ($memegen_categories as $category_index => $category)
                {

                    //read memegen image sources into an array
                    //each element of the $memegen_images array is an associative array (url, width, height)

                    $image_args = array(
                        'posts_per_page' => -1,  //all posts
                        'post_type' => 'attachment',
                        'category_name' => $category->slug,
                        'post_mime_type' => 'image',
                    );

                    $memegen_posts = get_posts($image_args);

                    foreach ( $memegen_posts as $memegen_image_index => $post ){

                        $image = wp_get_attachment_image_src($post->ID, 'full'); //array 0=>url , 1=>width, 2=> height
                        if ($image !== false && $image[1] > 0 && $image[2] > 0) {
                            $new_image = array();

                            //set url
                            $new_image['url'] = $image[0];

                            //resize to expected width, height
                            $aspect_ratio = $image[2] / $image[1];// height / width
                            if ($aspect_ratio > MEMEGEN_MAX_IMAGE_HEIGHT / MEMEGEN_MAX_IMAGE_WIDTH)
                            {
                                //resize to maximum height
                                $new_image['height'] = MEMEGEN_MAX_IMAGE_HEIGHT;
                                $new_image['width'] = round( $image[1] * MEMEGEN_MAX_IMAGE_HEIGHT / $image[2] );
                            }
                            else
                            {
                                //resize to maximum width
                                $new_image['width'] = MEMEGEN_MAX_IMAGE_WIDTH;
                                $new_image['height'] = round( $image[2] * MEMEGEN_MAX_IMAGE_WIDTH / $image[1] );
                            }

                            $new_image['class'] = 'image-category-'.$category_index;
                            $new_image['category'] = $category_index;
                            $new_image['id'] = 'image-post-id-'.$post->ID;
                            $memegen_images[] = $new_image; 
                        }
                    }
                }
       
        
        


		?>
		<title>Memegen</title>	

			
		<div id="container">		
			<div role="main">
				<section id="main-content">			

                                    <p>
                                    <label for="category"><h2>Memegen Categories</h2></label><br/>
                                    <?php foreach ($memegen_categories as $category_index => $memegen_category) : ?>

                                    <input type="button" class="category-button" id="category-<?php echo $category_index.'-'.$memegen_category->term_id; ?>" value="<?php echo $memegen_category->name; ?>" />

                                    <?php endforeach; ?>

                                    </p>

                                    <div id="memegen-image-panel">
                                        <?php foreach ($memegen_images as $memegen_image) : ?>

                                            <img src = "<?php echo $memegen_image['url']; ?>" width="<?php echo $memegen_image['width']; ?>" height="<?php echo $memegen_image['height']; ?>"  class="<?php echo $memegen_image['class']; ?>" id="<?php echo $memegen_image['id']; ?>"
                                            <?php if ($memegen_image['category'] > 0) echo 'style="display:none;"';   ?> />

                                                
                                        <?php endforeach; ?>
                                    </div>		
                                    <div id="form-panel">
                                        
                                        <h1 id="active-category-name"><?php echo $memegen_categories[0]->name; ?></h1>
                                        <hr />
                                        <h3>Upload Images</h3>
                                        <form enctype="multipart/form-data" action="#" method="POST">
                                            <input type='hidden' class='hidden-category-id' name='category_id' value='<?php echo $memegen_categories[0]->term_id; ?>' />
                                            <input type="file" name="file[]" multiple />
                                            <input type="submit" name="upload" value = "Upload" />
                                        </form>
                                        <br />
                                        
                                        <hr />
                                        <h3>Edit Category</h3>
                                        <form action='#' method='POST'>
                                            
                                            <input type='text' name='newname' id='current-name' value='<?php echo $memegen_categories[0]->name; ?>' />
                                        
                                        
                                            <input type='hidden' class='hidden-category-id' name='category_id' value='<?php echo $memegen_categories[0]->term_id; ?>' />
                                            <input type='submit' name='rename' value='Rename' />
                                            <br />
                                            <input type='submit' name='delete' value='Delete' />
                                            <br/><br/>
                                            <hr />
                                            <h3>Create New Category</h3>
                                            <input type='text' name='newcategory' id='current-name' value='New Category' />
                                            <input type='submit' name='new' value='Create' />
                                            <br/><br/>
                                            
                                        </form>
                                        
                                        
                                    </div>

				</section>
			</div>
		</div>
                
                <div id="image-update-panel">
                    
                    <h3>Update Image</h3>

                    <form action='#' method='POST'>
                        <br />

                        <input type='submit' name='delete-image' value='Delete Permanently' />
                        <br />
 
                        <br />
                        <input type='submit' name='update-image-category' value='Change Category' />
                        <br />
                        <br />

                        <input type="hidden" class="hidden-post-id" name="image-post-id" value="" />
                        <?php foreach ($memegen_categories as $category_index => $memegen_category) : ?>

                            <input type="radio" id="category-radio-<?php echo $category_index; ?>" name="image-category-id" value="<?php echo $memegen_category->term_id; ?>" /><?php echo $memegen_category->name; ?><br />

                        <?php endforeach; ?>


                    </form>    
                </div>
        <?php }
