jQuery(document).ready(function ($){

	// holds all our content boxes
	var contentBoxes = [];
        // path to image currently dragged or selected
        var targetImageSrc = null;
        // if in textmode a click on the screen produces a textbox
        var textMode = false;
        
        var imageRowIndex = [];
        var imageExpandedRowIndex = [];
        var activeImageTab = 0;
        
        var addTextOffset = 150;
        

	// New, holds the 8 tiny boxes that will be our selection handles
	// the selection handles will be in this order:
	//     r-1
	// 0  1  2
	// 3     4
	// 5  6  7
	var resizeHandles = [];
	var rotationHandles = [];

	// Hold canvas information
	var canvas;
	var ctx;
	var WIDTH;
	var HEIGHT;
	var INTERVAL = 20;  // how often, in milliseconds, we check to see if a redraw is needed

	var isDrag = false;
	var isResizeDrag = false;
	var isRotateDrag = false;
	var expectResize = -1; // New, will save the # of the selection handle if the mouse is over one.
	var expectRotate = -1;
	var mx, my; // mouse coordinates
	
	//min resize dim
	var minResizeW = 20;	//default
	var minResizeH = 20;	//default

	 // when set to true, the canvas will redraw everything
	 // invalidate() just sets this to false right now
	 // we want to call invalidate() whenever we make a change
	var canvasValid = false;

	// The node (if any) being selected.
	// If in the future we want to select multiple objects, this will get turned into an array
	var mySel = null;

	// The selection color and width. Right now we have a red selection with a small width
	var mySelColor = '#c7c7c7';
	var mySelWidth = 1;
	var mySelBoxColor = '#0eadee'; // New for selection boxes
	var myRotBoxColor = '#0eadee'; // New for rotation boxes
        var mySelBoxFillColor = '#ffffff';
	var mySelBoxSize = 8;

	// we use a fake canvas to draw individual shapes for selection testing
	var ghostcanvas;
	var gctx; // fake canvas context

	// since we can drag from anywhere in a node
	// instead of just its x/y corner, we need to save
	// the offset of the mouse when we start dragging.
	var offsetx, offsety;

	// Padding and border style widths for mouse offsets
	var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;
        
        function getFullImgSrcFromThumbImgSrc(thumbImgSrc)
        { 
            var fullImgSrc = thumbImgSrc.replace(/-\d+x\d+(\.[^\.]*)$/,"$1");
            return fullImgSrc;
            //return thumbImgSrc;
        }

        //view previous row if exists
        function previousImageRow(expanded)
        {
            if (expanded === true) {
                if ( imageExpandedRowIndex[activeImageTab] === undefined) imageExpandedRowIndex[activeImageTab] = 0;
                imageExpandedRowIndex[activeImageTab]--;
            }
            else {
                if ( imageRowIndex[activeImageTab] === undefined) imageRowIndex[activeImageTab] = 0;
                imageRowIndex[activeImageTab]--;
            }
        }
        
        //view next row if exists
        function nextImageRow(expanded)
        {
            if (expanded === true) {
                if ( imageExpandedRowIndex[activeImageTab] === undefined) imageExpandedRowIndex[activeImageTab] = 0;
                imageExpandedRowIndex[activeImageTab]++;
            }
            else {
                if ( imageRowIndex[activeImageTab] === undefined) imageRowIndex[activeImageTab] = 0;
                imageRowIndex[activeImageTab]++;
            }
        }

        //hide images in the active tab
        function hideTabContent() {               
            $('.memegen-tab-' + activeImageTab).removeClass('visible expanded-visible');
            $('.memegen-tab-' + activeImageTab).addClass('hidden expanded-hidden');
        }
    
    
        //display of memegen images in an active tab 
        function showTabContent() {
            if ( imageRowIndex[activeImageTab] === undefined) imageRowIndex[activeImageTab] = 0;
            if ( imageExpandedRowIndex[activeImageTab] === undefined) imageExpandedRowIndex[activeImageTab] = 0;
            
            //display settings in the minimised window
            for (var imageRow = imageRowIndex[activeImageTab]; imageRow < imageRowIndex[activeImageTab] + 3 ; imageRow++ ) {
                $('.memegen-tab-' + activeImageTab + '.memegen-row-' + imageRow).removeClass('hidden');
                $('.memegen-tab-' + activeImageTab + '.memegen-row-' + imageRow).addClass('visible');
            }
            //display settings in the expanded window
            for (var imageExpandedRow = imageExpandedRowIndex[activeImageTab]; imageExpandedRow < imageExpandedRowIndex[activeImageTab] + 3 ; imageExpandedRow++ ) {
                $('.memegen-tab-' + activeImageTab + '.memegen-expanded-row-' + imageExpandedRow).removeClass('expanded-hidden');
                $('.memegen-tab-' + activeImageTab + '.memegen-expanded-row-' + imageExpandedRow).addClass('expanded-visible');
            }  
        }




	// ContentBox object to hold data
	function ContentBox2D() {
		// screen coords of center of content
		this.x = 0;	
		this.y = 0; 
		// default width and height?
		this.w = 1; 
		this.h = 1;
		//rotation
		this.r = 0;	
		
		//flop | flip
		this.flop=1;	//horizontal, 1=>no change, -1=>flop original
		this.flip=1;	//vertical, 1=>no change, -1=>flip original
		
		//placeholder, implement "has a" relationship.
		this.content=null;
	}

	//given model position (px, py), compute the scr pos
	ContentBox2D.prototype.scrPos = function(px, py) {
		//first do rotation
		var sin_theta = Math.sin(this.r);
		var cos_theta = Math.cos(this.r);
		var rx, ry;
		rx = px * cos_theta - py * sin_theta;
		ry = py * cos_theta + px * sin_theta;
		
		//then translation
		rx = rx + this.x;
		ry = ry + this.y;
		
		var result = [];
		result.push(rx);
		result.push(ry);
		return result;
	}

	//to get the mapped coords of screen coords
	//input: screen coords px, py
	//return maped coords rx, ry such that we get (px, py) by first rotaing (rx, ry) by (r) then translate by (x,y) 
	ContentBox2D.prototype.mapPos = function(px, py) {
		//first undo translate
		px = px - this.x;
		py = py - this.y;
		
		//then undo rotation
		var sin_theta = Math.sin(-this.r);
		var cos_theta = Math.cos(-this.r);
		//var rx, ry;
		//rx = px * cos_theta - py * sin_theta;
		//ry = py * cos_theta + px * sin_theta;
		var result = [];
		result.push(px * cos_theta - py * sin_theta);
		result.push(py * cos_theta + px * sin_theta);
		return result;
	} //end mapPos
		
	// tell if the input screen coords (px, py) falls into 
	// the box defined by (x,y,w,h), with origin in the content center.
	// the box goes through the affine transformation (rotate by this.r then translate by this.x and this.y) to get the final pos in screen coords.
	// returns true if falls in, false otherwise.
	ContentBox2D.prototype.testHitBox = function(px, py, x, y, w, h) {
		//first get mapped pos
		var mappedPos = this.mapPos(px, py);
		var mx = mappedPos[0];
		var my = mappedPos[1];
		return (mx >= x && mx <= x+w && my >= y && my <= y+h);
	} // end testHitBox
		
	//test if the screen coords (px, py) is inside the content box
	//return true if it falls in, false otherwise.
	//this is used as a new way to tell if we're selecting an content
	ContentBox2D.prototype.testHitContentBox = function(px, py) {
		return (this.testHitBox(px, py, -this.w/2, -this.h/2, this.w, this.h));
	} // end testHitRotationHandles
	
	//test if the screen coords (px, py) is inside the rotation handles
	//return the index to the rotation handle the (px, py) falls in. otherwise -1
	ContentBox2D.prototype.testHitRotationHandles = function(px, py) {
		var cur = rotationHandles[0];
		if(this.testHitBox(px, py, cur.x, cur.y, mySelBoxSize, mySelBoxSize))
		{
			return 1;
		}
		return -1;
	} // end testHitRotationHandles

	//test if the screen coords (px, py) is inside the resize handles
	//return the index to the resize handle the (px, py) falls in. otherwise -1
	ContentBox2D.prototype.testHitResizeHandles = function(px, py) {
                expectResize = -1;
		for (var i = 0; i < 8; i++) {
			// 0  1  2
			// 3     4
			// 5  6  7		  
			var cur = resizeHandles[i];
			if(this.testHitBox(px, py, cur.x, cur.y, mySelBoxSize, mySelBoxSize))
			{
                            expectResize = i;
                            //use gradient to show cursors instead
                            if (px === this.x) return 1;//north south
                            var gradient = (this.y - py) /(px - this.x);//note that y increases down the page
                            if (gradient > 2) return 1;//north south
                            if (gradient > 0.5) return 2;//northeast southwest
                            if (gradient > -0.5) return 4;//east west
                            if (gradient > -2) return 7;//northwest southeast                            
                            return 1;//north sout
			}
		} //end for
		return -1;
	} //end testHitResizeHandles
		
		
	// we used to have a solo draw function
	// but now each box is responsible for its own drawing
	// mainDraw() will call this with the normal canvas
	// myDown will call this with the ghost canvas with 'black'
	ContentBox2D.prototype.draw = function(context, optionalColor) {
	  
		// We can skip the drawing of elements that have moved off the screen:
		if (this.x > WIDTH || this.y > HEIGHT) return; 
		if (this.x + this.w < 0 || this.y + this.h < 0) return;
	  
		//rotate agains content center
		var w2 = this.w/2;
		var h2 = this.h/2;
		
		//push matrix
		context.save();
		context.translate(this.x, this.y);
		context.rotate(this.r);
		
		//flop/flip
		context.scale(this.flop, this.flip);
		
		//drawContent
		this.content.draw(context, -this.w/2, -this.h/2, this.w, this.h);
		
		//revert scale for flop/flip
		context.scale(1/this.flop, 1/this.flip);
	  
		// draw selection
		// this is a stroke along the box and also 8 new selection handles
		if (mySel === this) {
			context.strokeStyle = mySelColor;
			context.lineWidth = mySelWidth;
			context.strokeRect(-w2, -h2, this.w, this.h);
	   
			// draw the boxes
			var half = mySelBoxSize / 2;

			//     R
			// 0  1  2
			// 3     4
			// 5  6  7
			
			//rotation handle top of [1]
			rotationHandles[0].x = -half;
			rotationHandles[0].y = -h2-half*10;
	  
			// top left, middle, right
			resizeHandles[0].x = -w2-half;
			resizeHandles[0].y = -h2-half;
	  
			resizeHandles[1].x = -half;
			resizeHandles[1].y = -h2-half;

			resizeHandles[2].x = w2-half;
			resizeHandles[2].y = -h2-half;

			//middle left
			resizeHandles[3].x = -w2-half;
			resizeHandles[3].y = -half;
	  
			//middle right
			resizeHandles[4].x = w2-half;
			resizeHandles[4].y = -half;

			//bottom left, middle, right
			resizeHandles[5].x = -w2-half;
			resizeHandles[5].y = h2-half;
			
			resizeHandles[6].x = -half;
			resizeHandles[6].y = h2-half;

			resizeHandles[7].x = w2-half;
			resizeHandles[7].y = h2-half;
                        
                        
                        
                        context.beginPath();
                        context.moveTo(resizeHandles[1].x + mySelBoxSize/2 ,resizeHandles[1].y + mySelBoxSize/2);
                        context.lineTo(rotationHandles[0].x + mySelBoxSize/2,rotationHandles[0].y + mySelBoxSize/2);
                        context.stroke();
                        
                        context.fillStyle = mySelBoxFillColor;
			
			//draw rotaion handel
			context.strokeStyle = myRotBoxColor;
                        
                        context.beginPath();
                        context.arc(rotationHandles[0].x + mySelBoxSize/2,rotationHandles[0].y + mySelBoxSize/2,mySelBoxSize/2,0,2*Math.PI);
                        context.fill();
                        context.stroke();


			//context.strokeRect(rotationHandles[0].x, rotationHandles[0].y, mySelBoxSize, mySelBoxSize);

			//draw scaling handel
			context.strokeStyle = mySelBoxColor;
                        
			for (var i = 0; i < 8; i ++) {
				var cur = resizeHandles[i];
				context.fillRect(cur.x, cur.y, mySelBoxSize, mySelBoxSize);
                                context.strokeRect(cur.x, cur.y, mySelBoxSize, mySelBoxSize);
			}
		}
		
		//pop matrix
		ctx.restore();
	
	} // end draw


	// ColorBox object to hold rotate/resize handles
	function ColorBox2() {
		this.x = 0;
		this.y = 0;
		this.w = 1; // default width and height?
		this.h = 1;
		this.fill = '#444444';
	}
	
	//image content
	function ImageContent()
	{
		this.img = document.createElement("img");
                this.img.src = targetImageSrc;
	}
		
	ImageContent.prototype.draw = function(context, x, y, w, h) {
		//draw image now
		context.drawImage(this.img, x, y, w, h);
		//anti-aliasing
		//context.drawImage(this.img, -w2-0.5, -h2-0.5, this.w, this.h);
		//context.drawImage(this.img, -w2-0.5, -h2+0.5, this.w, this.h);
		//context.drawImage(this.img, -w2+0.5, -h2-0.5, this.w, this.h);
		//context.drawImage(this.img, -w2+0.5, -h2+0.5, this.w, this.h);
	}
	
	//Initialize a new ContentBox2D, add it, and invalidate the canvas
	function addImageBox(x, y, w, h, r, flip, flop) {
		var rect = new ContentBox2D;
		rect.x = x;
		rect.y = y;
		rect.w = w;
		rect.h = h;
                rect.r = r;
                rect.flip = flip;
                rect.flop = flop;
                
		rect.content = new ImageContent;
                
		contentBoxes.push(rect);
                mySel = rect;
                $('.select-block .drop').hide();
                $('#nick-text-tools-list').hide();
                $('#nick-text-update-list').hide();
                $('#nick-tools-list').show();
                
		invalidate();
	}
	
	//text content
	function TextContent()
	{
		this.text = "I love Bforbel.";
		//this.text = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.";
		this.fontSize = 20; //default font
		this.font = "px Arial";
		this.fillStyle = "black";
		//default minWidth
		this.minWidth = 20;
		
		//flag to re-calculate font size, use it to avoid heavy computations in draw.
		//start as true to calculate once.
		this.bCalculateFontSize = true;
	}
	
	TextContent.prototype.maxFontSize = 100; 	//max font size
	TextContent.prototype.minFontSize = 10; 	//min font size
	
	TextContent.prototype.getMinWidth = function(){
		return this.minWidth;
	}
		
	TextContent.prototype.draw = function(context, x, y, w, h) {
		//compute size on the fly?
		context.fillStyle = this.fillStyle;

		//need calculate the biggest fit font size | bin search
		if(this.bCalculateFontSize)
		{
			var maxSize = this.maxFontSize;
			var minSize = this.minFontSize;
			context.font = minSize + this.font;	

			//update minWidth with min font size
			this.minWidth = context.measureText(this.text).width;				
		
			if(w>this.minWidth && h>minSize)
			{	
				while (minSize < maxSize)
				{
					this.fontSize = (minSize + maxSize) / 2;
					context.font = this.fontSize + this.font;
					
					// too small
					if(w>context.measureText(this.text).width && h>this.fontSize)
					{
						minSize = this.fontSize;
					}
					// too big
					else
					{
						maxSize = this.fontSize;
					}
					
					//stop when min max differ only 1
					if(minSize > maxSize - 1)
					{
						this.fontSize = minSize;
						break;
					}				
				}//end of while	
			}//end if bonding box bigger than min font
			this.bCalculateFontSize=false;
		}//end of need calculate font size on fly

		//draw text now
		//console.log(context.font);
		//console.log(x + " | " + y + " | " + w + " | " + h);
		
		//context.textBaseline='alphabetic';
		context.font = this.fontSize + this.font;
		context.textAlign = 'left';
		context.fillText(this.text, x, y+h-3);
	}
	
	//Initialize a new ContentBox2D, add it, and invalidate the canvas
	function addTextBox(x, y, w, h, r, flip, flop, text) {
		var rect = new ContentBox2D;
		rect.x = x;
		rect.y = y;
		rect.w = w;
		rect.h = h;
                rect.r = r;
                rect.flip = flip;
                rect.flop = flop;
		rect.content = new TextContent;
                rect.content.text = text;
		contentBoxes.push(rect);
                mySel = rect;
                $('#nick-tools-list').hide();
                $('#nick-text-update-field').val(mySel.content.text);
                $('.select-block .drop').hide();
                $('#nick-text-tools-list').show();
                $('#nick-text-update-list').show();
		invalidate();
	}
	

	// initialize our canvas, add a ghost canvas, set draw loop
	// then add everything we want to intially exist on the canvas
	function init2() {
		canvas = document.getElementById('memegen-canvas');
		HEIGHT = canvas.height;
		WIDTH = canvas.width;
		ctx = canvas.getContext('2d');
		ghostcanvas = document.createElement('canvas');
		ghostcanvas.height = HEIGHT;
		ghostcanvas.width = WIDTH;
		gctx = ghostcanvas.getContext('2d');

		//fixes a problem where double clicking causes text to get selected on the canvas
		canvas.onselectstart = function () { return false; }
	  
		// fixes mouse co-ordinate problems when there's a border or padding
		// see getMouse for more detail
		if (document.defaultView && document.defaultView.getComputedStyle) {
			stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)     || 0;
			stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)      || 0;
			styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10) || 0;
			styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)  || 0;
		}
	  
		// make mainDraw() fire every INTERVAL milliseconds
		setInterval(mainDraw, INTERVAL);

		// set our events. Up and down are for dragging,
		// double click is for making new boxes
		canvas.onmousedown = myDown;
		canvas.onmouseup = myUp;
		canvas.onmousemove = myMove;

                //attach handlers for tool buttons
                
                $('.remove-button').click(function(e) {
                    e.preventDefault();
                    var index = contentBoxes.indexOf(mySel);
                    if (index > -1) {
			contentBoxes.splice(index, 1);
			mySel = null;
			invalidate();	
			clear(gctx);
                        
                        $('.select-block .drop').hide();
                        $('#nick-text-tools-list').hide();
                        $('#nick-text-update-list').hide();
                        $('#nick-tools-list').hide();

			//update extra buttons
			onSelectChange();
                    }
                });
                
                $('#flip-button').click(function(e) {
                    e.preventDefault();
                    var index = contentBoxes.indexOf(mySel);
                    if (index > -1) {
			mySel.flip = -mySel.flip;
			invalidate();	
                    }
                });
                
                $('#flop-button').click(function(e) {
                    e.preventDefault();
                    var index = contentBoxes.indexOf(mySel);
                    if (index > -1) {
			mySel.flop = -mySel.flop;
			invalidate();	
                    }
                });
                
                $('.clone-button').click(function(e) {
                    e.preventDefault();                   
                    var index = contentBoxes.indexOf(mySel);
                    if (index > -1) {
                        
			var height = mySel.h;
                        var width = mySel.w;
                        var rotation = mySel.r;
                        var flip = mySel.flip;
                        var flop = mySel.flop;
                        if (mySel.content instanceof TextContent) {
                            var text = mySel.content.text;
                            addTextBox(mySel.x - (width / 2) + 10, mySel.y - (height / 2) + 10, width, height, rotation, flip, flop, text);
                        }
                        else {
                            targetImageSrc = mySel.content.img.src;
                            addImageBox(mySel.x - (width / 2) + 10, mySel.y - (height / 2) + 10, width, height, rotation, flip, flop);	
                        }
                    }
                });
                
                //reorder the content boxes with current selection at the end
                $('.forward-button').click(function(e) {
                    e.preventDefault();
                    var myselIndex = contentBoxes.indexOf(mySel);
                    if (myselIndex > -1) {
                        var reorderedContentboxes = [];
                        for (var i = 0; i < contentBoxes.length; i++) {
                            if (i != myselIndex) {
                                reorderedContentboxes.push(contentBoxes[i]);
                            }
                        }
                        reorderedContentboxes.push(mySel);
                        contentBoxes = reorderedContentboxes;
                        invalidate();
                    }
                });
                
                //reorder the content boxes with current selection at the start
                $('.backwards-button').click(function(e) {
                    e.preventDefault();
                    var myselIndex = contentBoxes.indexOf(mySel);
                    if (myselIndex > -1) {
                        var reorderedContentboxes = [];
                        reorderedContentboxes.push(mySel);
                        for (var i = 0; i < contentBoxes.length; i++) {
                            if (i != myselIndex) {
                                reorderedContentboxes.push(contentBoxes[i]);
                            }
                        }
                        contentBoxes = reorderedContentboxes;
                        invalidate();
                    }
                });
                
                //set handlers for background color
                $('.colors-list li a').click(function(e) {
                    e.preventDefault();
                    $('#memegen-canvas').css('background-color',$(this).css('background-color'));
                    $('#nick-canvas-container').css('background-color',$(this).css('background-color'));
                });
                
                //set text update button handler
                $('#nick-text-update-button').click(function(e) {
                    e.preventDefault();
                    if(null !== mySel)
                    {
                        if (mySel.content instanceof  TextContent) 
                        {
                            mySel.content.text = $('#nick-text-update-field').val();
                            mySel.content.bCalculateFontSize = true;
                            invalidate();
                        }		
                    }
                });
                        
                $('#nick-text-shade-drop').click(function(e) {
                    e.preventDefault();
                    $('.select-block .drop').toggle();
                });        
                        
  
                
                $('#nick-text-button').click(function(e) {
                    e.preventDefault();
                    addTextOffset += 30;
                    if (addTextOffset > 300) addTextOffset -= 133;
                    addTextBox(addTextOffset, addTextOffset, 200, 50,0,1,1,'Enter text here');
                    
                    
                });
                
                //set text shade handler
                $('.select-block .drop ul li a').click(function(e) {
                    e.preventDefault();
                    if (mySel.content instanceof  TextContent) {
                        //change the shade
                        mySel.content.fillStyle = $(this).css('background-color');
                        invalidate();
                    }
                });
                


                
                
                //drag and drop functionality
                $('.memegen-image').bind('dragstart',function(e) {
                    //e.originalEvent.dataTransfer.setData("URL",e.target.src);
                    //e.stopPropogation();
                    var fullImgSrc = getFullImgSrcFromThumbImgSrc(e.target.src);
                    e.originalEvent.dataTransfer.setData("Text",fullImgSrc + ',' + e.target.width + ',' + e.target.height);
                    //e.originalEvent.dataTransfer.setData("Height",e.target.height);
                });
                
                $('#memegen-canvas').bind('dragover',function(e) {
                    e.preventDefault();
                });

                $('#memegen-canvas').bind('drop',function(e) {
                    e.preventDefault();
                    //targetImageSrc = e.originalEvent.dataTransfer.getData("URL");
                    var image_data = e.originalEvent.dataTransfer.getData("Text");
                    var image_data_parts = image_data.split(',');
                    
                    
                    if (image_data_parts.length === 3) {
                        targetImageSrc = image_data_parts[0];
                        var width = parseInt(image_data_parts[1]);
                        var height = parseInt(image_data_parts[2]);

                        console.log('height: ' + height);
                        console.log('widtht: ' + width);
                        console.log('source: ' + targetImageSrc);
                        
                        if (width > 10 && height > 10 && targetImageSrc !== null) {

                            
                            getMouse(e.originalEvent);                   
                            addImageBox(mx - (width / 2), my - (height / 2), width, height,0,1,1);
                            
                        }
                    }
                });
                
                //drop a selected item when you leave the canvas
                //$('#memegen-canvas').bind('mousedown',function(e) {
                //    mySel = null;
                //    invalidate();
                //});
                
                //handle save button
                $('#memegen-save-button').click(function(e) {
                    e.preventDefault();
                    mySel = null;
                    invalidate();
                    mainDraw();
			
                    window.open(canvas.toDataURL("image/png"));
                });
                
                //toggle expansion
                $('#expanded-view').click(function(e) {
                    e.preventDefault();
                    $('.tab-content .items-holder').toggleClass('expanded');
                    $('.tabs-area .view').toggleClass('expanded');
                });
                
                //memegen tab handler
                $('.tabs-area .tabset li a').click(function(e) {
                    e.preventDefault();
                    var clickedTabId = $(this).attr('id');
                    //get the tab index from the end of the id string
                    var clickedTabIndex = parseInt(clickedTabId.match(/(\d+)$/)[0], 10);
                    if (clickedTabIndex !== activeImageTab) {
                        //remove current display
                        hideTabContent();
                        //show old tab as deactivated;
                        $('.tabs-area .tabset li a#memegen-tab-' + activeImageTab).parent().removeClass('active');
                        //change activeTab and display new images
                        activeImageTab = clickedTabIndex;
                        showTabContent();
                        //show active tab
                        $(this).parent().addClass('active');
                    }
                });
                
                //scrolling image panel
                $('.tab-content a.prev').click(function(e) {
                    e.preventDefault();
                    hideTabContent();
                    if ($(this).siblings('div.items-holder').hasClass('expanded')) {
                        previousImageRow(true);//advance expanded row
                    }
                    else {
                        previousImageRow(false);//advance minimised row
                    }
                    showTabContent();
                });
                
                $('.tab-content a.next').click(function(e) {
                    e.preventDefault();
                    hideTabContent();
                    if ($(this).siblings('div.items-holder').hasClass('expanded')) {
                        nextImageRow(true);//advance expanded row
                    }
                    else {
                        nextImageRow(false);//advance minimised row
                    }
                    showTabContent();
                });
	  
		//rotation handels
		rotationHandles.push(new ColorBox2);
		
		// set up the selection handle boxes
		for (var i = 0; i < 8; i ++) {
			var rect = new ColorBox2;
			resizeHandles.push(rect);
		}

	} //end init2


	//wipes the canvas context
	function clear(c) {
		c.clearRect(0, 0, WIDTH, HEIGHT);
	}

	// Main draw loop.
	// While draw is called as often as the INTERVAL variable demands,
	// It only ever does something if the canvas gets invalidated by our code
	function mainDraw() {
		if (canvasValid == false) {
			clear(ctx);

			// Add stuff you want drawn in the background all the time here

			// draw all boxes
			var l = contentBoxes.length;
			for (var i = 0; i < l; i++) {
				contentBoxes[i].draw(ctx); // we used to call drawshape, but now each box draws itself
			}

			// Add stuff you want drawn on top all the time here
			
			//set valid true
			canvasValid = true;
		}
	}

	// Happens when the mouse is moving inside the canvas
	function myMove(e){
		if (isDrag) {
			getMouse(e);

			mySel.x = mx - offsetx;
			mySel.y = my - offsety;   

			// something is changing position so we better invalidate the canvas!
			invalidate();
		} else if (isRotateDrag) {
			// time to rotate!
			var oldx = mySel.x;
			var oldy = mySel.y;
			//     r
			// 0  1  2
			// 3     4
			// 5  6  7	
			if (1 == expectRotate){
				//compute r
				var dx=oldx - mx;
				var dy=oldy - my;
				mySel.r = Math.atan2(dy,dx) - Math.PI/2;
				//console.log("rotate: " + mySel.r + ".\n");
				invalidate();
			}		
		} else if (isResizeDrag) {
			// time ro resize!
			var oldx = mySel.x;
			var oldy = mySel.y;
			var oldw = mySel.w;
			var oldh = mySel.h;
			var sin_theta = Math.sin(mySel.r);
			var cos_theta = Math.cos(mySel.r);
			
			//map mouse pos to model space
			var mappedPos = mySel.mapPos(mx, my);
			
			//inform text content to re-calculate font size.
			if (mySel.content instanceof  TextContent) 
			{
				mySel.content.bCalculateFontSize = true;
			}
			
			// 0  1  2
			// 3     4
			// 5  6  7
			switch (expectResize) {
				case 0:
					//pt-7 stays (w/2, h/2)
					mySel.w = oldw/2 - mappedPos[0];
					mySel.h = oldh/2 - mappedPos[1];
					//check for negative. we keep w/h positive always
					if(mySel.w < minResizeW) mySel.w = minResizeW;
					if(mySel.h < minResizeH) mySel.h = minResizeH;

					mySel.x += (oldw-mySel.w)/2 * cos_theta - (oldh-mySel.h)/2 * sin_theta;
					mySel.y += (oldh-mySel.h)/2 * cos_theta + (oldw-mySel.w)/2 * sin_theta;
					break;
				case 1:
					//pt-6 stays (0, h/2)
					//w part no change.
					mySel.h = oldh/2 - mappedPos[1];
					//check for negative. we keep w/h positive always
					if(mySel.w < minResizeW) mySel.w = minResizeW;
					if(mySel.h < minResizeH) mySel.h = minResizeH;

					mySel.x += - (oldh-mySel.h)/2 * sin_theta;
					mySel.y += (oldh-mySel.h)/2 * cos_theta;
					break;
				case 2:
					//pt-5 stays (-w/2, h/2)
					mySel.w = oldw/2 + mappedPos[0];
					mySel.h = oldh/2 - mappedPos[1];
					//check for negative. we keep w/h positive always
					if(mySel.w < minResizeW) mySel.w = minResizeW;
					if(mySel.h < minResizeH) mySel.h = minResizeH;
					
					mySel.x += -(oldw-mySel.w)/2 * cos_theta - (oldh-mySel.h)/2 * sin_theta;
					mySel.y += (oldh-mySel.h)/2 * cos_theta - (oldw-mySel.w)/2 * sin_theta;
					break;
				case 3:
					//pt-4 stays (w/2, 0)
					//h part no change
					mySel.w = oldw/2 - mappedPos[0];
					//check for negative. we keep w/h positive always
					if(mySel.w < minResizeW) mySel.w = minResizeW;
					if(mySel.h < minResizeH) mySel.h = minResizeH;

					mySel.x += (oldw-mySel.w)/2 * cos_theta;
					mySel.y += (oldw-mySel.w)/2 * sin_theta;
					break;
				case 4:
					//pt-3 stays (-w/2, 0)
					//h part no change
					mySel.w = oldw/2 + mappedPos[0];
					//check for negative. we keep w/h positive always
					if(mySel.w < minResizeW) mySel.w = minResizeW;
					if(mySel.h < minResizeH) mySel.h = minResizeH;

					mySel.x += -(oldw-mySel.w)/2 * cos_theta;
					mySel.y += -(oldw-mySel.w)/2 * sin_theta;
					break;
				case 5:
					//pt-2 stays (w/2, -h/2)
					mySel.w = oldw/2 - mappedPos[0];
					mySel.h = oldh/2 + mappedPos[1];
					//check for negative. we keep w/h positive always
					if(mySel.w < minResizeW) mySel.w = minResizeW;
					if(mySel.h < minResizeH) mySel.h = minResizeH;

					mySel.x += (oldw-mySel.w)/2 * cos_theta + (oldh-mySel.h)/2 * sin_theta;
					mySel.y += -(oldh-mySel.h)/2 * cos_theta + (oldw-mySel.w)/2 * sin_theta;
					break;
				case 6:
					//pt-1 stays (0, -h/2)
					//w part no change.
					mySel.h = oldh/2 + mappedPos[1];
					//check for negative. we keep w/h positive always
					if(mySel.w < minResizeW) mySel.w = minResizeW;
					if(mySel.h < minResizeH) mySel.h = minResizeH;

					mySel.x += (oldh-mySel.h)/2 * sin_theta;
					mySel.y += -(oldh-mySel.h)/2 * cos_theta;
					break;
				case 7:
					//pt-0 stays (-w/2, -h/2)
					mySel.w = oldw/2 + mappedPos[0];
					mySel.h = oldh/2 + mappedPos[1];
					//check for negative. we keep w/h positive always
					if(mySel.w < minResizeW) mySel.w = minResizeW;
					if(mySel.h < minResizeH) mySel.h = minResizeH;

					mySel.x += -(oldw-mySel.w)/2 * cos_theta + (oldh-mySel.h)/2 * sin_theta;
					mySel.y += -(oldh-mySel.h)/2 * cos_theta - (oldw-mySel.w)/2 * sin_theta;
					break;
			} // end switch
		
			invalidate();
		} // end else
	  
		getMouse(e);
		// if there's a selection see if we grabbed one of the selection handles
		if (mySel !== null && !isResizeDrag && !isRotateDrag) {
			//check for rotate
			{
				expectRotate = mySel.testHitRotationHandles(mx, my);
				if(1 == expectRotate)
				{
					invalidate();
					this.style.cursor='crosshair';
					return;
				}
			} // end check for rotate
			
			//check for resize
			{
				var resizeCursor = mySel.testHitResizeHandles(mx, my);
				if(0 <= resizeCursor)
				{
					invalidate();		
					switch (resizeCursor) {
						case 0:
							this.style.cursor='nw-resize';
							break;
						case 1:
							this.style.cursor='n-resize';
							break;
						case 2:
							this.style.cursor='ne-resize';
							break;
						case 3:
							this.style.cursor='w-resize';
							break;
						case 4:
							this.style.cursor='e-resize';
							break;
						case 5:
							this.style.cursor='sw-resize';
							break;
						case 6:
							this.style.cursor='s-resize';
							break;
						case 7:
							this.style.cursor='se-resize';
							break;
					}//end switch
					return;				
				}			
			} //end check for resize

			// not over a selection box, return to normal
			isResizeDrag = false;
			expectResize = -1;
			isRotateDrag = false;
			expectRotate = -1;
			this.style.cursor='auto';
		} //end if
	  
	} //end myMove

	// Happens when the mouse is clicked in the canvas
	function myDown(e){
                
		getMouse(e);
                
                //put in a text box?
                if (textMode === true && mySel === null)
                {
                    addTextBox(mx, my, 200, 50,0,1,1,'I love Bforbel');
                    return;
                }
	  
		//we are over a selection box
		if (expectResize !== -1) {
			isResizeDrag = true;
			return;
		}
		else if (expectRotate !== -1) {
			isRotateDrag = true;
			return;
		}
	  
		clear(gctx);
		
		//test if we're clicking on a content box to select it.
		var l = contentBoxes.length;
		for (var i = l-1; i >= 0; i--) {
			if(contentBoxes[i].testHitContentBox(mx, my))
			{
				mySel = contentBoxes[i];
				
				//update extra buttons
				onSelectChange();
				
				//update offsets
				offsetx = mx - mySel.x;
				offsety = my - mySel.y;
				mySel.x = mx - offsetx;
				mySel.y = my - offsety;
				isDrag = true;
		  
				invalidate();
				clear(gctx);
                                if (mySel.content instanceof  TextContent) {
                                    $('#nick-tools-list').hide();
                                    $('#nick-text-update-field').val(mySel.content.text);
                                    $('.select-block .drop').hide();
                                    $('#nick-text-tools-list').show();
                                    $('#nick-text-update-list').show();
                                }
                                else {
                                    $('.select-block .drop').hide();
                                    $('#nick-text-tools-list').hide();
                                    $('#nick-text-update-list').hide();
                                    $('#nick-tools-list').show();                                    
                                }
 
                                
				return;
			}		
		}//end for
		// havent returned means we have selected nothing
		mySel = null;
                
                $('#nick-tools-list').hide();
                $('.select-block .drop').hide();
                $('#nick-text-tools-list').hide();
                $('#nick-text-update-list').hide();
		
		//update extra buttons
		onSelectChange();
		
		// clear the ghost canvas for next time
		clear(gctx);
		// invalidate because we might need the selection border to disappear
		invalidate();
	} //end myDown

	function myUp(){
		isDrag = false;
		isResizeDrag = false;
		expectResize = -1;
		isRotateDrag = false;
		expectRotate = -1;
	}


	function invalidate() {
		canvasValid = false;
	}

	// Sets mx,my to the mouse position relative to the canvas
	// unfortunately this can be tricky, we have to worry about padding and borders
	function getMouse(e) {
		var element = canvas, offsetX = 0, offsetY = 0;

		if (element.offsetParent) {
			do {
				offsetX += element.offsetLeft;
				offsetY += element.offsetTop;
			} while ((element = element.offsetParent));
		}

		// Add padding and border style widths to offset
		offsetX += stylePaddingLeft;
		offsetY += stylePaddingTop;

		offsetX += styleBorderLeft;
		offsetY += styleBorderTop;

		mx = e.pageX - offsetX;
		my = e.pageY - offsetY
	}//end getMouse
	
	//show | hide extra buttons based on items selected or not.
	function onSelectChange()
	{
		if(null !== mySel)
		{
			if (mySel.content instanceof  TextContent) 
			{
				minResizeW = mySel.content.getMinWidth();
			}
			else 
			{
				minResizeW = 20;
			}
		}
	}

	// If you dont want to use <body onLoad='init()'>
	// You could uncomment this init() reference and place the script reference inside the body tag
	init2();
	//window.init2 = init2;
//})(window);
});