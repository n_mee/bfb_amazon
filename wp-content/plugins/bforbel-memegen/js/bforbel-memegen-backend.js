jQuery(document).ready(function ($){
    var activeImageTab = 0;//selected category tab
    var activeImageId = null;//selected image post_id
    
    $('.category-button').click(function(e) {
        var clickedTabId = $(this).attr('id');//category-tab_id-category_id
        var idArray = clickedTabId.split('-');
        if (idArray[1] !== activeImageTab) {
            //remove current display
            $('.image-category-' + activeImageTab).hide();
            //change activeTab and display new images
            activeImageTab = idArray[1];
            $('.image-category-' + activeImageTab).show();
            $('input.hidden-category-id').val(idArray[2]);
            $('#active-category-name').text($(this).val());
            $('#current-name').val($(this).val());
            
            
            if (activeImageId !== null) {
                //ungrey the previous selected image
                $('img#image-post-id-' + activeImageId).toggleClass('selected');
                activeImageId = null;
                $('input.hidden-post-id').val('');
                $('#image-update-panel').hide();
            }
        }
    });
    
    $('div#memegen-image-panel img').click(function(e) {
        //set post id
        var clickedImgId = $(this).attr('id');
        var idArray = clickedImgId.split('-');
        if (idArray[3] !== activeImageId) {
            $('input.hidden-post-id').val(idArray[3]);
            //check the active category radio button
            $('input#category-radio-' + activeImageTab).prop('checked', true);
            if (activeImageId !== null) {
                //ungrey the previous selected image
                $('img#image-post-id-' + activeImageId).toggleClass('selected');
            }
            //grey the current selected image
            $(this).toggleClass('selected');
            //update active image id
            activeImageId = idArray[3];
            $('#image-update-panel').show();
        }
       
        
    });
});


