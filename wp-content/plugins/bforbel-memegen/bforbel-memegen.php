<?php
/**
 * Plugin Name: Bforbel Memegen
 * Plugin URI: bforbel.com
 * Description: To enable memegen feature
 * Version: 1.0
 * Author: Bforbel Media Pte Ltd
 * Author URI: bforbel.com
 * License: Bforbel License
 */


define('BFBM_PATH', plugins_url() . '/bforbel-memegen');

function bfbm_init() {
    $bfbm_options = array();
    $bfbm_options['debug'] = 1;
    update_option('bfbm_options', $bfbm_options);
}
add_action('activate_bforbel-memegen/bforbel-memegen.php', 'bfbm_init');


function bfbm_config() { include('bfbm-admin.php'); }

function bfbm_config_page() {
	if ( function_exists('add_submenu_page') )
		add_options_page(__('Bforbel Memegen'), __('Bforbel Memegen'), 'manage_options', 'bforbel-memegen', 'bfbm_config');
}
add_action('admin_menu', 'bfbm_config_page');

function bfbm_get_options() {
   return get_option('bfbm_options');
}

function bfbm_get_option($opt) {
    $bfbm_options = bfbm_get_options();
    return htmlspecialchars_decode( stripslashes ( $bfbm_options[$opt] ) );
}

/* html rendering */

function bfbm_link_html($post_id, $action) {
    $link = "<a class='bfbm-link' href='?bfbmaction=".$action."&amp;postid=". $post_id ."' rel='nofollow'>". bfbm_before_link_img() ."</a>";
    $link = apply_filters( 'bfbm_link_html', $link );
    return $link;
}

//render html 
function bfbm_frontend( $args = array() ) {
	if(true || is_user_logged_in())
	{
		include("bfbm-page-template.php");
	}
	else 
	{
		die("login needed");
	}
}

function bfbm_shortcode_func() {
    bfbm_frontend();
}
add_shortcode('bforbel-memegen-frontend', 'bfbm_shortcode_func');

function bfbm_content_filter($content) {
	if (is_page()){
		if (strpos($content,'{{bforbel-memegen-frontend}}')!== false) {
			$content = str_replace('{{bforbel-memegen-frontend}}', bfbm_frontend(), $content);
		}
	}
	return $content;
}
add_filter('the_content','bfbm_content_filter');


//add post menu
//if ( current_user_can( 'edit_posts' ) )  {
    include('bfbm-post.php');
//}