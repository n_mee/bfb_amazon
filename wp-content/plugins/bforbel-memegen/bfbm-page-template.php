<?php

        define("MEMEGEN_MAX_IMAGE_HEIGHT",94);
        define("MEMEGEN_MAX_IMAGE_WIDTH",87);
        define("MEMEGEN_DISPLAYED_IMAGES",9);
        define("MEMEGEN_EXPANDED_DISPLAYED_IMAGES",24);
        
        wp_enqueue_style('bforbel_memegen_frontend_css', plugins_url('/bforbel-memegen/css/bforbel-memegen-frontend.css'), array(),null );
        wp_enqueue_script('bforbel_memegen_frontend_js', plugins_url('/bforbel-memegen/js/bforbel-memegen-frontend.js'), array(),null );
        
        //read subcategories of memegen
        
        $memegen_category_id = get_category_by_slug('memegen')->term_id;
        $category_args = array(
            'child_of' => $memegen_category_id,    
        );
        $memegen_categories = get_categories($category_args);
        /*
        $memegen_tabs = array();
        foreach ($memegen_subcategories as $subcategory)
        {
            $memegen_tabs[] = array(
                'name' => $subcategory->name,
                'slug' => $subcategory->name,
            );
        }
         * 
         */
        
        //read images from each category into memegen_images array
        $memegen_images = array();
        
        foreach ($memegen_categories as $category_index => $category)
        {
        
            //read memegen image sources into an array
            //each element of the $memegen_images array is an associative array (url, width, height)

            $image_args = array(
                'posts_per_page' => -1,  //all posts
                'post_type' => 'attachment',
                'category_name' => $category->slug,
                'post_mime_type' => 'image',
            );
            
            $memegen_posts = get_posts($image_args);

            foreach ( $memegen_posts as $memegen_image_index => $post ){

                //$image = wp_get_attachment_image_src($post->ID, 'full'); //array 0=>url , 1=>width, 2=> height
                $image = wp_get_attachment_image_src($post->ID, 'thumbnail');
                if ($image !== false && $image[1] > 0 && $image[2] > 0) {
                    $new_image = array();

                    //set url
                    $new_image['url'] = $image[0];

                    //resize to expected width, height
                    $aspect_ratio = $image[2] / $image[1];// height / width
                    if ($aspect_ratio > MEMEGEN_MAX_IMAGE_HEIGHT / MEMEGEN_MAX_IMAGE_WIDTH)
                    {
                        //resize to maximum height
                        $new_image['height'] = MEMEGEN_MAX_IMAGE_HEIGHT;
                        $new_image['width'] = round( $image[1] * MEMEGEN_MAX_IMAGE_HEIGHT / $image[2] );
                    }
                    else
                    {
                        //resize to maximum width
                        $new_image['width'] = MEMEGEN_MAX_IMAGE_WIDTH;
                        $new_image['height'] = round( $image[2] * MEMEGEN_MAX_IMAGE_WIDTH / $image[1] );
                    }

                    //set up display classes for the image div
                    $memegen_row = floor($memegen_image_index/3);
                    $memegen_expanded_row = floor($memegen_image_index/8);

                    $new_image['div_class'] = 'block memegen-tab-'.$category_index.' memegen-row-'.$memegen_row.' memegen-expanded-row-'.$memegen_expanded_row;
                    
                    
                    if ($category_index > 0)
                    {
                        //no initial display
                        $new_image['div_class'] .= ' hidden expanded-hidden';
                    }
                    else
                    {
                        //only display up the first few
                        if ($memegen_image_index < MEMEGEN_DISPLAYED_IMAGES)
                        {
                                $new_image['div_class'] .= ' visible';
                        }
                        else
                        {
                            $new_image['div_class'] .= ' hidden';
                        }
                        if ($memegen_image_index < MEMEGEN_EXPANDED_DISPLAYED_IMAGES)
                        {
                                $new_image['div_class'] .= ' expanded-visible';
                        }
                        else
                        {
                            $new_image['div_class'] .= ' expanded-hidden';
                        }
                    }

                    $memegen_images[] = $new_image; 
                }
            }
        }
        

?>
			
<body>
	<!-- main container of all the page elements -->
	<div id="wrapper">
		<!-- header of the page -->
		<header id="header">
			<div class="holder">
				<!-- page logo -->
				<strong class="logo"><a href="#">B for Bel</a></strong>
				<div class="section">
					<div class="popup-holder1">
						<a class="open" href="#">menu</a>
						<div class="popup">
							<div class="categories">
								<ul>
									<li><a href="#">Humor &amp; Fun</a></li>
									<li><a href="#">Beauty &amp; Fashion</a></li>
									<li><a href="#">Ideas &amp; Inspiration</a></li>
								</ul>
								<ul>
									<li><a href="#">Products &amp; Gifts</a></li>
									<li><a href="#">Food</a></li>
									<li><a href="#">Games</a></li>
									<li><a href="#">Tips &amp; Tricks</a></li>
								</ul>
							</div>
							<ul class="nav">
								<li><a href="#">About</a></li>
								<li><a href="#">Privacy &amp; Terms</a></li>
								<li><a href="#">Copyright &amp; Trademark</a></li>
							</ul>
						</div>
					</div>
					<!-- search form -->
					<form action="#" class="search-form">
						<fieldset>
							<input type="submit" value="search" >
							<input type="text" >
						</fieldset>
					</form>
				</div>
				<div class="section r">
					<ul class="social-list">
						<li><a href="#">facebook</a></li>
						<li><a href="#" class="twitter">twitter</a></li>
						<li><a href="#" class="pinterest">pinterest</a></li>
					</ul>
					<a class="upload-btn lightbox" href="#popup1">upload</a>
					<div class="popup-holder1">
						<a class="open" href="#"><img src="wp-content/themes/bforbel/images/photo1.jpg" alt="image description" width="30" height="31" ><span>Belle</span></a>
						<div class="popup">
							<ul class="topiclist">
								<li><a href="#">My Ratings</a></li>
								<li><a href="#" class="creations">My Creations</a></li>
								<li><a href="#" class="uploads">My Uploads</a></li>
								<li><a href="#" class="favorites">My Favorites</a></li>
								<li><a href="#" class="notifications">Notifications</a></li>
								<li><a href="#" class="settings">Settings</a></li>
								<li><a href="#" class="logout">Log Out</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>       

		<div class="w1">
			<!-- poll-form -->
			<form action="" onSubmit="return false;" class="poll">
				<fieldset>
					<div class="column">
						<div class="block">
							<div class="area"><textarea cols="30" rows="5" placeholder="Enter background story text here"></textarea></div>
						</div>
						<div class="block" id="nick-canvas-container">
                                                    <ul id="nick-tools-list" class="tools-list">
                                                        <li><a id="remove-button" class="remove-button" href="#">Remove</a></li>
                                                        <li><a id="flip-button" href="#" class="flip">Flip</a></li>
                                                        <li><a id="flop-button" href="#" class="flop">Flop</a></li>
                                                        <li><a id="clone-button" class="clone-button" href="#" class="clone">Clone</a></li>
                                                        <li><a id="forward-button" class="forward-button" href="#" class="forward">Bring<br> Forward</a></li>
                                                        <li><a id="backwards-button" class="backwards-button" href="#" class="backwards">Bring<br> Backwards</a></li>
                                                    </ul>
                                                    
                                                    <ul id="nick-text-tools-list" class="tools-list">
                                                        <li><a class="remove-button" href="#">Remove</a></li>
                                                        <li><a class="clone-button" href="#" class="clone">Clone</a></li>
                                                        <li><a class="forward-button" href="#" class="forward">Bring<br> Forward</a></li>
                                                        <li><a class="backwards-button" href="#" class="backwards">Bring<br> Backwards</a></li>
                                                    </ul>
                                                    
                                                    <ul id="nick-text-update-list" class="tools-list">
                                                        <li><input type="text"  id="nick-text-update-field" value="Enter Text Here" /></li>
                                                        <li><input type="button" id="nick-text-update-button" value="Update" /></li>
                                                        <li><input type="button" id="nick-text-shade-drop" value="" /></li>
                                                    </ul>
                                                    
                                                    <div class="select-block">
                                                        <div class="drop" id="nick-text-shade-drop-list">
								<ul>
									<li><a href="#">1</a></li>
									<li><a href="#" class="bg1">2</a></li>
									<li><a href="#" class="bg2">3</a></li>
								</ul>
                                                        </div>
                                                    </div>
                                                    
                                                    <div id="memegen-container">
                                                        <canvas id="memegen-canvas" width="593" height="433">
							
                                                        
                                                        
                                                        
							<div class="area drag"><textarea cols="30" rows="5" placeholder="Enter text here"></textarea><span class="drag1">&nbsp;</span></div>

                                                        
                                                        
                                                        
                                                        
                                                        
                                                        </canvas>
                                                    </div>
						</div>
					</div>
					<aside class="aside">
						<div class="choose-block">
							<span>Choose your background color:</span>
							<ul class="colors-list">
								<li><a href="#">1</a></li>
								<li><a href="#" class="bg2">2</a></li>
								<li><a href="#" class="bg3">3</a></li>
								<li><a href="#" class="bg4">4</a></li>
								<li><a href="#" class="bg5">5</a></li>
								<li><a href="#" class="bg6">6</a></li>
								<li><a href="#" class="bg7">7</a></li>
								<li><a href="#" class="bg8">8</a></li>
								<li><a href="#" class="bg9">9</a></li>
								<li><a href="#" class="bg10">10</a></li>
							</ul>
						</div>
						<div class="select-block">
							<a href="#" class="open">select</a>
							<div class="drop">
								<ul>
									<li><a href="#">1</a></li>
									<li><a href="#" class="bg1">2</a></li>
									<li><a href="#" class="bg2">3</a></li>
								</ul>
							</div>
						</div>
						<div class="tabs-area">
							<ul class="tabset">                                                          
                                                            <?php foreach ($memegen_categories as $category_index => $category) : ?>
								<li <?php if ($category_index == 0) echo 'class="active"' ?>>
								<a href="#" id="<?php echo 'memegen-tab-'.$category_index; ?>"><?php echo $category->name; ?></a></li>
                                                            <?php endforeach; ?>
							</ul>
							<div class="tab-content">
								<div class="items-holder">
                                                                    
                                                                    <?php foreach ($memegen_images as $memegen_image) : ?>
                                                                    
                                                                        <div class="<?php echo $memegen_image['div_class']; ?>" id="<?php echo $memegen_image['div_id']; ?>">
                                                                            <a href="#">
											<img class="memegen-image" src="<?php echo $memegen_image['url']; ?>" alt="image description" width="<?php echo $memegen_image['width']; ?>" height="<?php echo $memegen_image['height']; ?>" >
											<div class="mask">&nbsp;</div>
                                                                            </a>
									</div>
                                                                    
                                                                    <?php endforeach ; ?>
  
								</div>
								<a href="#" class="prev">prev</a>
								<a href="#" class="next">next</a>
							</div>
							<div class="view">
								<a href="#" id="expanded-view" class="view"><span>View All</span><em>Minimise</em></a>
							</div>
                                                    
                                                    
                                                        <img id="nick-text-button" src="wp-content/themes/bforbel/images/textbutton_nick.jpg" />
                                                    
                                                    
                                                    
							<input id="save-button" type="submit" value="Save">
						</div>
					</aside>
				</fieldset>
			</form>
		</div>
                <!-- footer of the page -->
		<footer id="footer">
			<div class="footer-holder">
				<div class="promobox"><a href="#"><img src="wp-content/themes/bforbel/images/banner.jpg" alt="image description" width="702" height="87" ></a></div>
				<div class="holder">
					<!-- navigation of the page -->
					<nav class="add-nav">
						<ul>
							<li><a href="#">About</a></li>
							<li><a href="#">Privacy &amp; Terms</a></li>
							<li><a href="#">Advertise</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</nav>
					<p class="copyright"><a href="#">&copy; Bforbel Media 2014</a></p>
				</div>
			</div>
		</footer>
	</div>
	<div class="popup-holder">
		<div id="popup1" class="lightbox">
			<strong class="head">Would you like to...</strong>
			<div class="upload-block">
				<div class="block">
					<a href="#">
						<span>Upload an image?</span>
						<img src="wp-content/themes/bforbel/images/img12.jpg" alt="image description" width="153" height="115" >
						<div class="mask">&nbsp;</div>
					</a>
				</div>
				<span class="sep">or</span>
				<div class="block">
					<a href="#">
						<span>Make a Benjamin Meme?</span>
						<img src="wp-content/themes/bforbel/images/img13.jpg" alt="image description" width="138" height="158" >
						<div class="mask">&nbsp;</div>
					</a>
				</div>
			</div>
		</div>
		<div id="popup2" class="lightbox">
			<h2>Log In</h2>
			<div class="holder">
				<p>Connect with a social network</p>
				<a href="#" class="btn-facebook">Login with Facebook</a>
				<span class="or">Or <a href="#popup3" class="lightbox">Sign Up</a> for an account</span>
				<p>Log in with your email address:</p>
				<form action="#" class="login-form">
					<fieldset>
						<div class="row">
							<label for="email">Email</label>
							<input id="email" type="email" >
						</div>
						<div class="row">
							<label for="password">Password</label>
							<input id="password" type="password" >
							<a href="#popup4" class="lightbox">Forgot your password?</a>
						</div>
						<input type="submit" value="Log In">
					</fieldset>
				</form>
			</div>
		</div>
		<div id="popup3" class="lightbox">
			<h2>Sign Up- Become a Belly Button</h2>
			<div class="holder">
				<p>BforBel is an internet party. <br>We love cats, food, Disney Princesses and important, political issues like giving dogs eyebrows.</p>
				<a href="#" class="btn-facebook">Login with Facebook</a>
				<p>
					<span>Sign up with your <a class="lightbox" href="#popup5">Email Address</a></span>
					<span>Have an account? <a href="#popup2" class="lightbox">Login</a></span>
				</p>
			</div>
		</div>
		<div id="popup4" class="lightbox">
			<strong class="head">Reset Password</strong>
			<div class="holder">
				<form action="#" class="survey">
					<fieldset>
						<div class="row">
							<label for="email1">Enter your email below</label>
							<input id="email1" type="email" >
						</div>
						<input type="submit" value="Submit">
					</fieldset>
				</form>
				<p>Thank you! You will receive an email with further instructions.</p>
			</div>
		</div>
		<div id="popup5" class="lightbox">
			<strong class="head">Become a Belly Button</strong>
			<div class="holder1">
				<form action="#" class="login-form">
					<fieldset>
						<div class="row">
							<label for="name">Full Name</label>
							<input id="name" type="text" >
						</div>
						<div class="row">
							<label for="email2">Email Address</label>
							<input id="email2" type="email" >
						</div>
						<div class="row">
							<label for="password1">Password</label>
							<input id="password1" type="password" >
						</div>
						<div class="visual1"><img src="wp-content/themes/bforbel/images/img16.jpg" alt="image description" width="345" height="99" ></div>
						<div class="row long">
							<label for="words">Enter The Words Above</label>
							<input id="words" type="text" >
						</div>
						<input type="submit" value="Sign Up">
					</fieldset>
				</form>
			</div>
		</div>
	</div>
        <?php wp_footer(); ?>
</body>
<?php

		